/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#include <ztas/shrp.h>
#include <ztas/ds/tree.h>
#include <ztas/ds/queue.h>
#include <ztas/utils/hash.h>
#include <debug.h>
#include <ztas/shrp.h>
#include "../rsm_client.h"
#include <ztas/ztime.h>
#include <ztas/proc/cfg.h>

#define TINTERVAL 1.0

/* -----------------------------------------------------------------------------
 * data structures
 * -------------------------------------------------------------------------- */

typedef struct {
    void*    data;
    uint64_t size;
    int conn;
} message_t;

typedef struct {
    int32_t k;
    int32_t cid;
    int sent_count;

    int reqnb;
    double reqp;
    int32_t req_size;

    struct ev_loop* loop;

    queue_t* requests;
    queue_t* responses;

    struct ev_async acreate_w;
    struct ev_timer finish_w;
    struct ev_timer create_w;
    struct ev_timer stat_w;

    uint64_t count;
    uint64_t tput_count;
    uint64_t recv_count;
    uint64_t ts;

    uint64_t avglatency;
    uint64_t minlatency;
    uint64_t maxlatency;
    int      seconds;

    rsm_client_t* client;
} state_t;

/* -----------------------------------------------------------------------------
 * client
 * -------------------------------------------------------------------------- */
void
my_response(rsm_client_t* rsm, rsmmsg_response_t* res, void* arg)
{
    // client receives response from rsm but can't keep the pointer
    state_t* state = (state_t*) arg;
    uint64_t now = __ztas_now64();
    uint64_t send_time = res->sent; // contains the sent time of the request

    state->count      += res->bcount ? res->bcount : 1;
    state->tput_count += res->bcount ? res->bcount : 1;
    state->recv_count++;

    state->avglatency += now - send_time;
    state->minlatency += now - send_time;
    state->maxlatency += now - send_time;
}


void
create_request(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if(EV_ERROR & revents) exit(EXIT_FAILURE);

    state_t* state = (state_t*) w->data;
    ev_async_send(loop, &state->acreate_w);
}

void
create_request_async(struct ev_loop* loop, struct ev_async* w, int revents)
{
    if(EV_ERROR & revents) exit(EXIT_FAILURE);

    state_t* state = (state_t*) w->data;

    // create request
//    if (state->cid >= 10)        return;

    rsmmsg_request_t* req = rsmmsg_request_init(state->req_size);
    req->k    = state->k;
    req->cid  = state->cid;
    req->sent = __ztas_now64();

    state->cid++;
//    sprintf(req->data, "hello world %d (k: %d)",
//            state->cid, state->k);

    DLOG("Sending request %d\n", state->cid-1);

    int r = 0;
    r = rsm_client_request(state->client, req, my_response, (void*) state);
    shrp_free(req);

    if (r == 0) {
        printf("ERROR WHILE SENDING REQUEST\n");
        state->cid--;
//        ev_timer_set(&state->create_w, state->reqp * 10, 0);
//        ev_timer_start(state->loop, &state->create_w);
    } else {
        state->sent_count++;

        if (state->cid % state->reqnb == 0) {
//            ev_timer_set(&state->create_w, state->reqp, 0);
//            ev_timer_start(state->loop, &state->create_w);
            return;
        }
        ev_async_send(loop, w);
    }
}

void
print_stat(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    state_t* state = (state_t*) w->data;
    uint64_t now = __ztas_now64();
    uint64_t elapsed = now - state->ts;

    printf("CLIENT\n");
    printf("\tthroughput: %f\n", state->tput_count*TINTERVAL*S/elapsed);
    if (state->tput_count != 0) {
        printf("\tavglatency: %f us\n", state->avglatency/1.0/US/state->recv_count);
//        printf("\tminlatency: %f us\n", state->minlatency/1.0/US/state->recv_count);
//        printf("\tmaxlatency: %f us\n", state->maxlatency/1.0/US/state->recv_count);
    } else {
        printf("\tavglatency: 0.0 us\n");
//        printf("\tminlatency: 0.0 us\n");
//        printf("\tmaxlatency: 0.0 us\n");
    }
    printf("\tmissing responses: %ld\n", state->sent_count - state->count);
//    printf("\ttime: %lu\n", now);
    printf("\tseconds: %d\n", state->seconds);
    state->tput_count = 0;
    state->recv_count = 0;
    state->ts = __ztas_now64();
    state->avglatency = 0;
    state->minlatency = 0;
    state->maxlatency = 0;
    state->seconds++;
}

/* -----------------------------------------------------------------------------
 * main()
 * -------------------------------------------------------------------------- */

void
finish_test(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if(EV_ERROR & revents) exit(EXIT_FAILURE);

    state_t* state = (state_t*) w->data;

    ev_async_stop(loop, &state->acreate_w);
    ev_timer_stop(loop, &state->finish_w);
    ev_timer_stop(loop, &state->create_w);
    ev_timer_stop(loop, &state->stat_w);

    rsm_client_stop(state->client);

    // finish rsm library
    rsm_client_fini(state->client);
    DLOG("Test finished\n");
    printf("[echo_client] Test finished\n");

    // free data structures
    while(!queue_empty(state->responses)) {
        shrp_free(queue_deq(state->responses));
    }
    queue_free(state->responses);

    while(!queue_empty(state->requests)) {
        shrp_free(queue_deq(state->requests));
    }
    queue_free(state->requests);

}
int
main(const int argc, const char* argv[])
{
    // create proxy state
    state_t state;
    state.requests = queue_create(100000);
    state.responses = queue_create(100000);
    state.loop = ev_loop_new(0);
    state.cid = 0;

    // measurements
    state.sent_count = 0;
    state.ts = __ztas_now64();
    state.count = 0;
    state.tput_count = 0;
    state.recv_count = 0;
    state.avglatency = 0;
    state.minlatency = 0;
    state.maxlatency = 0;
    state.seconds = 1;

    // initilize rsm
    assert (argc >= 2 && "echo_client rsm.ini [client_id]");

    int32_t client_id = -1;
    if (argc > 2)
        client_id = atoi(argv[2]);

//    state.client = rsm_client_initev(NULL, argv[1], client_id, (void*) &state);
    state.client = rsm_client_initev(state.loop, argv[1], client_id, (void*) &state);

    state.k   = state.client->k;

    cfg_t* cfg = cfg_parse(argv[1]);
    const char* str = cfg_get(cfg, "main:halt_after");
    double halt_time_s = str ? atof(str): 0; // in seconds

    // number of requests per upcall
    str = cfg_get(cfg, "echo_server:reqnb");
    state.reqnb = str ? atoi(str) : 10;

    // period for create-upcalls (in seconds)
    str = cfg_get(cfg, "echo_server:reqp");
    state.reqp = str ? atof(str) : 0.2;

    // request size
    str = cfg_get(cfg, "echo_server:req_size");
    state.req_size = str ? atoi(str) : 1024;


    //*** configure and start watchers
    ev_async_init(&state.acreate_w, create_request_async);
    ev_set_priority(&state.acreate_w, -2);
    // requests are created once starting in 1s
    ev_timer_init(&state.create_w, create_request, 3., state.reqp);
    //ev_set_priority(&state.create_w, -2);
    // terminate everything after x seconds
    ev_timer_init(&state.finish_w, finish_test, halt_time_s, 0.);
    // get statistics every TINTERVAL seconds
    ev_timer_init(&state.stat_w, print_stat, 3.03, TINTERVAL);

    // save state as watchers data
    state.create_w.data   = (void*) &state;
    state.acreate_w.data  = (void*) &state;
    state.finish_w.data   = (void*) &state;
    state.stat_w.data   = (void*) &state;

    ev_async_start(state.loop, &state.acreate_w);
    ev_timer_start(state.loop, &state.create_w);
    ev_timer_start(state.loop, &state.stat_w);
    if ((int)halt_time_s != 0)
        ev_timer_start(state.loop, &state.finish_w);

    // watchers configured ***//
    ev_loop(state.loop, 0);

    ev_loop_destroy(state.loop);
    printf("[echo_client] Client is stopped\n");

    return 0;
}
