.. -*- rst -*-

A simple echo service
---------------------

Echo client sends client requests with dummy payload of specified size. A bulk of x requests is sent each y seconds.

Echo server receives a client request and sends back a reply with the same payload.
Echo server does not do snapshots.

The exp/ directory contains 3 types of experiments on cluster:

- paxos     -- hardpaxos run as normal unprotected paxos
- encoded   -- hardpaxos, hardened with one AN-encoded trusted module
- twice     -- hardpaxos, hardened with duplicated AN-encoded trusted module