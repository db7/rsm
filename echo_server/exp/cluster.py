# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

# ~~~~~~~~~~~ experiment ~~~~~~~~~~~ 
optspace = {
    'clients'    : [1, 2, 4, 8, 16, 32, 48, 64, 96, 112, 114, 116, 118, 120, 122, 124, 126, 128],
    'batch'      : [30],
    'times'      : [1,2,3],
    'size'       : [64],
}

REQ_NB       = 2
REQ_P        = 0.002

HALT_AFTER   = 30
MAX_INFLIGHT = 0        # doesn't matter
CONNECTALL   = 'false'
RSMENGINE    = 'true'

# ~~~~~~~~~~~ cluster ~~~~~~~~~~~ 
reserved_nodes = range(4, 50)
broken_nodes   = [7, 8, 9, 13, 17, 18, 28, 29]

first_server = reserved_nodes[0]
first_client = first_server + 3

# servers
servers = range(first_server, first_client)
for i in broken_nodes:
    if i in servers: servers.remove(i)

#clients
clients = range(first_client, max(reserved_nodes)+1)
for i in broken_nodes:
    if i in clients: clients.remove(i)

#hosts  = chosts = shosts = ['localhost']

hosts  = ['stream%d' % i for i in servers] + ['stream%d' % i for i in clients]
shosts = hosts[:len(servers)]
chosts = hosts[len(servers):]
