## ---------------------------------------------------------------------
## Copyright (c) 2013 Technische Universitaet Dresden
## Distributed under the MIT license. See accompanying file LICENSE.
## ---------------------------------------------------------------------
library(ggplot2)
library(plyr)

read.table('./output/tput', header = T) -> df
## lat in us

#df <- ddply(df, c('clients', 'batch', 'times'), function(x) {
#  x$time <- x$time - min(x$time)
#  x$time <- x$time/1000000000.0
#  return (x)
#})

df2 <- ddply(df, c('client', 'clients', 'batch', 'times'), function(x) {
  y <- x[x$sec >= 10,]
  return (y)
})

df2 <- df2[df2$sec <= 20,]
df2$sec <- df2$sec

# df2$time <- trunc(df2$time, digits = 0)

## qplot(time, lat/1000.0, data = df2,##[df2$time < 20,],
##       geom="point", shape = factor(client), color = factor(client)) +
##   geom_line() +
##   facet_grid(clients ~ batch)

df3 <- aggregate(df2[,c('lat', 'tput')],
                 df2[,c('client', 'clients', 'batch', 'sec')], mean)
df4 <- aggregate(list(tput=df3[, c('tput')]), df3[,c('clients', 'batch', 'sec')], sum)
df5 <- aggregate(list(lat=df3[, c('lat')]), df3[,c('clients', 'batch', 'sec')], mean)
df6 <- merge(df4,df5)

qplot(sec, tput/1000, data = df6, geom = 'line') + facet_grid(clients ~ batch)
qplot(sec, lat/1000.0, data = df6, geom = 'line') + facet_grid(clients ~ batch, scale = 'free_y')  + expand_limits(y=0)

ggplot(df6, aes(x=factor(clients), y = lat/1000.0)) +
    geom_boxplot() +
    facet_grid(. ~ batch, scale = 'free_y')
    
df10 <- df6[order(df6$clients), ]
df11 <- aggregate(df10[,c('lat', 'tput')], df10[,c('clients', 'batch')], mean)

ggplot(df11, aes(tput, lat)) + geom_path()

write.table(df11, 'results.txt')

df6$tput2 <- round(df6$tput/100)*100
df7 <- aggregate(list(lat = df6[, c('lat')]), df6[,c('batch', 'tput2')], mean)
## df8 <- df7[df7$tput2 < 45000 & df7$lat < 1000,]
df8 <- df7
qplot(tput2, lat/1000, data = df8, geom = 'line') + facet_grid(. ~ batch, scale = 'free_y')


df9 <- aggregate(df6[, c('lat', 'tput')], df6[,c('clients', 'batch')], mean)
qplot(clients, lat/1000, data = df9,
      geom="point") +
  geom_line() + facet_grid(batch ~ . , scale = 'free_y')


