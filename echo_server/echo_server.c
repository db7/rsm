/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/ztime.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../rsm_server.h"


int
echo_deliver(rsm_server_t* server, rsmmsg_request_t* req, void* args)
{
    //printf("[echo_server] deliver: %s\n", req->data);
    int isleader = ((req->flags & 0x01) == 1);
#if 0
    int32_t size = isleader ? 0 : req->size;
#endif
    rsmmsg_response_t* res = rsmmsg_response_init(req->size);

    memcpy(&res->header, &req->header, sizeof(rsmmsg_t));
    res->k        = req->k;
    res->cid      = req->cid;
    res->sent     = req->sent;
    res->has_next = req->has_next;
    res->bcount   = req->bcount;
#if 0
    if (!isleader) {
        // only non-leaders send payload back
        res->size = req->size;
        memcpy(res->data, req->data, req->size);
    }
#endif

    res->size = req->size;
    memcpy(res->data, req->data, req->size);
/*
    //TODO for now leader does not send any reply
    if (isleader) {
        shrp_free(res);
        return 1;
    }
*/
    rsm_server_response(server, res);
    shrp_free(res);

    return 1;
}

int
echo_snapshot(rsm_server_t* server, int32_t snap_id, void* args)
{
    printf("[echo_server] snapshoting: %d\n", snap_id);
    rsm_server_snapdone(server, snap_id);

    return 1;
}

int
main(int argc, char* argv[])
{
    struct ev_loop* loop = ev_loop_new(0);

    assert (argc == 3 && "echo_server 0 rsm.ini");

    rsm_server_cbs_t cbs;
    cbs.deliver = echo_deliver;
    cbs.snapshot = echo_snapshot;
    cbs.response = NULL;

    rsm_server_t* server = rsm_server_init(loop, atoi(argv[1]),
            argv[2], &cbs, NULL);

    ev_loop(loop, 0);

    rsm_server_fini(server);

    ev_loop_destroy(loop);
    printf("[echo_server] server is stopped\n");

    return 0;
}
