/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _RSM_MSG_H_
#define _RSM_MSG_H_

#define _ZTAS_NO_SIZE_T

#include <stdint.h>
#include <ztas/msg.h>

/*
 * Base struct for all implemented consensus protocols.
 * RSM internal implementation reserves negative integers for message types,
 * consensus protocols must use positive integers.
 */
typedef struct {
    enum {
        RSM_DUMMY   = -1,
        RSM_CONFIG  = -2,
        RSM_BATCH   = -3,

        RSM_REQUEST  = -4,
        RSM_RESPONSE = -5,
        RSM_SNAPDONE = -6
    } type;
} rsmmsg_t;

/* constants for batching */
enum { HASNEXT_NO = 0, HASNEXT_YES = 1, HASNEXT_HEAD = 2 };

/*
 * Request message from RSM Client to RSM Service
 */
typedef struct {
    rsmmsg_t header;
    int32_t  k;        // client id
    int32_t  cid;      // request id
    uint64_t sent;     // send time (needed to calc latency)
    uint32_t flags;    // misc: bit 1 indicates isleader; bit 2 is a client batch
    int32_t  bcount;   // number of message in batch
    int32_t  has_next; // used for batches
    uint32_t mac;      // request MAC
    int32_t  size;     // payload size
    char     data[];   // request payload
} rsmmsg_request_t;

/*
 * Response message from RSM Service to RSM Client
 */
typedef rsmmsg_request_t rsmmsg_response_t;

typedef struct {
    int32_t size;
    char    data[];
} rsmmsg_item_t;

// TODO add message consensus_snapshot_t (from Consensus -> RSM)

/*
 * Snapshot Done message from RSM Service to Consensus
 */
typedef struct {
    rsmmsg_t header;
    int32_t  replica;
    int32_t  slot;
} rsmmsg_snapdone_t;

rsmmsg_request_t*  rsmmsg_request_init(int32_t size);
rsmmsg_response_t* rsmmsg_response_init(int32_t size);
void rsm_msg_seal(rsmmsg_request_t* msg, int32_t k, int32_t cid);
int rsm_msg_verify(rsmmsg_request_t* msg);

#endif
