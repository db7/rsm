/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/*
 * this file glues the rsm interface with hardpaxos implementation
 */
#define _ZTAS_CORE
#include <ztas.h>
#include <ztas/msg.h>
#include <ztas/mem.h>
#include <ztas/log.h>
#include <ztas/string.h>
#include <ztas/ds/idset.h>
#include <ztas/shrp.h>
#include <ztas/proc/proc.h>
//#include <ztas/utils/crc.h>
#include <assert.h>
#include <debug.h>

#ifdef IS_MODULE
#include <ztas/module.h>
#endif

#include "../utils/crc32c/crc32c.h"
#include "../hardpaxos/untrusted/replica.h"
#include "../hardpaxos/trusted/trusted.h"
#include "../hardpaxos/trusted/cert_mac.h"
#include "../hardpaxos/certs.h"

#include <ev.h>

#include "../rsm_msg.h"
#include "../rsm.h"

typedef struct {
    replica_t* replica;
    rsm_t* rsm;
    rsm_cbs_t cbs;
    void* args;
    int seal_requests;
} self_t;

__thread self_t* _self;
static crc32c_f* crc_f = NULL;

static trusted_t* trusted;
const char *key = "558";    // dummy key

#define SELF_SAVE(state) _self = state;
#define SELF_LOAD

#define WINDOW_MAX (10)		// for congestion window batching

extern int __ztas_sendv(int32_t dst, void** data, uint64_t* sizes, int len);

/*
 * indicates that the system is under heavy load (overloaded)
 * this function is used by RSM engine to regulate batching
 * (idea taken from PBFT)
 */
int
consensus_saturated(void* replica) {
    replica_t* r = (replica_t*) replica;
    assert(r);
    int32_t window = trusted_get_a(r->trusted) - trusted_get_c(r->trusted);
    return (window >= WINDOW_MAX);
}

/*
 * indicates that this replica is the leader
 * this function is used by RSM engine to decide if send to this replica
 */
int
consensus_leader(void* replica) {
    replica_t* r = (replica_t*) replica;
    assert(r);
    return r->follower->leaderid == r->pid;
}

/*
 * for direct call to hardpaxos's "snapshot done" routine from "snapshot"
 * thread (another technique with sending RSM_SNAPDONE message utilizes
 * main thread, which is not we need)
 */
int
consensus_setsnapdone(void* replica, int32_t snap_id) {
    replica_t* r = (replica_t*) replica;
    assert(r);
    replica_snapdone(r, snap_id);
    return 0;
}

int
consensus_setdying(void* replica) {
    replica_t* r = (replica_t*) replica;
    assert(r);
    r->dying = 1;
    return 0;
}

void*
ztas_init(int32_t pid)
{
    self_t* self = (self_t*) malloc(sizeof(self_t));
    assert (self);
    SELF_SAVE(self);

    self->args = NULL;
    bzero(&self->cbs, sizeof(rsm_cbs_t));

#ifdef HARDPAXOS_ENCODED
    LOG( "[hardpaxos rsm_replica] trusted module is encoded\n");
#endif
    LOG1("[hardpaxos rsm_replica] cert_t size is %lu\n", sizeof(cert_t));

    const char* max_inflight_str = ztas_cread("paxos:max_inflight");
    assert (max_inflight_str && "no paxos:max_inflight entry in config file");
    int max_inflight = atoi(max_inflight_str);

    const char* rstr = ztas_cread("paxos:replicas");
    assert (rstr && "no paxos:replicas entry in config file");
    idset_t* replicas = idset_init_str(rstr);

    trusted = trusted_init(idset_size(replicas), pid, strlen(key), key);

    replica_t* replica = replica_init(pid, replicas, trusted, max_inflight);
    self->replica = replica;
    idset_fini(replicas);

    self->seal_requests = 0;
    crc_f = crc32c_impl();

    return self;
}

void
ztas_fini(void* state)
{
    self_t* self = (self_t*) state;
    if (self) {
        trusted_fini(self->replica->trusted);
        replica_fini(self->replica);
        free(self);
    }
    free(trusted);
}

void
ztas_trig(void* state, int32_t aid)
{
    self_t* self = (self_t*) state;
    SELF_SAVE(self);

    if (aid == 2314) {
        if (__ztas_sendv(self->replica->pid, NULL, NULL, 100)
                == ZTAS_UCAST_OK) {
            // we call ztas_sendv with NULL pointers. It returns
            // whether it is possible to buffer 100 messages.
//            LOG("RSM: can_send = 1\n");
        } else
            // schedule another alarm
            ztas_alarm(2314, 200);
    }

    replica_trig(self->replica, aid);
}

void
ztas_recv(void* state, const void* data, size_t size)
{
    self_t* self = (self_t*) state;
    rsmmsg_t* header = (rsmmsg_t*) data;
    SELF_SAVE(self);

    switch(header->type) {
    case RSM_CONFIG: {
        assert (!self->args);
        rsmmsg_config_t* conf_msg = (rsmmsg_config_t*) data;
        assert (conf_msg->args);
        self->args = conf_msg->args;
        self->rsm  = conf_msg->rsm;
        memcpy(&self->cbs, &conf_msg->cbs, sizeof(rsm_cbs_t));

        self->seal_requests = conf_msg->seal_requests;
        printf("[hardpaxos rsm_replica] sealing requests: %d\n", self->seal_requests);

        replica_cbs_t rcbs;
        rcbs.issaturated = consensus_saturated;
        rcbs.isleader    = consensus_leader;
        rcbs.setsnapdone = consensus_setsnapdone;
        rcbs.setdying    = consensus_setdying;
        self->cbs.bindreplica(self->rsm, &rcbs, self->replica);

        break;
    }

    case RSM_BATCH: {
        /*
         * RSM_Replica receives a batch of rsmmsg_requests, where message[0]
         * is the header -- it contains no data in itself, but its `size`
         * indicates the total size of messages from 1 to batch->len.
         */

        rsm_batch_t* batch = (rsm_batch_t*) data;
        rsmmsg_request_t* header_msg = (rsmmsg_request_t*)batch->messages[0];

        uint32_t req_msg_size = sizeof(msg_request_t) + header_msg->size;
        msg_request_t* mrequest = shrp_malloc(req_msg_size);

        bzero(mrequest, req_msg_size);
        mrequest->type    = HP_REQUEST;
        mrequest->size    = header_msg->size;

        // copy all batched requests into REQ message
        int i;
        int size = 0;
        mrequest->cmd_mac = crc32c_init();
        for (i = 1; i < batch->len; i++) {
            rsmmsg_request_t* r = (rsmmsg_request_t*)batch->messages[i];
            if (self->seal_requests) {
                // trick: for batch, CRC over request CRCs
            	mrequest->cmd_mac = crc_f(mrequest->cmd_mac, &r->mac, sizeof(uint32_t));
            }

            memcpy(mrequest->cmd + size, batch->messages[i], batch->sizes[i]);
            size += batch->sizes[i];
            shrp_free(batch->messages[i]); // don't need this batch any more
            batch->messages[i] = NULL;
        }
        assert(size == mrequest->size);
        shrp_free(batch->messages[0]);
        batch->messages[0] = NULL;

        mrequest->cmd_mac = crc32c_finish(mrequest->cmd_mac);

        replica_recv(self->replica, req_msg_size, (cert_t*) mrequest);
        shrp_free(mrequest);

#if 0
        shrp_free(batch->messages[0]);
        batch->messages[0] = mrequest;
        batch->sizes[0]    = sizeof(msg_request_t);

        int r = __ztas_sendv(self->replica->pid, batch->messages, batch->sizes, batch->len);
        if (r != ZTAS_UCAST_OK) {
            // block further enqueues
            LOG("[hardpaxos rsm_replica] cannot send...\n");
            ztas_alarm(2314, 1000);

            // ztas_sendv() does not free memory in case of errors
            int i;
            for (i = 0; i < batch->len; ++i)
                shrp_free(batch->messages[i]);
        }
#endif

        break;
    }

    case RSM_REQUEST: {
        rsmmsg_request_t* msg = (rsmmsg_request_t*) data;
        int32_t tsize = sizeof(rsmmsg_request_t) + msg->size;
        if (tsize != size) {
            LOG("[hardpaxos rsm_replica] incomplete request, ignoring\n");
        } else {
            msg_request_t* mrequest = shrp_malloc(sizeof(msg_request_t) + tsize);
            bzero(mrequest, sizeof(msg_request_t) + tsize);

            mrequest->type    = HP_REQUEST;
            mrequest->size    = tsize;

            mrequest->cmd_mac = crc32c_init();
            if (self->seal_requests) {
                // trick: for batch, CRC over request CRCs
            	mrequest->cmd_mac = crc_f(mrequest->cmd_mac, &msg->mac, sizeof(uint32_t));
            }
            mrequest->cmd_mac = crc32c_finish(mrequest->cmd_mac);

            // send the whole rsmmsg_request in `cmd` field
            memcpy(mrequest->cmd, msg, tsize);

            replica_recv(self->replica, sizeof(msg_request_t) + tsize, (cert_t*) mrequest);
            shrp_free(mrequest);
        }
        break;
    }

    case RSM_RESPONSE: {
    	/* nothing to do */
        break;
    }

    case RSM_SNAPDONE: {
        // consensus_setsnapdone() must be used for thread-safe snapshotting!
        rsmmsg_snapdone_t* msg = (rsmmsg_snapdone_t*) data;
        assert(msg->replica == self->replica->pid);
        replica_snapdone(self->replica, msg->slot);
        break;
    }

    default: {
        cert_t* cert = (cert_t*) data;

        if (cert->type == HP_PROPOSE && self->seal_requests && !issnapmessage(cert->a)) {
            // before accepting, need to check a batch for integrity
            msg_propose_t* mpropose = (msg_propose_t*) data;
            if (size != sizeof(msg_propose_t) + mpropose->size)
                return;

            // iterate over a batch and check all rsm_requests
            uint64_t size  = 0;
            uint64_t total = mpropose->size;

            uint32_t bmac = crc32c_init();
            while (size < total) {
                rsmmsg_request_t* r = (rsmmsg_request_t*)(mpropose->cmd + size);
                if (!rsm_msg_verify(r))
                    return;
                bmac = crc_f(bmac, &r->mac, sizeof(uint32_t));
                size += sizeof(rsmmsg_request_t) + r->size;
            }
            bmac = crc32c_finish(bmac);

            if (size != total || bmac != mpropose->cpropose.cmd_mac) {
                LOG2("[hardpaxos rsm_replica] Corrupted batch, ignore PROPOSE message (id = %d, a = %d)\n",
                		mpropose->cpropose.id, mpropose->cpropose.a);
                return;
            }
        }

        replica_recv(self->replica, size, cert);
    }
    }
}

void
replica_deliver(replica_t* replica, const int32_t cmdsize, const void* cmd)
{
    SELF_LOAD;
    assert (_self->cbs.deliver);

    // iterate over a batch and deliver all rsm_requests
    uint64_t size  = 0;
    uint64_t total = cmdsize;

    while (size < total) {
        rsmmsg_request_t* r = (rsmmsg_request_t*)(cmd + size);
        _self->cbs.deliver(_self->rsm, r, _self->args);
        size += sizeof(rsmmsg_request_t) + r->size;
    }
    assert (size == total);
}

int
replica_snapshot(replica_t* replica, const int32_t snap_id)
{
    SELF_LOAD;
    assert(_self->cbs.snapshot);
    return _self->cbs.snapshot(_self->rsm, snap_id, _self->args);
}

void
set_alarm(int32_t aid, int32_t ms)
{
    ztas_alarm(aid, ms);
}

void
ztax_clock(ztime_t* ts)
{
    ztas_clock(ts);
}

void send_propose(int32_t pid, msg_propose_t* mpropose) {
    ztas_ucast(pid, mpropose, sizeof(msg_propose_t) + mpropose->size, 0);
}

void send_accept(int32_t pid, cert_accept_t* caccept) {
    ztas_ucast(pid, caccept, sizeof(cert_accept_t), 0);
}

void send_commit(int32_t pid, cert_commit_t* ccommit) {
    ztas_ucast(pid, ccommit, sizeof(cert_commit_t), 0);
}

void send_prepare(int32_t pid, cert_prepare_t* cprepare) {
    ztas_ucast(pid, cprepare, sizeof(cert_prepare_t), 0);
}

void send_promise(int32_t pid, msg_promise_t* mpromise) {
    ztas_ucast(pid, mpromise, sizeof(msg_promise_t) + mpromise->size, 0);
}

void SIListra_signal_error(const char* const errorMessage) {
    fprintf(stderr, "%s\n", errorMessage);
//    exit(CRASH);
}
