/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _RSM_CLIENT_H_
#define _RSM_CLIENT_H_
#include <stdint.h>
#include <ev.h>
#include <ztas/shrp.h>
#include <ztas/ds/queue.h>
#include "rsm.h"
#include "rsm_msg.h"

/*
 * res_arr (stores how many responses received for each request)
 * is of size MAXRESARR. res_arr is a circular slot array, if
 * some message comes from ancient past (more than MAXRESARR/2
 * messages ago), we come into troubles.
 */
#define MAXRESARR 100000
#define MAX_CLIENT_INFLIGHT 50

#define MAXPAYLOAD 10*1024*1024 // 10 MB
#define RSMMSG_MAX 2900      // 5 KB client requests

typedef struct rsm_client rsm_client_t;

/*
 * Callback when RSM Client receives response from RSM Service
 */
typedef void (rsm_client_response_cb) (rsm_client_t* rsm,
        rsmmsg_response_t* response, void* args);

/*
 * UNUSED
 * Each request from RSM Client can execute different callbacks
 * on response and be associated with different data (args)
 */
typedef struct {
    rsmmsg_request_t* req;
    rsm_client_response_cb* cb;
    void* args;
} request_t;

struct rsm_client {
    // TODO: remove this hack. it should be request dependent
    rsm_client_response_cb* response_cb;

    void* args;
    int32_t k;                  // client id counter
    int32_t cid;                // command id counter
    tree_t* replicas;           // connection to replica/replicas
    int buffer_size;            // buffering incoming messages
    queue_t* requests;          // to keep multiple inflight requests

    int32_t  seal_requests;      // all client requests must be sealed (with crc)
    int32_t  client_sync;        // client only works in sync mode
    int32_t  send_to_all;        // multicast request to all replicas
    int32_t  num_responses;      // receive that many responses before upcalling
    idset_t* replicas_idset;     // set of replica IDs

    int8_t   res_arr[MAXRESARR]; // each slot -- how many responses received
    uint32_t mac_arr[MAXRESARR]; // each slot -- received mac
    uint32_t min_arr;

    // ev_loop and thread (case loop created by rsm_client)
    struct ev_loop* loop;
    pthread_t thread;
    int local_loop;

    struct ev_async stop_w;     // used to stop local thread
    struct ev_timer halt_timer;

    rsmmsg_request_t* batch;
    int inflight;
};

rsm_client_t* rsm_client_initev(struct ev_loop* loop, const char* fname, int32_t client_id, void* args);
rsm_client_t* rsm_client_init(const char* fname, int32_t client_id, void* args);
void rsm_client_stop(rsm_client_t* rsm);
void rsm_client_fini(rsm_client_t* rsm);

int rsm_client_request(rsm_client_t* rsm, rsmmsg_request_t* req, rsm_client_response_cb* cb, void* args);
int rsm_client_invoke(rsm_client_t* rsm, rsmmsg_request_t* req, rsmmsg_response_t** res, void* args);

#endif /* _RSM_CLIENT_H_ */
