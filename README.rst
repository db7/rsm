RSM - a state machine replication library
=========================================

Quick Tutorial
--------------

RSM is a state machine replication library built using the ZTAS framework.

Different atomic broadcast protocols can be tied to it. For now only versions
of HardPaxos (including the unencoded one) are implemented, based on Paxos Made
Moderately Complex.

To get things running:

1) Clone this repository to the algorithms/ subdirectory of ZTAS

   - Clone ZTAS first, if you don't have it
     (clone from https://bitbucket.org/db7/ztas)
  
2) Instruct ZTAS to build RSM by adding 'rsm' entry to the algorithms/SConscript
   file (usually this entry is already there, but make sure).

3) Build ZTAS (using scons); RSM will be built also.

4) If build was successful, test RSM using echo service:

   - run hardpaxos.sh -- in normal case, it runs for 10 seconds and closes
  
   - examine server*.txt files to check that no errors happened
  
   - to test RSM on cluster, use echo_server/exp/ dude scripts


Overview of the stuff
---------------------

- rsm_msg:    messages used by RSM library
- rsm_engine: implementation of the main functionality of the server
- rsm_server: rsm server to work with rsm_client
- rsm_client: to be used as a client library


RSM CLIENT
----------

Handling synchronous requests::

  +--------+
  | client |
  +--------+
      ^
      |(1-2) rsm_client_invoke
      v
  +-------------+                              +-----------------+
  |             |+---------------------------->|                 |+--->
  | rsm_client  |                              |   connection    | 
  |             |<----------------------------+|                 |<---+
  +-------------+                              +-----------------+


Handling asynchronous requests::

  +--------+
  | client |
  +--------+
      +
      |(1) rsm_client_request
      v
  +-------------+     (2) connection_send      +-----------------+
  |             |+---------------------------->|                 |+--->
  | rsm_client  |                              |   connection    | (3) send
  |             |<----------------------------+|                 |<---+
  +-------------+  (4) connection_send_done    +-----------------+


Handling responses::

  +--------+
  | client |
  +--------+
       ^
       |(3) rsm->response_cb
       +
  +-------------+                              +-----------------+
  |             |      (2) connection_recv     |                 |+--->
  | rsm_client  |<----------------------------+|   connection    | (1) receive
  |             |                              |                 |<---+
  +-------------+                              +-----------------+


RSM SERVER (with RSM engine enabled)
------------------------------------

Handling requests::

       +   ^
       |   |(1) recv
       v   +
   +------------+
   | connection |
   +------------+                           +------+
         +                                  |      |(4) add request
         |(2) connection_recv               |      |    to batch
         v                                  +      v
   +------------+    (3) rsm_request      +------------+
   | rsm_server |+----------------------->| rsm_engine |
   +------------+                         +------------+
      +       ^                                +
      |       |                                |(5) if batch full
      |       |                                |    send to consensus
      |       |                                v
      |       | (7) rsm_server_deliver  +---------------+
      |       +------------------------+|   consensus   |
      |(8) server->deliver              +---------------+
      v                                     ^      +
   +-----------+                            |      |(6) decide
   |  server   |                            +------+
   +-----------+
   

Handling responses::

   +------------+
   |   server   |
   +------------+
         +
         |(1) rsm_server_response
         v
   +------------+    (2) rsm_response     +------------+
   | rsm_server |+----------------------->| rsm_engine |
   +------------+                         +------------+
      +       ^                             +      +
      |       |    (3a) send_response       |      |(3b) send response
      |       +-----------------------------+      |     to consensus
      |                                            v
      |(4a) connection_send             +---------------+
      v                                 |   consensus   |
   +-------------+                      +---------------+
   | connection  |                          ^      +
   +-------------+                          |      |(4b) do pruning
       +   ^                                +------+
       |   |(5) send
       v   +   
   
   
How to use that
---------------

* Scenario 1 (building a new service). Write the main file including
  rsm_server.h, link it with rsm_server and rsm_engine. The client code
  has to be linked with rsm_client.

* Scenario 2 (building a proxy). Write the main file including rsm.h,
  link it with rsm_engine. Handle requests from clients and forward them
  to the server.

* Scenario 3 (building a local proxy). Write two programs, a remote
  proxy and a local proxy. Remote proxy uses rsm_server. Local proxy
  uses rsm_client. The former uses the connection component to connect
  to the real service, the latter uses the listener to wait for
  requests from the client.
