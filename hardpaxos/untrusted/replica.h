/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _REPLICA_H_
#define _REPLICA_H_

#include <ztas/types.h>
#include <ztas/ztime.h>
#include <ztas/ds/idset.h>
#include <ztas/ds/queue.h>
#include <ztas/ds/tree.h>
#include "ds/sarr.h"
#include "follower.h"
#include "../certs.h"
#include "candidate.h"

#define NOPROGRESS_ALARM 1000

#define DUMMY_SNAP_MAC 12345678

typedef struct {
    int32_t pid;
    idset_t* replicas;
    trusted_t* trusted;

    int32_t max_inflight;
    sarr_t* commanders;
    queue_t* resting_commanders;

    follower_t* follower;
    candidate_t* candidate;

    int32_t     last_snap_id;
    uint32_t    last_snap_mac;

    ztime_t last_client_active;

    int32_t dead;           // TODO only for recovery test
    int32_t dying;          // TODO only for recovery test
} replica_t;

replica_t* replica_init(const int32_t pid,
                      const idset_t* replicas,
                      const trusted_t* trusted,
                      const int32_t max_inflight);
void      replica_fini(replica_t* replica);
int       replica_recv(replica_t* replica, const size_t size, const cert_t* msg);
int       replica_trig(replica_t* replica, int32_t aid);
int32_t   replica_pid(const replica_t* replica);

void      replica_execute(replica_t* replica, uint32_t cmd_mac, const int32_t size, const void* cmd);

void      replica_prepare(replica_t* replica, const int32_t epoch);
void      replica_propose(replica_t* replica, const msg_request_t* mrequest);
void      replica_repropose(replica_t* replica, const int32_t ac);
void      replica_commit(replica_t* replica, const cert_commit_t* ccommit);

void      replica_snapdone(replica_t* replica, const int32_t snap_id);
void      replica_prune(replica_t* replica, const int32_t snap_id);

#endif /* _REPLICA_H_ */
