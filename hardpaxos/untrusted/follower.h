/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _FOLLOWER_H_
#define _FOLLOWER_H_

#include <ztas/types.h>
#include <ztas/ds/queue.h>
#include "ds/sarr.h"
#include "../certs.h"
#include "../trusted/trusted.h"

typedef struct {
    cert_accept_t* caccept;     // proof that replica accepted slot
    msg_propose_t* mpropose;    // contains the command
} slot_t;

typedef struct {
    int32_t pid;
    trusted_t* trusted;

    int32_t leaderid;
    int32_t amount_accepted;
    int32_t amount_commited;

    sarr_t*  accepted;          // sarr contains slot_t items
    queue_t* unused_slots;      // preallocated slot_t's to use

    cert_commit_t* last_snap_cert;    // last SNAPSHOT message (proof of snapshot)

    uint32_t noprogress_timeout; // in ms!
} follower_t;

void follower_accept_local(follower_t* follower, const cert_accept_t* caccept, const msg_propose_t* mpropose);
void follower_commit_local(follower_t* follower, const cert_commit_t* ccommit);

void follower_prune(follower_t* follower, const int32_t snap_id, const cert_commit_t* ccommit);

int follower_accept(follower_t* follower, const msg_propose_t* mpropose);
int follower_committed(follower_t* follower, const cert_commit_t* ccommit);
int follower_promise(follower_t* follower, const cert_prepare_t* cprepare);

void follower_reset(follower_t* follower);
follower_t* follower_init(const int32_t pid, trusted_t* trusted);
void follower_fini(follower_t* follower);

/** return values */
#define FOLLOWER_ERROR       -1
#define FOLLOWER_OK           0

#endif /* _FOLLOWER_H_ */
