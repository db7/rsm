/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include <assert.h>
#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/log.h>
#include <ztas/shrp.h>
#include <ztas/ds/idset.h>
#include "util/copy_cert.h"
#include "../certs.h"
#include "../trusted/trusted.h"
#include "follower.h"
#include "candidate.h"

extern void send_prepare(int32_t pid, const cert_prepare_t* cprepare);

// ***************************************************************************
//                     Initialize/Finalize
// ***************************************************************************
void candidate_clean(candidate_t* candidate) {
    int i;
    for (i = 0; i < MAX_QUORUM; i++) {
        int j;
        for (j = 0; j < MAX_SLOTS; j++) {
            if (candidate->slots[j][i].caccept)
                shrp_free(candidate->slots[j][i].caccept);
            if (candidate->slots[j][i].mpropose)
                shrp_free(candidate->slots[j][i].mpropose);
            candidate->slots[j][i].caccept  = NULL;
            candidate->slots[j][i].mpropose = NULL;
        }
    }
}

static void candidate_reset(candidate_t* candidate) {
    candidate->accepted_num = 0;
    candidate->epoch_accepted = 0;
    candidate->slots_num = 0;
    candidate->curr_slot = 0;

    int i;
    for (i = 0; i < MAX_QUORUM; i++) {
        if (candidate->guards[i])
            shrp_free(candidate->guards[i]);
        candidate->guards[i] = NULL;

        if (candidate->snaps[i])
            shrp_free(candidate->snaps[i]);
        candidate->snaps[i] = NULL;
    }
    candidate_clean(candidate);
}

candidate_t*
candidate_init(const int32_t pid, const idset_t* replicas, trusted_t* trusted, follower_t* follower)
{
    candidate_t* candidate = (candidate_t*) malloc(sizeof(candidate_t));
    candidate->replicas = idset_init_copy(replicas);
    candidate->quorum_size = idset_size(replicas) / 2 + 1;
    candidate->trusted = trusted;
    candidate->last_epoch = 0;
    candidate->pid = pid;
    candidate->follower = follower;

    int i, j;
    for (i = 0; i < MAX_QUORUM; i++) {
        candidate->guards[i] = NULL;
        candidate->snaps[i] = NULL;
        for (j = 0; j < MAX_SLOTS; j++) {
            candidate->slots[j][i].caccept  = NULL;
            candidate->slots[j][i].mpropose = NULL;
        }
    }

    candidate_reset(candidate);

    return candidate;
}

void
candidate_fini(candidate_t* candidate)
{
    int i, j;
    for (i = 0; i < MAX_QUORUM; i++) {
        if (candidate->guards[i])
            shrp_free(candidate->guards[i]);

        if (candidate->snaps[i])
            shrp_free(candidate->snaps[i]);

        for (j = 0; j < MAX_SLOTS; j++) {
            if (candidate->slots[j][i].caccept)
                shrp_free(candidate->slots[j][i].caccept);
            if (candidate->slots[j][i].mpropose)
                shrp_free(candidate->slots[j][i].mpropose);
        }
    }

    idset_fini(candidate->replicas);
    free(candidate);
}


// ***************************************************************************
//                     Messages
// ***************************************************************************
int candidate_prepare(candidate_t* candidate, const int32_t epoch) {
    cert_prepare_t* cprepare = shrp_malloc(sizeof(cert_prepare_t));
    cert_promise_t* cpromise = shrp_malloc(sizeof(cert_promise_t));
    int res = trusted_prepare(candidate->trusted, epoch, cprepare, cpromise);
    if (res)
        return CANDIDATE_ERROR;

    if (candidate->last_epoch < cprepare->e)
        candidate->last_epoch = cprepare->e;

    candidate_reset(candidate);

    // add my own cpromise certificate to guards
    shrp_hold(cpromise);
    candidate->guards[candidate->accepted_num] = cpromise;
    // add my own SNAPSHOT certificate to snaps
    if (candidate->follower->last_snap_cert) {
        shrp_hold(candidate->follower->last_snap_cert);
        candidate->snaps[candidate->accepted_num] = candidate->follower->last_snap_cert;
    }

    int i;
    int min = sarr_min(candidate->follower->accepted);
    int max = cpromise->a; // the highest slot ever seen
    for (i = min; i <= max; i++) {
        slot_t* slot = sarr_get(candidate->follower->accepted, i);
        assert(slot && "Follower's accepted is corrupted");
        trusted_update(candidate->trusted, slot->caccept);

        slot_t* cslot = &candidate->slots[i-min][candidate->accepted_num];
        if (cslot->caccept)  shrp_free(cslot->caccept);
        if (cslot->mpropose) shrp_free(cslot->mpropose);

        shrp_hold(slot->caccept);
        cslot->caccept  = slot->caccept;
        shrp_hold(slot->mpropose);
        cslot->mpropose = slot->mpropose;
    }

    candidate->accepted_num++;
    candidate->slots_num = max - min + 1;

    idset_it_t it;
    idset_it_init(candidate->replicas, &it);
    int32_t pid;
    while ((pid = idset_it_next(&it)) >= 0) {
        // broadcast to everyone except me
        if (pid != candidate->pid)
            send_prepare(pid, cprepare);
    }

    follower_reset(candidate->follower);
//    assert( sarr_min(candidate->follower->accepted) % TRUSTED_Z == 1 );

    shrp_free(cprepare);
    shrp_free(cpromise);
    return CANDIDATE_OK;
}

int candidate_start_epoch(candidate_t* candidate, const cert_promise_t* cpromise,
		const cert_commit_t* last_snap_cert, const int32_t size, const void* slots) {
    candidate->last_epoch = trusted_get_e(candidate->trusted);
    if (cpromise->e < candidate->last_epoch) {
        // we are already in epoch greater that incoming proposed epoch
        return CANDIDATE_IGNORED;
    }
    if (cpromise->e > candidate->last_epoch) {
        // somebody else proposed greater epoch
        candidate->last_epoch = cpromise->e;
        return CANDIDATE_PREEMPTED;
    }
    if (cpromise->id == NONE_ID) {
        // somebody else is already in the same epoch as I tried to propose
        candidate->last_epoch = cpromise->e;
        return CANDIDATE_PREEMPTED;
    }

    // TODO check that promise certificate is good and slots are good as well

    // add promise certificate to accepted and his slots to candidate's slots
    // NOTE: cannot use shrp_hold because certs are part of the whole message
    candidate->guards[candidate->accepted_num] = shrp_malloc(sizeof(cert_promise_t));
    _copy_promise(candidate->guards[candidate->accepted_num], cpromise);
    if (last_snap_cert->c > TRUSTED_Z) {
        candidate->snaps[candidate->accepted_num] = shrp_malloc(sizeof(cert_commit_t));
        _copy_commit(candidate->snaps[candidate->accepted_num], last_snap_cert);
    }

    int amount = 0;
    if (size > 0) {
        const void* pos = slots;
        const void* end = pos + size;

        // add slots in the candidate's `slots` one by one
        while (pos < end) {
            cert_accept_t* caccept  = (cert_accept_t*) pos;
            msg_propose_t* mpropose = (msg_propose_t*) (pos + sizeof(cert_accept_t));

            slot_t* cslot = &candidate->slots[amount][candidate->accepted_num];
            if (cslot->caccept)  shrp_free(cslot->caccept);
            if (cslot->mpropose) shrp_free(cslot->mpropose);

            cslot->caccept  = shrp_malloc(sizeof(cert_accept_t));
            cslot->mpropose = shrp_malloc(sizeof(msg_propose_t) + mpropose->size);

            memcpy(cslot->caccept,  caccept,  sizeof(cert_accept_t));
            memcpy(cslot->mpropose, mpropose, sizeof(msg_propose_t) + mpropose->size);

            amount++;
            pos += sizeof(cert_accept_t) + sizeof(msg_propose_t) + mpropose->size;
        }
    }

    candidate->accepted_num++;

    if (candidate->epoch_accepted)
        return CANDIDATE_ACCEPTED;

    // update the amount of slots to repropose
    if (candidate->slots_num < amount)
        candidate->slots_num = amount;

    if (candidate->accepted_num < candidate->quorum_size)
        return CANDIDATE_OK;

    candidate->epoch_accepted = 1;
    candidate->follower->leaderid = candidate->pid;
    return CANDIDATE_ACCEPTED_NOW;
}
