/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/string.h>
#include "../../certs.h"
#include "copy_cert.h"

static void _copy_cert(cert_t* to, const cert_t* from) {
    memset(to, 0, sizeof(cert_t));
    if (from)
        memcpy(to, from, sizeof(cert_t));
}

void _copy_propose(cert_propose_t *to, const cert_propose_t *from) {
    _copy_cert((cert_t*) to, (cert_t*) from);
}

void _copy_accept(cert_accept_t *to, const cert_accept_t *from) {
    _copy_cert((cert_t*) to, (cert_t*) from);
}

void _copy_commit(cert_commit_t *to, const cert_commit_t *from) {
    _copy_cert((cert_t*) to, (cert_t*) from);
}

void _copy_prepare(cert_prepare_t *to, const cert_prepare_t *from) {
    _copy_cert((cert_t*) to, (cert_t*) from);
}

void _copy_promise(cert_promise_t *to, const cert_promise_t *from) {
    _copy_cert((cert_t*) to, (cert_t*) from);
}
