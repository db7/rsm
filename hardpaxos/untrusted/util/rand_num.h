/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef RAND_NUM_H_
#define RAND_NUM_H_

int rand_num(int min_num, int max_num);

#endif /* RAND_NUM_H_ */
