/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/ztime.h>
#include <stdlib.h>

extern void ztax_clock(ztime_t* ts);

int rand_num(int min_num, int max_num)
{
    static int initialized = 0;

    if (!initialized) {
        ztime_t now;
        ztax_clock(&now);

        srand(now.low);
        initialized = 1;
    }

    int low_num = 0, hi_num = 0;
    if (min_num < max_num) {
        low_num = min_num;
        hi_num  = max_num+1;
    } else {
        low_num = max_num+1;
        hi_num  = min_num;
    }
    return (rand() % (hi_num - low_num)) + low_num;
}
