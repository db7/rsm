/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef COPY_CERT_H_
#define COPY_CERT_H_

#include "../../certs.h"

void _copy_propose(cert_propose_t *to, const cert_propose_t *from);
void _copy_accept(cert_accept_t *to, const cert_accept_t *from);
void _copy_commit(cert_commit_t *to, const cert_commit_t *from);

void _copy_prepare(cert_prepare_t *to, const cert_prepare_t *from);
void _copy_promise(cert_promise_t *to, const cert_promise_t *from);

#endif /* COPY_CERT_H_ */
