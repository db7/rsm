/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _COMMANDER_H_
#define _COMMANDER_H_

#include <ztas/types.h>
#include <ztas/ds/idset.h>
#include "follower.h"
#include "candidate.h"
#include "../certs.h"
#include "../trusted/trusted.h"

typedef struct {
    int32_t pid;
    idset_t* replicas;
    trusted_t* trusted;

    int32_t accepted_num;
    int32_t quorum_size;
    int32_t decided;

    cert_accept_t* accepted[MAX_QUORUM];
    msg_propose_t* mpropose;
    cert_commit_t* ccommit;

    follower_t*  follower;
    candidate_t* candidate;
} commander_t;

/** create a commander. it is set to the invalid slot -1. */
commander_t* commander_init(const int32_t pid, const idset_t* replicas,
		trusted_t* trusted, follower_t* follower, candidate_t* candidate);

/** free commander memory */
void commander_fini(commander_t* commander);

/** receive request for slot, generate propose and send it */
void commander_propose(commander_t* commander, const msg_request_t* mrequest, const uint32_t slot);

/** generate propose for cslot of candidate and send it */
void commander_repropose(commander_t* commander, const uint32_t cslot, const uint32_t slot);

/** receive commit for current slot and probably send commit */
int commander_commit(commander_t* commander, const cert_accept_t* caccept);

/** return values */
#define COMMANDER_ERROR      -1
#define COMMANDER_OK          0
#define COMMANDER_DECIDED     1
#define COMMANDER_PREEMPTED   2
#define COMMANDER_IGNORED     3
#define COMMANDER_DECIDED_NOW 4

#endif /* _COMMANDER_H_ */
