/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include "replica.h"

#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/shrp.h>
#include <ztas/log.h>
#include <ztas/ztime.h>
#include <ztas/ds/idset.h>
#include <ztas/ds/queue.h>

#include <assert.h>
#include <pthread.h>
#include <sys/time.h>

#include "ds/sarr.h"
#include "util/copy_cert.h"
#include "../certs.h"
#include "../trusted/trusted.h"
#include "../trusted/cert_mac.h"
#include "follower.h"
#include "commander.h"

#include "../../rsm_msg.h"

extern void send_commit(int32_t pid, const cert_commit_t* ccommit);
extern void set_alarm(int32_t aid, int32_t ms);
extern void ztax_clock(ztime_t* ts);
extern void replica_deliver(replica_t* replica, const int32_t cmdsize, const void* cmd);
extern int  replica_snapshot(replica_t* replica, const int32_t snap_id);
extern void commander_send(commander_t* commander);

// forward declaration
void recv_accept(replica_t* replica, const cert_accept_t* caccept);

// sync primitives for async snapshotting
// note: no performance penalty because used only for SNAPSHOT messages
static pthread_mutex_t    snap_mutex;
static pthread_cond_t     snap_cv;
static int                snap_async_done = 1;

// ***************************************************************************
//                     Helper Functions
// ***************************************************************************
uint64_t __time_snap_request = 0;

uint64_t __now64() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t) tv.tv_sec)*1000000000 + (uint64_t)(tv.tv_usec)*1000;
}

static void wait_snap_done() {
    uint64_t before = __now64();
    // wait till snapshot is done
    pthread_mutex_lock(&snap_mutex);
    if (!snap_async_done) {
        // could be that snapshot is already done,
        // but if not, wait on snap_cv conditional variable
        pthread_cond_wait(&snap_cv, &snap_mutex);
    }
    assert(snap_async_done);
    pthread_mutex_unlock(&snap_mutex);
    uint64_t after = __now64();
    LOG1("[hardpaxos_replica] blocked until snapshot done:  %ld us\n",
            (after - before)/US);
    LOG1("[hardpaxos_replica] total time for snapshot done: %ld us\n",
            (after - __time_snap_request)/US);
}

void replica_execute(replica_t* replica, uint32_t cmd_mac, const int32_t size, const void* cmd) {
    replica_deliver(replica, size, cmd);
    // don't send to Client, because the RSM Server will do it automatically
}

void replica_commit(replica_t* replica, const cert_commit_t* ccommit) {
    int snap_id = getsnapid(ccommit->c);

    if (issnapmessage(ccommit->c)) {
        // this message is special SNAPSHOT message, prune it
        assert(snap_id == replica->last_snap_id);
        follower_prune(replica->follower, snap_id, ccommit);
        replica_prune(replica, snap_id);
    } else {
        // it's a regular message from client, execute it
        slot_t* slot = sarr_get(replica->follower->accepted, ccommit->c);
        assert(slot->caccept && slot->mpropose);
        assert(slot->caccept->cmd_mac == ccommit->cmd_mac );

        replica_execute(replica, slot->caccept->cmd_mac, slot->mpropose->size, slot->mpropose->cmd);

        if (ccommit->c > 0 && ccommit->c % TRUSTED_Z == 0) {
            // time to do a snapshot (c = 100, 200, ...)
            //   snapshot will be done in another thread, so use mutex
            pthread_mutex_lock(&snap_mutex);
            snap_async_done = 0;
            pthread_mutex_unlock(&snap_mutex);

            __time_snap_request = __now64();
            replica_snapshot(replica, ccommit->c);
        }
    }
}

// ***************************************************************************
//                     Proposal/Reproposal
// ***************************************************************************
commander_t* get_commander(replica_t* replica, const uint32_t slot) {
    if (slot < sarr_min(replica->commanders) || slot > sarr_max(replica->commanders)) {
        // slot is too old or too new
        assert(0 && "slot is too old or too new");
        return NULL;
    }

    commander_t* commander = (commander_t*) sarr_get(replica->commanders, slot);
    if (!commander) {
        commander = (commander_t*) queue_deq(replica->resting_commanders);
        sarr_put(replica->commanders, slot, commander);
    }

    return commander;
}

void replica_propose(replica_t* replica, const msg_request_t* mrequest) {
    if (replica->follower->leaderid != replica->pid) {
        return;
    }

    assert(replica->candidate && "candidate is null");
    if (replica->candidate->curr_slot < replica->candidate->slots_num) {
        // I still have slots to repropose
        replica_repropose(replica, replica->candidate->curr_slot);
        return;
    }

    assert(replica->trusted && "trusted is null");
    int s = trusted_get_a(replica->trusted) + 1;

    if (issnapmessage(s)) {
        // the message to be proposed is SNAPSHOT message
        // propose it before the message from client

        // wait till snapshot is done, if it's still in progress
        wait_snap_done();

        // TODO only for recovery test; delete later
        if (replica->dying) {
            // RSM server timouted -- kill myself right before snapshotting
            // this way we test recovery time for the worst case
            replica->dead = 1;
            LOG2("[hardpaxos replica] end killing leader, a = %d, c = %d\n",
            		trusted_get_a(replica->trusted), trusted_get_c(replica->trusted));
            return;
        }

        msg_request_t* mrequest = shrp_malloc(sizeof(msg_request_t));
        mrequest->size = 0;
        mrequest->cmd_mac = replica->last_snap_mac;

        commander_t* commander = get_commander(replica, s);
        commander_propose(commander, mrequest, s++);

        shrp_free(mrequest);
    }

    // now propose the message from client
    commander_t* commander = get_commander(replica, s);
    commander_propose(commander, mrequest, s);

    // if we were reproposing, now we are finished
    // reset candidate, so it doesn't occupy memory space
    if (replica->candidate->slots_num > 0) {
        LOG1("[hardpaxos replica] reset candidate (slots_num = %d)\n", replica->candidate->slots_num);
        candidate_clean(replica->candidate);
        replica->candidate->slots_num = 0;
    }
}

/* reproposal
 * cslot is slot \in [0..slots_num) of candidate (in general not the counter value)
 */
void replica_repropose(replica_t* replica, const int32_t cslot)
{
    if (replica->follower->leaderid != replica->pid)
        return;

    int32_t slot = trusted_get_a(replica->trusted) + 1;

    commander_t* commander = get_commander(replica, slot);
    commander_repropose(commander, cslot, slot);
}

// ***************************************************************************
//                     Snapshots/Catchup
// ***************************************************************************
void
replica_prune(replica_t* replica, const int32_t snap_id) {
    while (sarr_min(replica->commanders) <= snap_id) {
        commander_t* commander = sarr_next(replica->commanders);
        if (commander) {
            int r = queue_enq(replica->resting_commanders, (void*) commander);
            assert (r == QUEUE_OK);
        }
    }
}

void
replica_snapset(replica_t* replica, const int32_t snap_id)
{
    replica->last_snap_id  = snap_id;

    // TODO supposed to get snapshot_mac and save it

    replica->last_snap_mac = DUMMY_SNAP_MAC;
}

void
replica_snapdone(replica_t* replica, const int32_t snap_id)
{
    if (snap_id > replica->last_snap_id)
        replica_snapset(replica, snap_id);

    pthread_mutex_lock(&snap_mutex);
    snap_async_done = 1;
    pthread_cond_signal(&snap_cv);
    pthread_mutex_unlock(&snap_mutex);
}


void
replica_catchup(replica_t* replica, const cert_commit_t* snap_cert) {
    // TODO get this snapshot from somewhere
    uint32_t acquired_mac = DUMMY_SNAP_MAC;

    if (acquired_mac == snap_cert->cmd_mac) {
        int res = trusted_catchup(replica->trusted, snap_cert);
        if (res)
            return;

        int32_t snap_id = getsnapid(snap_cert->c);
        replica->last_snap_id  = snap_id;
        replica->last_snap_mac = acquired_mac;
        follower_prune(replica->follower, snap_id, snap_cert);
        replica_prune(replica, snap_id);

        // TODO apply snapshot to my state
    }
}

// ***************************************************************************
//                     Normal Runs
// ***************************************************************************
void
recv_request(replica_t* replica, const msg_request_t* mrequest)
{
    ztime_t now;
    ztax_clock(&now);
    ZTIME_SET(replica->last_client_active, now);
    replica_propose(replica, mrequest);
}

int recv_propose(replica_t* replica, const msg_propose_t* mpropose) {
    if (mpropose->cpropose.a <= trusted_get_a(replica->trusted)) {
        // I already received propose with this slot, ignore
        set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);
        return FOLLOWER_OK;
    }

    if (issnapmessage(mpropose->cpropose.a)) {
        // wait till snapshot is done, if it's still in progress
        wait_snap_done();

        // this message is special SNAPSHOT message;
        // check my latest snapshot_mac to be the same as in mpropose
        int32_t snap_id = getsnapid(mpropose->cpropose.a);
        if (replica->last_snap_id != snap_id ||
                replica->last_snap_mac != mpropose->cpropose.cmd_mac) {
            // snapshot is not the same as my local snapshot, ignore it
            printf("SNAPSHOT SLOT %d HAS OTHER ID (mine = %d, msg = %d) or MAC (mine = %d, msg = %d)\n",
            		mpropose->cpropose.a,
            		replica->last_snap_id, snap_id,
            		replica->last_snap_mac, mpropose->cpropose.cmd_mac);
            return FOLLOWER_ERROR;
        }
    }

    int res = follower_accept(replica->follower, mpropose);
    if (res == FOLLOWER_OK)
        set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);

    return res;
}

void
recv_accept(replica_t* replica, const cert_accept_t* caccept)
{
	commander_t* commander = sarr_get(replica->commanders, caccept->a);
    if (commander) {
        int res = commander_commit(commander, caccept);

        switch (res) {
        case COMMANDER_DECIDED_NOW:
            if (commander->mpropose) {
                set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);

                if (replica->candidate->curr_slot < replica->candidate->slots_num) {
                    // I still have slots to repropose, repropose one more slot...
                    replica_repropose(replica, replica->candidate->curr_slot);
                    // ..and re-send next non-committed slot
                    int32_t c = trusted_get_c(replica->trusted) + 1;
                    commander_t* commander = (commander_t*) sarr_get(replica->commanders, c);
                    if (commander)
                        commander_send(commander);
                }

                replica_commit(replica, commander->ccommit);
            }
            break;
        case COMMANDER_PREEMPTED:
            // bigger epoch, I'm too old
            // do nothing, because I will end in NOPROGRESS timeout anyway
            break;
        }
    }
}

int recv_commit(replica_t* replica, const cert_commit_t* ccommit) {
    int32_t snap_id = getsnapid(ccommit->c);

    if ( issnapmessage(ccommit->c) && snap_id > replica->last_snap_id ) {
        // this message is special SNAPSHOT message
        // and I don't have this new snapshot

        // NOTE: It could happen that I simply received this 121st message
        //       out of order, before 100-120th commit messages. In this case
        //       I will do unnecessary catchup. I take this risk -- this
        //       situation should be really rare.
        replica_catchup(replica, ccommit);
    }

    int res = follower_committed(replica->follower, ccommit);
    if (res == FOLLOWER_OK) {
        set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);
        replica_commit(replica, ccommit);
    }
    return res;
}

// ***************************************************************************
//                     Leader Election
// ***************************************************************************
void replica_prepare(replica_t* replica, const int32_t epoch) {
    candidate_prepare(replica->candidate, epoch);
}

int recv_prepare(replica_t* replica, const cert_prepare_t* cprepare) {
    int res = follower_promise(replica->follower, cprepare);
    if (res == FOLLOWER_OK) {
        LOG2("[hardpaxos replica] epoch %d started by %d\n", cprepare->e, cprepare->id);
        LOG1("    trusted.a       = %d\n", trusted_get_a(replica->trusted));
        LOG1("    trusted.c       = %d\n", trusted_get_c(replica->trusted));

        set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);
    }
    return res;
}

int recv_promise(replica_t* replica, const msg_promise_t* mpromise) {
    int res = candidate_start_epoch(replica->candidate,
    		&mpromise->cpromise, &mpromise->snap, mpromise->size, mpromise->slots);

    switch (res) {
    case CANDIDATE_ACCEPTED_NOW: {
        LOG1("[hardpaxos replica] I start epoch %d\n", mpromise->cpromise.e);
        LOG1("    trusted.a       = %d\n", trusted_get_a(replica->trusted));
        LOG1("    trusted.c       = %d\n", trusted_get_c(replica->trusted));

        set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);

        // rollback to last stable snapshot (current snapshot can be unstable)
        replica_snapset(replica, trusted_get_c(replica->trusted));

        int i;
        int need_catchup = 0;
        int latest_snap_id = TRUSTED_Z;
        cert_commit_t* latest_snap_cert = NULL;
        for (i = 0; i < replica->candidate->accepted_num; i++)
            if (replica->candidate->snaps[i] &&
                    replica->candidate->snaps[i]->c > latest_snap_id) {
                latest_snap_id   = replica->candidate->snaps[i]->c;
                latest_snap_cert = replica->candidate->snaps[i];
            }

        if (latest_snap_id != TRUSTED_Z) {
            for (i = 0; i < replica->candidate->accepted_num; i++)
                if (replica->candidate->snaps[i] &&
                        replica->candidate->snaps[i]->c < latest_snap_id) {
                    need_catchup = 1;
                    send_commit(replica->candidate->guards[i]->id, latest_snap_cert);
                } else if (!replica->candidate->snaps[i]) {
                    need_catchup = 1;
                    send_commit(replica->candidate->guards[i]->id, latest_snap_cert);
                }
        }

        if (!need_catchup) {
        	LOG1("[hardpaxos replica] start reproposing %d slots\n", replica->candidate->slots_num);
            for (i = 0; i < replica->candidate->slots_num; i++)
                replica_repropose(replica, i);
        }

        LOG1("[hardpaxos replica] epoch %d started, ready for normal execution\n", mpromise->cpromise.e);

        break;
    }
    case CANDIDATE_PREEMPTED: {
        // bigger epoch, I'm too old
        // do nothing, because I will end in NOPROGRESS timeout anyway
        break;
    }
    }

    return res;
}

// ***************************************************************************
//                     Initialize/Finalize
// ***************************************************************************
replica_t*
replica_init(int32_t pid, const idset_t* replicas, const trusted_t* trusted, /* unused*/ const int32_t max_inflight) {
    LOG1("[hardpaxos] snapshot period: %d\n", SNAP_PERIOD);

    replica_t* replica = (replica_t*) malloc(sizeof(replica_t));

    replica->pid = pid;
    replica->replicas = idset_init_copy(replicas);
    replica->trusted = (trusted_t*) trusted;

    replica->follower  = follower_init(replica->pid, replica->trusted);
    replica->candidate = candidate_init(replica->pid, replica->replicas, replica->trusted, replica->follower);

    replica->max_inflight = MAX_SLOTS;
    replica->commanders = sarr_init(1, replica->max_inflight);
    replica->resting_commanders = queue_create(replica->max_inflight);
    int i;
    for (i = 1; i <= replica->max_inflight; ++i) {
        commander_t* commander = commander_init(replica->pid, replica->replicas, replica->trusted, replica->follower, replica->candidate);
        queue_enq(replica->resting_commanders, (void*) commander);
    }

    replica->last_snap_id  = 0;
    replica->last_snap_mac = DUMMY_SNAP_MAC;

    replica->dead  = 0;
    replica->dying = 0;

    ZTIME_ZERO(replica->last_client_active);
    set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);

    return replica;
}

void
replica_fini(replica_t* replica) {
    idset_fini(replica->replicas);

    commander_t* commander = NULL;
    while ((commander = sarr_next(replica->commanders))
            || sarr_last(replica->commanders))
        if (commander) commander_fini(commander);
    sarr_fini(replica->commanders);

    while (!queue_empty(replica->resting_commanders)) {
        commander = (commander_t*) queue_deq(replica->resting_commanders);
        commander_fini(commander);
    }
    queue_free(replica->resting_commanders);

    follower_fini(replica->follower);
    candidate_fini(replica->candidate);
    free(replica);
}

// ***************************************************************************
//                     Messages
// ***************************************************************************
int32_t
replica_recv(replica_t* replica, const size_t size, const cert_t* msg)
{
    if (replica->dead)
        return 0;

    // we check that size is correct before passing message further
    switch (msg->type) {
    case HP_REQUEST: {
        msg_request_t* mrequest = (msg_request_t*) msg;
        if (size != sizeof(msg_request_t) + mrequest->size)
            return 0;

        recv_request(replica, mrequest);
        return 1;
    }
    case HP_PROPOSE: {
        msg_propose_t* mpropose = (msg_propose_t*) msg;
        if (size != sizeof(msg_propose_t) + mpropose->size)
            return 0;

        recv_propose(replica, mpropose);
        return 1;
    }
    case HP_ACCEPT: // no break
    case HP_ACCEPT_COMMITTED: {
        cert_accept_t* caccept = (cert_accept_t*) msg;
        if (size != sizeof(cert_accept_t))
            return 0;

        recv_accept(replica, caccept);
        return 1;
    }
    case HP_COMMIT: {
        cert_commit_t* ccommit = (cert_commit_t*) msg;
        if (size != sizeof(cert_commit_t))
            return 0;

        recv_commit(replica, ccommit);
        return 1;
    }
    case HP_PREPARE: {
        cert_prepare_t* cprepare = (cert_prepare_t*) msg;
        if (size != sizeof(cert_prepare_t))
            return 0;

        recv_prepare(replica, cprepare);
        return 1;
    }
    case HP_PROMISE: {
        msg_promise_t* mpromise = (msg_promise_t*) msg;
        if (size != sizeof(msg_promise_t) + mpromise->size)
            return 0;

        recv_promise(replica, mpromise);
        return 1;
    }
    default:
        ;
    }
    return 0;
}

int
replica_trig(replica_t* replica, int32_t aid)
{
    if (replica->dead)
        return 0;

    switch (aid) {
    case NOPROGRESS_ALARM:
        set_alarm(NOPROGRESS_ALARM, replica->follower->noprogress_timeout);

        ztime_t now;
        ztime_t noprogress_duration;
        ztax_clock(&now);
        ZTIME_DUR(replica->last_client_active, now, noprogress_duration);

        if (trusted_get_e(replica->trusted) != 0
                && ZTIME_TS(noprogress_duration) > ((uint64_t)replica->follower->noprogress_timeout) * MS) {
            // I had some progress after client asked for something, and I completed normal run
            return 1;
        }

        int32_t curr_epoch = trusted_get_e(replica->trusted);
        if (replica->candidate->last_epoch < curr_epoch)
            replica->candidate->last_epoch = curr_epoch;

        uint32_t real_epoch = replica->candidate->last_epoch / MAXPROCESSES;
        uint32_t new_epoch = (real_epoch + 1) * MAXPROCESSES + replica->pid;

        LOG2("[hardpaxos replica] replica %d proposes new epoch %d\n", replica->pid, new_epoch);
        LOG1("    trusted.a       = %d\n", trusted_get_a(replica->trusted));
        LOG1("    trusted.c       = %d\n", trusted_get_c(replica->trusted));

        replica_prepare(replica, new_epoch);
        return 1;

    default:
        return 0;
    }
}

int32_t
replica_pid(const replica_t* replica) {
    return replica->pid;
}
