/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef CANDIDATE_H_
#define CANDIDATE_H_

#include <ztas/types.h>
#include <ztas/ds/idset.h>
#include "../certs.h"
#include "follower.h"

typedef struct {
    int32_t pid;
    idset_t* replicas;
    int32_t quorum_size;
    trusted_t* trusted;

    int32_t last_epoch; // last seen epoch, for proposing epochs
    int32_t epoch_accepted;

    int32_t accepted_num;
    int32_t slots_num;                          // how many slots to repropose
    int32_t curr_slot;                          // current slot to be reproposed
    cert_commit_t*  snaps[MAX_QUORUM];              // last snapshots
    cert_promise_t* guards[MAX_QUORUM];             // guards; used for proposals
    slot_t     slots[MAX_SLOTS][MAX_QUORUM];    // slots to be reproposed

    follower_t* follower;
} candidate_t;

int candidate_prepare(candidate_t* candidate, const int32_t epoch);
int candidate_start_epoch(candidate_t* candidate, const cert_promise_t* promise, const cert_commit_t* last_snap_cert, const int32_t size, const void* slots);

candidate_t* candidate_init(const int32_t pid, const idset_t* replicas, trusted_t* trusted, follower_t* follower);
void candidate_fini(candidate_t* candidate);
void candidate_clean(candidate_t* candidate);

/** return values */
#define CANDIDATE_ERROR       -1
#define CANDIDATE_OK           0
#define CANDIDATE_PREEMPTED    1
#define CANDIDATE_IGNORED      2
#define CANDIDATE_ACCEPTED     3
#define CANDIDATE_ACCEPTED_NOW 4

#endif /* CANDIDATE_H_ */
