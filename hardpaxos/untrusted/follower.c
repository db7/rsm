/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <assert.h>
#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/log.h>
#include <ztas/shrp.h>
#include <ztas/ds/queue.h>
#include "util/copy_cert.h"
#include "util/rand_num.h"
#include "ds/sarr.h"
#include "../certs.h"
#include "../trusted/trusted.h"
#include "follower.h"

extern void send_accept(int32_t pid, const cert_accept_t* accept);
extern void send_promise(int32_t pid, const msg_promise_t* mpromise);

// min & max No Progress timeouts (in ms)
#define NOPROGRESS_MIN 500
#define NOPROGRESS_MAX 800

static void follower_free_slot(follower_t* follower, slot_t* slot) {
    if (slot) {
        if (slot->caccept)  shrp_free(slot->caccept);
        if (slot->mpropose) shrp_free(slot->mpropose);
        slot->caccept  = NULL;
        slot->mpropose = NULL;

        // instead of freeing a slot, put it in a queue
        queue_enq(follower->unused_slots, (void*) slot);
    }
}

static void follower_free_accepted(follower_t* follower) {
    if (follower->accepted) {
        int i = sarr_min(follower->accepted);
        for (; i <= sarr_max(follower->accepted); ++i)
            follower_free_slot(follower, sarr_pop(follower->accepted, i));

        sarr_fini(follower->accepted);
    }
}

// ***************************************************************************
//                     Initialize/Finalize
// ***************************************************************************
void follower_reset(follower_t* follower) {
    follower->amount_accepted = trusted_get_a(follower->trusted);
    follower->amount_commited = trusted_get_c(follower->trusted);

    // roll forward the accepted array
    int32_t snapid = getsnapid(follower->amount_accepted);
    while (sarr_min(follower->accepted) <= snapid)
        follower_free_slot(follower, sarr_next(follower->accepted));
}

follower_t*
follower_init(const int32_t pid, trusted_t* trusted)
{
    follower_t* follower = (follower_t*) malloc(sizeof(follower_t));

    follower->pid = pid;
    follower->leaderid = NONE_ID;
    follower->accepted = NULL;
    follower->last_snap_cert = NULL;
    follower->trusted = trusted;

    // TODO delete after tests?
    if (follower->pid == 0) {
        LOG("[hardpaxos follower] Replica 0 will become a leader\n");
    follower->noprogress_timeout = 1000;
    } else {
        LOG1("[hardpaxos follower] Replica %d will be a follower\n", follower->pid);
        follower->noprogress_timeout = rand_num(3000, 4000);//50000;
    }
//    follower->noprogress_timeout = rand_num(NOPROGRESS_MIN, NOPROGRESS_MAX);
    LOG2("[hardpaxos follower] Follower %d has NOPROGRESS timeout %d ms\n", pid, follower->noprogress_timeout);

    follower->accepted = sarr_init(1, MAX_SLOTS);
    follower->unused_slots = queue_create(MAX_SLOTS);
    int i;
    for (i = 1; i <= MAX_SLOTS; ++i) {
        slot_t* slot = shrp_malloc(sizeof(slot_t));
        slot->caccept  = NULL;
        slot->mpropose = NULL;
        queue_enq(follower->unused_slots, (void*) slot);
    }

    follower_reset(follower);
    return follower;
}

void
follower_fini(follower_t* follower)
{
    follower_free_accepted(follower);
    if (follower->last_snap_cert)
        shrp_free(follower->last_snap_cert);

    while (!queue_empty(follower->unused_slots)) {
        slot_t* slot = (slot_t*) queue_deq(follower->unused_slots);
        if (slot->caccept)  shrp_free(slot->caccept);
        if (slot->mpropose) shrp_free(slot->mpropose);
        shrp_free(slot);
    }
    queue_free(follower->unused_slots);

    free(follower);
}

// ***************************************************************************
//                     Change State functions
// ***************************************************************************
void follower_accept_local(follower_t* follower, const cert_accept_t* caccept, const msg_propose_t* mpropose) {
    int s = mpropose->cpropose.a;

    // when accepting, the slot must be overwritten
    slot_t* slot = sarr_pop(follower->accepted, s);
    follower_free_slot(follower, slot);

    // get some unused slot and put it in "accepted"
    slot = (slot_t*) queue_deq(follower->unused_slots);
    shrp_hold((void*) caccept);
    shrp_hold((void*) mpropose);
    slot->caccept  = (cert_accept_t*) caccept;
    slot->mpropose = (msg_propose_t*)  mpropose;

    int r = sarr_put(follower->accepted, s, slot);
    assert(r == SARR_OK && "Follower could not add slot to its accepted!");
    follower->amount_accepted++;
}

void follower_commit_local(follower_t* follower, const cert_commit_t* ccommit) {
    follower->amount_commited++;
}

// snap_id is the highest slot to prune
void follower_prune(follower_t* follower, const int32_t snap_id, const cert_commit_t* ccommit) {
    if (follower->last_snap_cert)
        shrp_free(follower->last_snap_cert);
    shrp_hold((void*) ccommit);
    follower->last_snap_cert = (cert_commit_t*) ccommit;

    while (sarr_min(follower->accepted) <= snap_id)
        follower_free_slot(follower, sarr_next(follower->accepted));

    follower->amount_accepted = trusted_get_a(follower->trusted);
    follower->amount_commited = trusted_get_c(follower->trusted);
}

// ***************************************************************************
//                     Messages
// ***************************************************************************
int follower_accept(follower_t* follower, const msg_propose_t* mpropose) {
    cert_propose_t* cpropose = (cert_propose_t*) &mpropose->cpropose;

    if (cpropose->id != follower->leaderid)
        return FOLLOWER_ERROR;

    cert_accept_t* caccept = shrp_malloc(sizeof(cert_accept_t));

    int res = trusted_accept(follower->trusted, cpropose, caccept);
    if (res)
        return FOLLOWER_ERROR;

    follower_accept_local(follower, caccept, mpropose);
    send_accept(follower->leaderid, caccept);

    shrp_free(caccept);
    return FOLLOWER_OK;
}

int follower_committed(follower_t* follower, const cert_commit_t* ccommit) {
    if (ccommit->id != follower->leaderid)
        return FOLLOWER_ERROR;
    if (!sarr_get(follower->accepted, ccommit->c))
        return FOLLOWER_ERROR;

    int res = trusted_committed(follower->trusted, ccommit);
    if (res)
        return FOLLOWER_ERROR;

    follower_commit_local(follower, ccommit);
    return FOLLOWER_OK;
}

int follower_promise(follower_t* follower, const cert_prepare_t* cprepare) {
    cert_promise_t* cpromise = shrp_malloc(sizeof(cert_promise_t));
    int res = trusted_promise(follower->trusted, cprepare, cpromise);
    if (res)
        return FOLLOWER_ERROR;

    if (cpromise->id == NONE_ID) {
        // followers sends his promise to inform the other guy that he lags behind
        msg_promise_t* mpromise = shrp_malloc(sizeof(msg_promise_t));
        _copy_promise(&mpromise->cpromise, cpromise);
        _copy_commit(&mpromise->snap, follower->last_snap_cert);
        mpromise->size = 0;

        send_promise(cprepare->id, mpromise);

        shrp_free(cpromise);
        shrp_free(mpromise);
        return FOLLOWER_ERROR;
    }

    follower->leaderid = cprepare->id;

    // go through the accepted slots to know the size
    // ...and also update accepted slots
    int32_t size = 0;
    int i;
    int min = sarr_min(follower->accepted);
    int max = cpromise->a; // the highest slot ever seen
    for (i = min; i <= max; ++i) {
        slot_t* slot = sarr_get(follower->accepted, i);
        assert(slot && "Follower's accepted is corrupted");
        trusted_update(follower->trusted, slot->caccept);

        size += sizeof(cert_accept_t) + sizeof(msg_propose_t) + slot->mpropose->size;
    }

    msg_promise_t* mpromise = shrp_malloc(sizeof(msg_promise_t) + size);
    _copy_promise(&mpromise->cpromise, cpromise);
    _copy_commit(&mpromise->snap, follower->last_snap_cert);
    mpromise->size = size;

    // now that we know the size, go again through accepted
    // and copy slots directly to promise message
    int offset = 0;
    for (i = min; i <= max; ++i) {
        // add one more slot
        slot_t* slot = sarr_get(follower->accepted, i);
        memcpy(mpromise->slots + offset, slot->caccept, sizeof(cert_accept_t));
        memcpy(mpromise->slots + offset + sizeof(cert_accept_t), slot->mpropose, sizeof(msg_propose_t));
        memcpy(mpromise->slots + offset + sizeof(cert_accept_t) + sizeof(msg_propose_t),
                slot->mpropose->cmd, slot->mpropose->size);
        offset += sizeof(cert_accept_t) + sizeof(msg_propose_t) + slot->mpropose->size;
    }

    send_promise(cprepare->id, mpromise);

    follower_reset(follower);

    shrp_free(cpromise);
    shrp_free(mpromise);
    return FOLLOWER_OK;
}
