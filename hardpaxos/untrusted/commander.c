/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include "commander.h"
#include <ztas/log.h>
#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/shrp.h>
#include <assert.h>
#include "util/copy_cert.h"
#include "../trusted/trusted.h"

extern void send_propose(int32_t pid, const msg_propose_t* mpropose);
extern void send_commit(int32_t pid, const cert_commit_t* ccommit);
extern void send_accept(int32_t pid, cert_accept_t* caccept);


// ***************************************************************************
//                     Initialize/Finalize
// ***************************************************************************
commander_t*
commander_init(const int32_t pid, const idset_t* replicas, trusted_t* trusted, follower_t* follower, candidate_t* candidate)
{
    commander_t* commander = (commander_t*) malloc(sizeof(commander_t));

    commander->pid = pid;
    commander->replicas = idset_init_copy(replicas);
    commander->trusted = trusted;
    commander->accepted_num = 0;
    commander->quorum_size = idset_size(replicas) / 2 + 1;
    commander->decided = 0;

    commander->mpropose = NULL;
    commander->ccommit  = NULL;
    int i;
    for (i = 0; i < MAX_QUORUM; i++)
        commander->accepted[i] = NULL;

    commander->follower  = follower;
    commander->candidate = candidate;

    return commander;
}

void
commander_fini(commander_t* commander)
{
    if (commander->mpropose)
        shrp_free(commander->mpropose);
    if (commander->ccommit)
        shrp_free(commander->ccommit);
    int i;
    for (i = 0; i < MAX_QUORUM; i++)
        if (commander->accepted[i])
            shrp_free(commander->accepted[i]);

    idset_fini(commander->replicas);
    free(commander);
}

void
commander_reset(commander_t* commander)
{
    commander->accepted_num = 0;
    int i;
    for (i = 0; i < MAX_QUORUM; i++) {
        if (commander->accepted[i])
            shrp_free(commander->accepted[i]);
        commander->accepted[i] = NULL;
    }
    commander->decided = 0;

    if (commander->mpropose)
        shrp_free(commander->mpropose);
    if (commander->ccommit)
        shrp_free(commander->ccommit);

    commander->ccommit  = NULL;
    commander->mpropose = NULL;
}

// ***************************************************************************
//                     Messages
// ***************************************************************************
void
commander_send(commander_t* commander)
{
    idset_it_t it;
    idset_it_init(commander->replicas, &it);
    int32_t pid;
    while ((pid = idset_it_next(&it)) >= 0) {
        // broadcast to everyone except me
        if (pid != commander->pid)
            send_propose(pid, commander->mpropose);
    }
}

void
commander_propose(commander_t* commander, const msg_request_t* mrequest, const uint32_t slot)
{
	commander_reset(commander);

    msg_propose_t* mpropose = (msg_propose_t*) mrequest;
    cert_accept_t* caccept = shrp_malloc(sizeof(cert_accept_t));

    int res = trusted_propose(commander->trusted, slot,
            commander->candidate->accepted_num,
            commander->candidate->guards,
            mrequest->cmd_mac, &mpropose->cpropose, caccept);
    if (res != 0) {
        shrp_free(caccept);
        return;
    }

    shrp_hold((void*) mpropose);
    commander->mpropose = (msg_propose_t*) mpropose;

    commander->accepted[0] = (cert_accept_t*) caccept;
    commander->accepted_num++;

    follower_accept_local(commander->follower, caccept, mpropose);
    commander_send(commander);
}

void
commander_repropose(commander_t* commander, const uint32_t cslot, const uint32_t slot)
{
	commander_reset(commander);

    cert_propose_t* cpropose = shrp_malloc(sizeof(cert_propose_t));
    cert_accept_t*  caccept  = shrp_malloc(sizeof(cert_accept_t));

    uint32_t amount = 0;
    cert_t* certs[MAX_QUORUM];

    int i;
    for (i = 0; i < commander->candidate->accepted_num; i++) {
        if (commander->candidate->slots[cslot][i].caccept) {
            shrp_hold(commander->candidate->slots[cslot][i].caccept);
            certs[amount] = (cert_t*) commander->candidate->slots[cslot][i].caccept;
            amount++;
        } else {
            shrp_hold(commander->candidate->guards[i]);
            certs[amount] = (cert_t*) commander->candidate->guards[i];
            amount++;
        }
    }

    // send NULL as cmd, trusted module will suggest the command himself
    int res = trusted_propose(commander->trusted, slot, amount,
    		certs, 0, cpropose, caccept);
    if (res != 0)
        goto label_done;

    int32_t size = 0;
    void* cmd = NULL;

    if (!issnapmessage(slot)) {
        // find the appropriate cmd
        // but only if it is not a special SNAPSHOT message (which has size = 0)
        for (i = 0; i < commander->candidate->accepted_num; i++) {
            if (commander->candidate->slots[cslot][i].caccept)
                if (commander->candidate->slots[cslot][i].caccept->cmd_mac == cpropose->cmd_mac) {
                    size = commander->candidate->slots[cslot][i].mpropose->size;
                    cmd  = commander->candidate->slots[cslot][i].mpropose->cmd;
                    break;
                }
        }
        assert(size != 0 && cmd != NULL && "Repropose: cannot find command with specified cmd_mac!\n");
    }

    commander->candidate->curr_slot++;

    commander->mpropose = shrp_malloc(sizeof(msg_propose_t) + size);
    _copy_propose(&commander->mpropose->cpropose, cpropose);
    commander->mpropose->size = size;
    if (size > 0)
        memcpy(commander->mpropose->cmd, cmd, size);

    commander->accepted[0] = (cert_accept_t*) caccept;
    commander->accepted_num++;

    commander_send(commander);
    follower_accept_local(commander->follower, caccept, commander->mpropose);

    idset_it_t it;
    idset_it_init(commander->replicas, &it);
    int32_t pid;
    while ((pid = idset_it_next(&it)) >= 0) {
        // broadcast to everyone except me
        if (pid != commander->pid)
            send_propose(pid, commander->mpropose);
    }

    for (i = 0; i < amount; i++)
    	if (certs[i]->id != commander->pid && certs[i]->type == HP_ACCEPT_COMMITTED) {
    		// caccept was already committed; the follower won't answer
    		// to propose, so send "his" answer locally
    		send_accept(commander->pid, (cert_accept_t*) certs[i]);
    	}

    label_done:
        shrp_free(cpropose);
        shrp_free(caccept);
        for (i = 0; i < amount; i++)
            shrp_free(certs[i]);
}

int
commander_commit(commander_t* commander, const cert_accept_t* caccept)
{
    if (!commander->mpropose)
        return COMMANDER_ERROR;
    if (caccept->e > commander->mpropose->cpropose.e)
        return COMMANDER_PREEMPTED;
    if (caccept->a != commander->mpropose->cpropose.a || caccept->e != commander->mpropose->cpropose.e)
        return COMMANDER_IGNORED;

    // add caccept to accepted certificates
    shrp_hold((void*) caccept);
    commander->accepted[commander->accepted_num] = (cert_accept_t*) caccept;
    commander->accepted_num++;

    if (!commander->decided) {
        if (commander->accepted_num >= commander->quorum_size) {
            if (commander->ccommit)
                shrp_free(commander->ccommit);
            commander->ccommit = shrp_malloc(sizeof(cert_commit_t));

            int res = trusted_commit(commander->trusted, commander->accepted_num, commander->accepted, commander->ccommit);
            if (res)
                return COMMANDER_ERROR;

            idset_it_t it;
            idset_it_init(commander->replicas, &it);
            int32_t pid;
            while ((pid = idset_it_next(&it)) >= 0) {
                // broadcast to everyone except me
                if (pid != commander->pid)
                    send_commit(pid, commander->ccommit);
            }

            commander->decided = 1;
            follower_commit_local(commander->follower, commander->ccommit);

            return COMMANDER_DECIDED_NOW;
        } else
            return COMMANDER_OK;
    }
    return COMMANDER_DECIDED;
}
