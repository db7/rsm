/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include "trusted.h"
#include <string.h>

#ifdef NODEPENDENCIES
extern void* malloc(size_t size);
#else // NODEPENDENCIES
#include <stdlib.h>
#endif // NODEPENDENCIES

extern uint32_t check_integrity(trusted_t* trusted, cert_t* cert);

static uint32_t __maj(trusted_t* trusted) {
    return trusted->N / 2 + 1;
}

static uint32_t __leaderof(uint32_t e) {
    return e % MAXPROCESSES;
}

static uint32_t __check_certs(const uint32_t amount, cert_t** certs) {
    uint32_t i, j;
    for (i = 0; i < amount; i++)
        if (!certs[i]) {
            // no certificate provided
            return BAD_CERT_ERROR;
        }

    for (i = 0; i < amount; i++)
        for (j = i+1; j < amount; j++)
            if (certs[i]->id == certs[j]->id) {
                // some certificate is duplicated
                return DUPLICATE_CERT_ERROR;
            }

    return 0;
}

static uint32_t __check_integrity(trusted_t* trusted, cert_t* cert) {
	return check_integrity(trusted, cert);
}

static void __seal(trusted_t* trusted, cert_t* cert) {
    // no need to do anything; sealing happens elsewhere
    //create_mac(trusted, cert);
}

trusted_t* trusted_init(const uint32_t N, const uint32_t id, const uint32_t key_size, const char *key) {
    trusted_t* trusted = (trusted_t*) malloc(sizeof(trusted_t));
    trusted->N = N;
    trusted->id = id;

#if 0
    trusted->key_size = key_size;
    trusted->key = malloc(trusted->key_size);
    memcpy(trusted->key, key, trusted->key_size);
#endif

    trusted->e = trusted->a = trusted->c = 0;
    trusted->highest_a  = 0;
    trusted->prev_a     = 0;
    trusted->prev_e     = 0;

    return trusted;
}

void trusted_fini(trusted_t* trusted) {
#if 0
    free(trusted->key);
#endif
}

// ***************************************************************************
//                 Normal Execution: Leader
// ***************************************************************************

// Propose() and Repropose() are in the same function
uint32_t trusted_propose(trusted_t* trusted, const uint32_t slot, const uint32_t amount, cert_t** certs,
		uint32_t cmd_mac, cert_propose_t *cpropose, cert_accept_t *caccept) {
    if (__leaderof(trusted->e) != trusted->id) {
        // I am not the Leader
        return LEADER_ERROR;
    }
    if (trusted->e <= 0) {
        // cannot propose when we don't even have an epoch
        return EPOCH_ERROR;
    }
    if (trusted->a > trusted->c + TRUSTED_K) {
        // gap between A and C is too big
        return SYNC_ERROR;
    }
    if (slot != trusted->a + 1) {
        // cannot propose out of order
        return BAD_CERT_ERROR;
    }

    uint32_t r = __check_certs(amount, certs);
    if (r)
        return r;

    // find out how many certificates are good
    uint32_t i;
    uint32_t good = 0;
    uint32_t high_e = 0;
    uint32_t cert_e = NONE_ID;
    for (i = 0; i < amount; i++) {
        if (certs[i]->type != HP_ACCEPT && certs[i]->type != HP_ACCEPT_COMMITTED &&
        		certs[i]->type != HP_PROMISE)
            continue;
        // is it a bad command?
        if ( (certs[i]->type == HP_ACCEPT || certs[i]->type == HP_ACCEPT_COMMITTED) &&
                (certs[i]->e != trusted->e || certs[i]->a != slot
                		|| certs[i]->c >= trusted->e)  // (caccept.c = caccept.e_a) >= E
           )
            continue;
        // is it a bad guard?
        if ( certs[i]->type == HP_PROMISE &&
                (certs[i]->e != trusted->e || certs[i]->a >= slot) )
            continue;

        if (__check_integrity(trusted, (cert_t*) certs[i]) != 0)
            continue;

        // memorise the command accepted in the biggest epoch, if any
        if ((certs[i]->type == HP_ACCEPT || certs[i]->type == HP_ACCEPT_COMMITTED)
        		&& certs[i]->c > high_e) { // caccept.c = caccept.e_a
            high_e = certs[i]->c;
            cert_e = i;
        }

        good++;
    }
    if (good < __maj(trusted))
        return BAD_CERT_ERROR;

    uint32_t real_cmd_mac = cmd_mac;
    if (cert_e != NONE_ID) {
        // there was at least one command, must repropose it
        real_cmd_mac = certs[cert_e]->cmd_mac;
    }

    trusted->a++;

    memset(cpropose, 0, sizeof(cert_propose_t));
    cpropose->type = HP_PROPOSE;
    cpropose->id = trusted->id;
    cpropose->e = trusted->e;
    cpropose->a = trusted->a;
    cpropose->cmd_mac = real_cmd_mac;
    __seal(trusted, (cert_t*) cpropose);

    memset(caccept, 0, sizeof(cert_accept_t));
    caccept->type = HP_ACCEPT;
    caccept->id = trusted->id;
    caccept->e = trusted->e;
    caccept->c = trusted->e;	// caccept.c = caccept.e_a
    caccept->a = trusted->a;
    caccept->cmd_mac = real_cmd_mac;
    __seal(trusted, (cert_t*) caccept);

    return 0;
}

uint32_t trusted_commit(trusted_t* trusted, const uint32_t amount, cert_accept_t** caccepts, cert_commit_t *ccommit) {
    if (__leaderof(trusted->e) != trusted->id) {
        // I am not the Leader
        return LEADER_ERROR;
    }

    uint32_t r = __check_certs(amount, caccepts);
    if (r)
        return r;

    // find out how many certificates are good
    uint32_t i;
    uint32_t good = 0;
    for (i = 0; i < amount; i++)
        if ((caccepts[i]->type == HP_ACCEPT || caccepts[i]->type == HP_ACCEPT_COMMITTED) &&
                caccepts[i]->e == trusted->e &&
                caccepts[i]->a == trusted->c+1 &&
                __check_integrity(trusted, (cert_t*) caccepts[i]) == 0)
            good++;

    // compare cmd_macs of certificates
    // TODO I loosened the implementation because Encoding Compiler
    //      does something crazy with arrays allocated on stack.
    //      It does not affect safety, but liveness can hamper because
    //      leader will always commit with incorrect MACs and never succeed.
    uint32_t same_cmd = 0;
    uint32_t got__maj = 0;
    uint32_t cmd_mac = 0;

    if (amount > 0) {
        cmd_mac = caccepts[0]->cmd_mac;
        same_cmd = 1;
    }

    for (i = 1; i < amount; i++)
       if (cmd_mac == caccepts[i]->cmd_mac) {
            same_cmd++;
            if (same_cmd >= __maj(trusted)) {
                got__maj = 1;
                break;
            }
        }

#if 0
    // ** previous (correct) implementation ** //
    uint32_t j;
    uint32_t same_cmd[amount];
    uint32_t got__maj = 0;
    uint32_t cmd_mac = 0;
    for (i = 0; i < amount; i++)
        same_cmd[i] = 1;

    for (i = 0; i < amount; i++)
        for (j = i+1; j < amount; j++)
            if (caccepts[i]->cmd_mac == caccepts[j]->cmd_mac) {
                same_cmd[i]++;
                if (same_cmd[i] >= __maj(trusted)) {
                    got__maj = 1;
                    cmd_mac = caccepts[i]->cmd_mac;
                    break;
                }
            }
#endif

    // not enough good certificates or they are different
    if (good < __maj(trusted) || !got__maj)
        return BAD_CERT_ERROR;

    trusted->c++;

    memset(ccommit, 0, sizeof(cert_commit_t));
    ccommit->id = trusted->id;
    ccommit->type = HP_COMMIT;
    ccommit->e = trusted->e;
    ccommit->c = trusted->c;
    ccommit->cmd_mac = cmd_mac;
    __seal(trusted, (cert_t*) ccommit);

    return 0;
}

// ***************************************************************************
//                 Normal Execution: Follower
// ***************************************************************************

uint32_t trusted_accept(trusted_t* trusted, const cert_propose_t *cpropose, cert_accept_t *caccept) {
    if (cpropose->e != trusted->e)
        return EPOCH_ERROR;
    if (trusted->a > trusted->c + TRUSTED_K) {
        // gap between A and C is too big
        return SYNC_ERROR;
    }
    if (cpropose->type != HP_PROPOSE ||
            cpropose->a != trusted->a+1 ||
            __check_integrity(trusted, (cert_t*) cpropose) != 0) {
        // incorrect A counter or message is corrupted!
        return BAD_CERT_ERROR;
    }

    trusted->a++;

    memset(caccept, 0, sizeof(cert_accept_t));
    caccept->type = HP_ACCEPT;
    caccept->id = trusted->id;
    caccept->e = trusted->e;
    caccept->c = trusted->e;	// caccept.c = caccept.e_a
    caccept->a = trusted->a;
    caccept->cmd_mac = cpropose->cmd_mac;
    __seal(trusted, (cert_t*) caccept);

    return 0;
}

uint32_t trusted_committed(trusted_t* trusted, const cert_commit_t *ccommit) {
    if (ccommit->e != trusted->e)
        return EPOCH_ERROR;

    if (ccommit->type != HP_COMMIT ||
            ccommit->c != trusted->c+1 ||
            __check_integrity(trusted, (cert_t*) ccommit) != 0) {
        // incorrect C counter or message is corrupted!
        return BAD_CERT_ERROR;
    }

    trusted->c++;
    return 0;
}

// ***************************************************************************
//                 Leader Election: Candidate
// ***************************************************************************

uint32_t trusted_prepare(trusted_t* trusted, const uint32_t epoch, cert_prepare_t *cprepare, cert_promise_t *cpromise) {
    if (epoch < trusted->e)
        return EPOCH_ERROR;
    if (__leaderof(epoch) != trusted->id) {
        // epoch provided by untrusted part is not for this process!
        return EPOCH_ERROR;
    }

    trusted->prev_e = trusted->e;
    trusted->prev_a = trusted->a;
    trusted->e = epoch;

    memset(cprepare, 0, sizeof(cert_prepare_t));
    cprepare->type = HP_PREPARE;
    cprepare->id = trusted->id;
    cprepare->e = trusted->e;
    __seal(trusted, (cert_t*) cprepare);

    if (trusted->highest_a < trusted->a)
        trusted->highest_a = trusted->a;

    memset(cpromise, 0, sizeof(cert_promise_t));
    cpromise->type = HP_PROMISE;
    cpromise->id = trusted->id;
    cpromise->e = trusted->e;
    cpromise->a = trusted->highest_a;
    __seal(trusted, (cert_t*) cpromise);

    uint32_t s = 0;
    if (trusted->highest_a > TRUSTED_Z)
    	s = getsnapid(trusted->highest_a);

    // NB: unlike followers in Promise(), leader rolls back to last snapshotted
    //     slot -- to be able to repropose
    trusted->a = s;
    trusted->c = s;
    return 0;
}

// ***************************************************************************
//                Leader Election: Follower
// ***************************************************************************

uint32_t trusted_promise(trusted_t* trusted, const cert_prepare_t *cprepare, cert_promise_t *cpromise) {
    if (cprepare->type != HP_PREPARE ||
            __check_integrity(trusted, (cert_t*) cprepare) != 0) {
        // message is corrupted!
        return BAD_CERT_ERROR;
    }

    memset(cpromise, 0, sizeof(cert_promise_t));
    uint32_t start_epoch = (cprepare->e > trusted->e ? 1 : 0);

    if (start_epoch) {
    	trusted->prev_a = trusted->a;
    	trusted->prev_e = trusted->e;
        trusted->e = cprepare->e;
    }

    if (trusted->highest_a < trusted->a)
        trusted->highest_a = trusted->a;

    cpromise->type = HP_PROMISE;
    // trick: when cpromise is rejected, id = NONE_ID
    if (start_epoch) cpromise->id = trusted->id;
               else  cpromise->id = NONE_ID;
    cpromise->e = trusted->e;
    cpromise->a = trusted->highest_a;
    __seal(trusted, (cert_t*) cpromise);

    if (start_epoch) {
        // followers roll back to last committed slot C
        trusted->a = trusted->c;
    }

    return 0;
}

// ***************************************************************************
//                Leader Election: Candidate & Follower
// ***************************************************************************
uint32_t  trusted_update(trusted_t* trusted, cert_accept_t *caccept) {
    if ((caccept->type != HP_ACCEPT && caccept->type != HP_ACCEPT_COMMITTED) ||
    		caccept->id != trusted->id ||    // can update only my own certificates
    		caccept->e != trusted->prev_e ||
            __check_integrity(trusted, (cert_t*) caccept) != 0) {
        // message is corrupted!
        return BAD_CERT_ERROR;
    }
    if (caccept->type != HP_ACCEPT_COMMITTED &&
    		caccept->a <= trusted->prev_a &&
    		caccept->c != trusted->prev_e) { // caccept.c = caccept.e_a
    	// slot was proposed & accepted exactly in the prev epoch,
    	// so e_a has to be equal to previous E
    	return BAD_CERT_ERROR;
    }

    caccept->e = trusted->e;
    if (caccept->type == HP_ACCEPT && caccept->a <= trusted->c)
    	caccept->type = HP_ACCEPT_COMMITTED;
	return 0;
}

// ***************************************************************************
//                 Catchup Functions
// ***************************************************************************
uint32_t trusted_catchup(trusted_t* trusted, const cert_commit_t *ccommit) {
    if (!issnapmessage(ccommit->c)) {
        // ccommit certificate is not a correct SNAPSHOT certificate
        return BAD_CERT_ERROR;
    }

    uint32_t snap_slot = getsnapid(ccommit->c);

    if (ccommit->type != HP_COMMIT ||
            snap_slot <= trusted->c || // old snapshot, ignore it
            __check_integrity(trusted, (cert_t*) ccommit) != 0) {
        // incorrect C counter or message is corrupted!
        return BAD_CERT_ERROR;
    }

    trusted->a = snap_slot;
    trusted->c = snap_slot;
    return 0;
}

// ***************************************************************************
//                 Helper Functions
// ***************************************************************************
uint32_t trusted_get_e(trusted_t* trusted) {
    return trusted->e;
}

uint32_t trusted_get_a(trusted_t* trusted) {
    return trusted->a;
}

uint32_t trusted_get_c(trusted_t* trusted) {
    return trusted->c;
}
