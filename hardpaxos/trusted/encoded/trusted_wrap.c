/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

// NOT THREAD-SAFE!

#include "../trusted.h"
#include "test.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <malloc.h>

unsigned long long malloc_encoded(unsigned long long size);
//uint32_t compare_mac(const trusted_t* trusted, const cert_t* cert, uint32_t against_mac);
//void create_mac(const trusted_t* trusted, cert_t* cert);

typedef struct {
    uint64_t  type_low;
    uint64_t  type_high;
    uint64_t  id_low;
    uint64_t  id_high;
    uint64_t  e_low;
    uint64_t  e_high;
    uint64_t  a_low;
    uint64_t  a_high;
    uint64_t  c_low;
    uint64_t  c_high;
    uint64_t  cmd_mac_low;
    uint64_t  cmd_mac_high;

    uint64_t  mac;
    uint64_t  type;
    uint64_t  id;
    uint64_t  e;
    uint64_t  a;
    uint64_t  c;
    uint64_t  cmd_mac;
} cert_encoded_t;

static cert_encoded_t** certs_encoded = NULL;
static uint64_t* certs_encoded_array = NULL;
static cert_encoded_t* cert1 = NULL;
static cert_encoded_t* cert2 = NULL;

// ***************************************************************************
//                 SIListra Helpers
// ***************************************************************************

static inline uint64_t SIListra_memory64_get_alloc_size(const uint64_t size)
{
    /* size can be native or AN-encoded (see alloca operation) */
    return size * 2;
}

static inline void* SIListra_memory64_convert_backend_ptr_to_virtual_ptr(const uint32_t address)
{
    return (void*)((uintptr_t)address * 2);
}

static inline uint32_t SIListra_memory64_convert_virtual_ptr_to_backend_ptr(void* const ptr)
{
    /* the pointer must be aligned to a code word boundary */
    return (uint32_t)((uintptr_t)ptr / 2);
}

extern void SIListra_signal_error(const char* const errorMessage);

inline uint64_t SIListra_encode_an64(const uint32_t value, const uint32_t A)
{
    return (uint64_t)value * (uint64_t)A;
}

inline uint32_t SIListra_decode_an64(const uint64_t encodedValue, const uint32_t A)
{
    if ((encodedValue % A) != 0) {
        SIListra_signal_error("SIListra_decode_an64: Invalid code word.");
    }

    return (uint32_t)(encodedValue / A);
}

// ***************************************************************************
//                 Wrapper Helpers
// ***************************************************************************
#define to64(high, low) ((uint64_t) high << 32 | low)
#define to32l(x) ((uint32_t) x)
#define to32h(x) ((uint32_t) (x >> 32))

void trusted_wrap_init() {
    certs_encoded = malloc(sizeof(cert_encoded_t*) * 2 * MAX_QUORUM);
    uint32_t i;
    for (i = 0; i < MAX_QUORUM; i++) {
        certs_encoded[i] = malloc(sizeof(cert_encoded_t));
        memset(certs_encoded[i], 0, sizeof(cert_encoded_t));
    }
    certs_encoded_array = malloc(sizeof(uint64_t) * 2 * MAX_QUORUM);

    cert1 = malloc(sizeof(cert_encoded_t));
    cert2 = malloc(sizeof(cert_encoded_t));

    assert((uintptr_t)certs_encoded <= 0x1ffffffffull);
    assert((uintptr_t)certs_encoded % 8 == 0);
    assert((uintptr_t)certs_encoded_array <= 0x1ffffffffull);
    assert((uintptr_t)certs_encoded_array % 8 == 0);
    assert((uintptr_t)cert1 <= 0x1ffffffffull);
    assert((uintptr_t)cert1 % 8 == 0);
    assert((uintptr_t)cert2 <= 0x1ffffffffull);
    assert((uintptr_t)cert2 % 8 == 0);

}

void trusted_wrap_fini() {
    uint32_t i;
    for (i = 0; i < MAX_QUORUM; i++)
        free(certs_encoded[i]);
    free(certs_encoded);
    free(certs_encoded_array);

    free(cert1);
    free(cert2);
}

void augment_cert(cert_t* cert) {
    int64_t x = 0;

    x = SIListra_encode_an64(cert->type, 32715u);
    cert->type_high    = to32h(x);
    cert->type_low     = to32l(x);

    x = SIListra_encode_an64(cert->id, 32715u);
    cert->id_high    = to32h(x);
    cert->id_low     = to32l(x);

    x = SIListra_encode_an64(cert->e, 32715u);
    cert->e_high    = to32h(x);
    cert->e_low     = to32l(x);

    x = SIListra_encode_an64(cert->a, 32715u);
    cert->a_high    = to32h(x);
    cert->a_low     = to32l(x);

    x = SIListra_encode_an64(cert->c, 32715u);
    cert->c_high    = to32h(x);
    cert->c_low     = to32l(x);

    x = SIListra_encode_an64(cert->cmd_mac, 32715u);
    cert->cmd_mac_high    = to32h(x);
    cert->cmd_mac_low     = to32l(x);
}

void set_cert(cert_t* cert, cert_encoded_t* cert_encoded) {
    cert->type_high    = to32h(cert_encoded->type);
    cert->type_low     = to32l(cert_encoded->type);
    cert->id_high      = to32h(cert_encoded->id);
    cert->id_low       = to32l(cert_encoded->id);
    cert->e_high       = to32h(cert_encoded->e);
    cert->e_low        = to32l(cert_encoded->e);
    cert->a_high       = to32h(cert_encoded->a);
    cert->a_low        = to32l(cert_encoded->a);
    cert->c_high       = to32h(cert_encoded->c);
    cert->c_low        = to32l(cert_encoded->c);
    cert->cmd_mac_high = to32h(cert_encoded->cmd_mac);
    cert->cmd_mac_low  = to32l(cert_encoded->cmd_mac);

    cert->type    = SIListra_decode_an64(cert_encoded->type, 32715u);
    cert->id      = SIListra_decode_an64(cert_encoded->id, 32715u);
    cert->e       = SIListra_decode_an64(cert_encoded->e, 32715u);
    cert->a       = SIListra_decode_an64(cert_encoded->a, 32715u);
    cert->c       = SIListra_decode_an64(cert_encoded->c, 32715u);
    cert->cmd_mac = SIListra_decode_an64(cert_encoded->cmd_mac, 32715u);
}

void set_cert_encoded(cert_encoded_t* cert_encoded, cert_t* cert) {
    cert_encoded->type_high    = cert->type_high;
    cert_encoded->type_low     = cert->type_low;
    cert_encoded->id_high      = cert->id_high;
    cert_encoded->id_low       = cert->id_low;
    cert_encoded->e_high       = cert->e_high;
    cert_encoded->e_low        = cert->e_low;
    cert_encoded->a_high       = cert->a_high;
    cert_encoded->a_low        = cert->a_low;
    cert_encoded->c_high       = cert->c_high;
    cert_encoded->c_low        = cert->c_low;
    cert_encoded->cmd_mac_high = cert->cmd_mac_high;
    cert_encoded->cmd_mac_low  = cert->cmd_mac_low;

    cert_encoded->type    = SIListra_encode_an64(cert->type, 32715u);
    cert_encoded->id      = SIListra_encode_an64(cert->id, 32715u);
    cert_encoded->e       = SIListra_encode_an64(cert->e, 32715u);
    cert_encoded->a       = SIListra_encode_an64(cert->a, 32715u);
    cert_encoded->c       = SIListra_encode_an64(cert->c, 32715u);
    cert_encoded->cmd_mac = SIListra_encode_an64(cert->cmd_mac, 32715u);
}

// ***************************************************************************
//                 Upcall Wrappers
// ***************************************************************************

unsigned long long malloc_encoded(unsigned long long size)
{
    void* const ptr = malloc(SIListra_decode_an64(SIListra_memory64_get_alloc_size(size), 32715u));
    const uint64_t ptr_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(ptr), 32715u);
    assert((uintptr_t)ptr <= 0x1ffffffffull);
    assert((uintptr_t)ptr % 8 == 0);
    return ptr_encoded;
}

uint64_t check_integrity_encoded(uint64_t trusted_encoded_ptr, uint64_t cert_encoded_ptr)
{
    cert_encoded_t* cert_encoded = (cert_encoded_t*) SIListra_memory64_convert_backend_ptr_to_virtual_ptr(SIListra_decode_an64(cert_encoded_ptr, 32715u));

    if (cert_encoded->type != to64(cert_encoded->type_high, cert_encoded->type_low) ||
            cert_encoded->id != to64(cert_encoded->id_high, cert_encoded->id_low) ||
            cert_encoded->e != to64(cert_encoded->e_high, cert_encoded->e_low) ||
            cert_encoded->a != to64(cert_encoded->a_high, cert_encoded->a_low) ||
            cert_encoded->c != to64(cert_encoded->c_high, cert_encoded->c_low) ||
            cert_encoded->cmd_mac != to64(cert_encoded->cmd_mac_high, cert_encoded->cmd_mac_low))
        return 1*32715u;

    return 0*32715u;
}

/*
uint64_t compare_mac_encoded(uint64_t trusted_encoded_ptr, uint64_t cert_encoded_ptr, uint64_t against_mac_encoded)
{
    cert_encoded_t* cert_encoded = (cert_encoded_t*) SIListra_memory64_convert_backend_ptr_to_virtual_ptr(SIListra_decode_an64(cert_encoded_ptr, 32715u));

    static cert_t cert;
    memset(&cert, 0, sizeof(cert_t));
    cert.mac     = SIListra_decode_an64(cert_encoded->mac, 32715u);
    cert.type    = SIListra_decode_an64(cert_encoded->type, 32715u);
    cert.id      = SIListra_decode_an64(cert_encoded->id, 32715u);
    cert.e       = SIListra_decode_an64(cert_encoded->e, 32715u);
    cert.a       = SIListra_decode_an64(cert_encoded->a, 32715u);
    cert.c       = SIListra_decode_an64(cert_encoded->c, 32715u);
    cert.cmd_mac = SIListra_decode_an64(cert_encoded->cmd_mac, 32715u);

    trusted_t* trusted = (trusted_t*) SIListra_memory64_convert_backend_ptr_to_virtual_ptr(SIListra_decode_an64(trusted_encoded_ptr, 32715u));
    const uint32_t against_mac = (uint32_t)SIListra_decode_an64(against_mac_encoded, 32715u);
    const uint64_t SIListra_ret = SIListra_encode_an64((uint32_t)(compare_mac(NULL, &cert, against_mac)), 32715u);
    return SIListra_ret;
}

void create_mac_encoded(uint64_t trusted_encoded_ptr, uint64_t cert_encoded_ptr)
{
    cert_encoded_t* cert_encoded = (cert_encoded_t*) SIListra_memory64_convert_backend_ptr_to_virtual_ptr(SIListra_decode_an64(cert_encoded_ptr, 32715u));

    static cert_t cert;
    memset(&cert, 0, sizeof(cert_t));
    cert.type    = SIListra_decode_an64(cert_encoded->type, 32715u);
    cert.id      = SIListra_decode_an64(cert_encoded->id, 32715u);
    cert.e       = SIListra_decode_an64(cert_encoded->e, 32715u);
    cert.a       = SIListra_decode_an64(cert_encoded->a, 32715u);
    cert.c       = SIListra_decode_an64(cert_encoded->c, 32715u);
    cert.cmd_mac = SIListra_decode_an64(cert_encoded->cmd_mac, 32715u);

    trusted_t* trusted = (trusted_t*) SIListra_memory64_convert_backend_ptr_to_virtual_ptr(SIListra_decode_an64(trusted_encoded_ptr, 32715u));
    create_mac(NULL, &cert);

    cert_encoded->mac = SIListra_encode_an64(cert.mac, 32715u);
}
*/


// ***************************************************************************
//                 Test Helpers
// ***************************************************************************
typedef struct {
    uint64_t N;
    uint64_t id;
    char *key;
    uint64_t key_size;
    uint64_t e;
    uint64_t a;
    uint64_t c;
    uint64_t highest_a;
    uint64_t prev_a;
    uint64_t prev_e;
} trusted_enc_t;

void compare_state(trusted_t* trusted, int32_t expN, int32_t expid,
        int32_t expe, int32_t expa, int32_t expc) {
    trusted_enc_t* trusted_enc = (trusted_enc_t*) trusted;

    uint64_t A = 32715u;
    if (trusted_enc == NULL ||
            ((uint64_t)trusted_enc->key % A) != 0 ||
            trusted_enc->key_size   % A != 0 ||
            trusted_enc->highest_a  % A != 0 ||
            trusted_enc->prev_a     % A != 0 ||
            trusted_enc->prev_e     % A != 0 ||
            trusted_enc->N          % A != 0 ||
            trusted_enc->id         % A != 0 ||
            trusted_enc->e          % A != 0 ||
            trusted_enc->a          % A != 0 ||
            trusted_enc->c          % A != 0) {
        fprintf(stderr, "Invalid state.\n");
        exit(INVALSTATE);
    }

    if (
            (SIListra_decode_an64((uint64_t)trusted_enc->key, 32715u) != 0) ||
            (SIListra_decode_an64(trusted_enc->key_size, 32715u) != 0) ||
            (SIListra_decode_an64(trusted_enc->highest_a, 32715u) != 0) ||
            (SIListra_decode_an64(trusted_enc->prev_a, 32715u) != 0) ||
            (SIListra_decode_an64(trusted_enc->prev_e, 32715u) != 0) ||
            /* now those fields that change */
            (SIListra_decode_an64(trusted_enc->N, 32715u) != expN) ||
            (SIListra_decode_an64(trusted_enc->id, 32715u) != expid) ||
            (SIListra_decode_an64(trusted_enc->e, 32715u) != expe) ||
            (SIListra_decode_an64(trusted_enc->a, 32715u) != expa) ||
            (SIListra_decode_an64(trusted_enc->c, 32715u) != expc)) {
        fprintf(stderr, "Unexpected state.\n");
        exit(UNEXPSTATE);
    }
}

void compare_cert(cert_t* cert, int32_t exptype, int32_t expid,
        int32_t expe, int32_t expa, int32_t expc, int32_t expcmd_mac) {
    if (SIListra_encode_an64(cert->type, 32715u)   != to64(cert->type_high, cert->type_low) ||
            SIListra_encode_an64(cert->id, 32715u) != to64(cert->id_high, cert->id_low) ||
            SIListra_encode_an64(cert->e, 32715u)  != to64(cert->e_high, cert->e_low) ||
            SIListra_encode_an64(cert->a, 32715u)  != to64(cert->a_high, cert->a_low) ||
            SIListra_encode_an64(cert->c, 32715u)  != to64(cert->c_high, cert->c_low) ||
            SIListra_encode_an64(cert->cmd_mac, 32715u) != to64(cert->cmd_mac_high, cert->cmd_mac_low)) {
        fprintf(stderr, "Invalid certificate.\n");
        exit(INVALCERT);
    }

    if ((cert->type != exptype) ||
            (cert->id != expid) ||
            (cert->e != expe) ||
            (cert->a != expa) ||
            (cert->c != expc) ||
            (cert->cmd_mac != expcmd_mac)){
        fprintf(stderr, "Unexpected certificate.\n");
        exit(UNEXPCERT);
    }
}

// ***************************************************************************
//                 Function Wrappers
// ***************************************************************************

uint64_t trusted_init_encoded(uint64_t N_AN, uint64_t id_AN, uint64_t key_size_AN, uint64_t key_AN);
trusted_t* trusted_init(const uint32_t N, const uint32_t id, const uint32_t key_size, const char *key) {
    static int initialized = 0;
    if (!initialized) {
        trusted_wrap_init();
        initialized = 1;
    }

    const uint64_t N_encoded = SIListra_encode_an64((uint32_t)(N), 32715u);
    const uint64_t id_encoded = SIListra_encode_an64((uint32_t)(id), 32715u);
    const uint64_t key_size_encoded = SIListra_encode_an64((uint32_t)(key_size), 32715u);
    const uint64_t key_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr((void* const)key), 32715u);
    uint64_t ptr_encoded = trusted_init_encoded(N_encoded, id_encoded, key_size_encoded, key_encoded);
    void* ptr = SIListra_memory64_convert_backend_ptr_to_virtual_ptr(SIListra_decode_an64(ptr_encoded, 32715u));

    return (trusted_t*) ptr;
}

void trusted_fini_encoded(uint64_t trusted_AN);
void trusted_fini(trusted_t* trusted)
{
    static int finalized = 0;
    if (!finalized) {
        trusted_wrap_fini();
        finalized = 1;
    }

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);
    trusted_fini_encoded(trusted_encoded);
}

uint64_t trusted_get_e_encoded(uint64_t trusted_GEPI_PtrToInt_AN);
uint32_t trusted_get_e(trusted_t* trusted)
{
    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);
    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(trusted_get_e_encoded(trusted_encoded), 32715u);
    return SIListra_ret;
}

uint64_t trusted_get_a_encoded(uint64_t trusted_GEPI_PtrToInt_AN);
uint32_t trusted_get_a(trusted_t* trusted)
{
    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);
    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(trusted_get_a_encoded(trusted_encoded), 32715u);
    return SIListra_ret;
}

uint64_t trusted_get_c_encoded(uint64_t trusted_GEPI_PtrToInt_AN);
uint32_t trusted_get_c(trusted_t* trusted)
{
    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);
    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(trusted_get_c_encoded(trusted_encoded), 32715u);
    return SIListra_ret;
}

uint64_t trusted_propose_encoded(uint64_t trusted_GEPI_Casted_AN, uint64_t slot_AN, uint64_t amount_AN, uint64_t certs_GEPI_PtrToInt_AN,
		uint64_t cmd_mac_AN, uint64_t cpropose_GEPI_PtrToInt_AN, uint64_t caccept_GEPI_PtrToInt_AN);
uint32_t  trusted_propose(trusted_t* trusted, const uint32_t slot, const uint32_t amount, cert_t* certs[],
		uint32_t cmd_mac, cert_propose_t *cpropose, cert_accept_t *caccept)
{
    uint32_t i;

    for (i = 0; i < amount; i++) {
        memset(certs_encoded[i], 0, sizeof(cert_encoded_t));
        if (certs[i])
            set_cert_encoded(certs_encoded[i], certs[i]);
        certs_encoded_array[i] = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(certs_encoded[i]), 32715u);
    }
    const uint64_t certs_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(certs_encoded_array), 32715u);

    memset(cert1, 0, sizeof(cert_encoded_t));
    const uint64_t cpropose_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    memset(cert2, 0, sizeof(cert_encoded_t));
    const uint64_t caccept_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert2), 32715u);

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);
    const uint64_t slot_encoded = SIListra_encode_an64((uint32_t)(slot), 32715u);
    const uint64_t amount_encoded = SIListra_encode_an64((uint32_t)(amount), 32715u);
    const uint64_t cmd_mac_encoded = SIListra_encode_an64((uint32_t)(cmd_mac), 32715u);
    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
            trusted_propose_encoded(trusted_encoded, slot_encoded, amount_encoded, certs_encoded_ptr,
                    cmd_mac_encoded, cpropose_encoded_ptr, caccept_encoded_ptr), 32715u);

    set_cert(cpropose, cert1);
    set_cert(caccept, cert2);
    return SIListra_ret;
}

uint64_t trusted_commit_encoded(uint64_t trusted_GEPI_PtrToInt_AN, uint64_t amount_AN, uint64_t caccepts_GEPI_PtrToInt_AN, uint64_t ccommit_GEPI_PtrToInt_AN);
uint32_t  trusted_commit(trusted_t* trusted, const uint32_t amount, cert_accept_t* caccepts[], cert_commit_t *ccommit)
{
    uint32_t i;
    for (i = 0; i < amount; i++) {
        memset(certs_encoded[i], 0, sizeof(cert_encoded_t));
        if (caccepts[i])
            set_cert_encoded(certs_encoded[i], (cert_t*) caccepts[i]);
        certs_encoded_array[i] = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(certs_encoded[i]), 32715u);
    }
    const uint64_t certs_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(certs_encoded_array), 32715u);

    memset(cert1, 0, sizeof(cert_encoded_t));
    const uint64_t ccommit_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);
    const uint64_t amount_encoded = SIListra_encode_an64((uint32_t)(amount), 32715u);

    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
            trusted_commit_encoded(trusted_encoded, amount_encoded,
                    certs_encoded_ptr, ccommit_encoded_ptr), 32715u);

    set_cert(ccommit, cert1);
    return SIListra_ret;
}

uint64_t trusted_accept_encoded(uint64_t trusted_GEPI_PtrToInt_AN, uint64_t cpropose_GEPI_PtrToInt_AN, uint64_t caccept_GEPI_PtrToInt_AN);
uint32_t  trusted_accept(trusted_t* trusted, const cert_propose_t *cpropose, cert_accept_t *caccept)
{
    memset(cert1, 0, sizeof(cert_encoded_t));
    set_cert_encoded(cert1, (cert_t*) cpropose);
    const uint64_t cpropose_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    memset(cert2, 0, sizeof(cert_encoded_t));
    const uint64_t caccept_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert2), 32715u);

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);

    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
            trusted_accept_encoded(trusted_encoded, cpropose_encoded_ptr, caccept_encoded_ptr), 32715u);

    set_cert(caccept, cert2);
    return SIListra_ret;
}

uint64_t trusted_committed_encoded(uint64_t trusted_GEPI_PtrToInt_AN, uint64_t ccommit_GEPI_PtrToInt_AN);
uint32_t  trusted_committed(trusted_t* trusted, const cert_commit_t *ccommit)
{
    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);

    memset(cert1, 0, sizeof(cert_encoded_t));
    set_cert_encoded(cert1, (cert_t*) ccommit);
    const uint64_t ccommit_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
            trusted_committed_encoded(trusted_encoded, ccommit_encoded_ptr), 32715u);
    return SIListra_ret;
}

uint64_t trusted_prepare_encoded(uint64_t trusted_GEPI_PtrToInt_AN, uint64_t epoch_AN, uint64_t cprepare_GEPI_PtrToInt_AN, uint64_t cpromise_GEPI_PtrToInt_AN);
uint32_t  trusted_prepare(trusted_t* trusted, const uint32_t epoch, cert_prepare_t *cprepare, cert_promise_t *cpromise)
{
    memset(cert1, 0, sizeof(cert_encoded_t));
    const uint64_t cprepare_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    memset(cert2, 0, sizeof(cert_encoded_t));
    const uint64_t cpromise_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert2), 32715u);

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);
    const uint64_t epoch_encoded = SIListra_encode_an64((uint32_t)(epoch), 32715u);

    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
            trusted_prepare_encoded(trusted_encoded, epoch_encoded,
            		cprepare_encoded_ptr, cpromise_encoded_ptr), 32715u);

    set_cert(cprepare, cert1);
    set_cert(cpromise, cert2);
    return SIListra_ret;
}

uint64_t trusted_promise_encoded(uint64_t trusted_GEPI_PtrToInt_AN, uint64_t cprepare_GEPI_PtrToInt_AN, uint64_t cpromise_GEPI_PtrToInt_AN);
uint32_t  trusted_promise(trusted_t* trusted, const cert_prepare_t *cprepare, cert_promise_t *cpromise)
{
    memset(cert1, 0, sizeof(cert_encoded_t));
    set_cert_encoded(cert1, (cert_t*) cprepare);
    const uint64_t cprepare_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    memset(cert2, 0, sizeof(cert_encoded_t));
    const uint64_t cpromise_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert2), 32715u);

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);

    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
            trusted_promise_encoded(trusted_encoded, cprepare_encoded_ptr, cpromise_encoded_ptr), 32715u);

    set_cert(cpromise, cert2);
    return SIListra_ret;
}

uint64_t trusted_update_encoded(uint64_t trusted_GEPI_PtrToInt_AN, uint64_t caccept_GEPI_PtrToInt_AN);
uint32_t trusted_update(trusted_t* trusted, cert_accept_t* caccept)
{
    memset(cert1, 0, sizeof(cert_encoded_t));
    set_cert_encoded(cert1, (cert_t*) caccept);
    const uint64_t caccept_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);

    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
    		trusted_update_encoded(trusted_encoded, caccept_encoded_ptr), 32715u);

    set_cert(caccept, cert1);
    return SIListra_ret;
}


uint64_t trusted_catchup_encoded(uint64_t trusted_GEPI_PtrToInt_AN, uint64_t ccommit_GEPI_PtrToInt_AN);
uint32_t trusted_catchup(trusted_t* trusted, const cert_commit_t *ccommit)
{
    memset(cert1, 0, sizeof(cert_encoded_t));
    set_cert_encoded(cert1, (cert_t*) ccommit);
    const uint64_t ccommit_encoded_ptr = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(cert1), 32715u);

    const uint64_t trusted_encoded = SIListra_encode_an64(SIListra_memory64_convert_virtual_ptr_to_backend_ptr(trusted), 32715u);

    const uint32_t SIListra_ret = (uint32_t)SIListra_decode_an64(
            trusted_catchup_encoded(trusted_encoded, ccommit_encoded_ptr), 32715u);
    return SIListra_ret;
}
