# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
import subprocess
import os
import re
import random
import sys
from time import gmtime, strftime
from threading import Timer
import signal

EXE = ' -- build/test_once'
LIMIT     = 1000
FAILLIMIT = 1000

BFI_SO = '/home/dimakuv/code/bfi/obj-intel64/bfi.so'
PIN = '/home/dimakuv/bin/pin-2.13-61206-gcc.4.4.7-linux/pin  -t ' + BFI_SO
FAULTS = ['WVAL', 'RVAL', 'WADDR', 'RADDR', 'RREG', 'WREG', 'CF']


def run(args, timeout):
    class Alarm(Exception):
        pass
    def alarm_handler(signum, frame):
        raise Alarm
    p = subprocess.Popen(args, shell = True
            , stdout = subprocess.PIPE
            , stderr = subprocess.PIPE
            , preexec_fn = os.setsid)

    signal.signal(signal.SIGALRM, alarm_handler)
    signal.alarm(timeout)

    try:
        stdout, stderr = p.communicate()
        if timeout != -1:
            signal.alarm(0)
    except Alarm:
        try: 
            os.killpg(p.pid, signal.SIGKILL)
        except OSError:
            pass
        return None, '', ''
    return p.returncode, stdout, stderr

###################
def test_func(func_name):
    print '\n\t\t Testing %s()' % (func_name)

    # 1. find the range of this function    
    ARGS   = ' -m %s' % (func_name)    
    str = subprocess.check_output(PIN + ARGS + EXE, stderr=subprocess.STDOUT, shell=True)
    
    func_range = [0, 0]
    lines = str.split('\n')
    i = 0
    while i < len(lines):
        s = lines[i]
        if len(s) > 0 and s[0] == '[':
            trigger = s.split('i =')[1].split(',')[0]
            trigger = int(trigger)
            # get following line
            sf = lines[i+1]
            # test only the first met invokation
            if re.match(".*enter", sf) and func_range[0] == 0: 
                func_range[0] = trigger
                i += 2
                continue
            if re.match(".*leave", sf) and func_range[1] == 0:
                func_range[1] = trigger
                i += 2
                continue
        i += 1
    
    print '[ function range:   %10d - %10d ]' % (func_range[0], func_range[1])
    func_range[0] = func_range[0] + 10 
    func_range[1] = func_range[1] - 10 
    print '[ inject faults in: %10d - %10d ]' % (func_range[0], func_range[1])
    
    print strftime('starting at %H:%M:%S', gmtime())
    
    total     = 0
    anfailed  = 0
    ansuccess = 0
    justcrash    = 0
    ancrash      = 0
    aninvalres   = 0
    aninvalcert  = 0
    anunexpcert  = 0
    aninvalstate = 0
    anunexpstate = 0  
    hang         = 0
    
    while ansuccess < LIMIT and anfailed < FAILLIMIT:
        if (total % 100 == 0):
            print >> sys.stderr, '%d \t(total %d) \tat %s...' \
                        % ( ansuccess, total, strftime('%H:%M:%S', gmtime()) )
    
        instr = random.randint(func_range[0], func_range[1])
        fault = FAULTS[ random.randint(0, len(FAULTS)-1) ]
        mask  = random.randint(1, 999) 
        ARGS   = ' -m %s -trigger %d -cmd %s -seed 1 -mask %d' \
                    % (func_name, instr, fault, mask)
        
        timeout = 20
        retcode, stdout, stderr = run(PIN + ARGS + EXE, timeout)

        if retcode != 0:
            stdout += stderr
            f = open('output_once/' + func_name + '.%d' % total, 'w')
            f.write(stdout)

            if retcode == None:
                outargs =  total, ARGS
                hang += 1
                print 'HANG     ', outargs
                print >>f, 'HANG', outargs
                f.close()
                total += 1
                ansuccess += 1
                continue
            
            enter_instr = 0
            exit_instr  = 0
            trig_instr  = 0
            
            lines = stdout.split('\n')
            i = 0
            while i < len(lines):
                s = lines[i]
                if len(s) > 0 and s[0] == '[':
                    trigger = s.split('i =')[1].split(',')[0]
                    trigger = int(trigger)
                    # get following line
                    sf = lines[i+1]
                    # test only the first met invokation
                    if re.match('.*enter', sf) and enter_instr == 0:
                        enter_instr = trigger
                        i += 2
                        continue
                    elif re.match('.*leave', sf) and exit_instr == 0:
                        exit_instr  = trigger
                        i += 2
                        continue
                    elif trig_instr == 0: 
                        trig_instr  = trigger
                        i += 2
                        continue
                i += 1
            
            
            if enter_instr > 0 and exit_instr > 0 and trig_instr > 0:
                if trig_instr < enter_instr or trig_instr > exit_instr:
                    print '    ignore this run'
                    print '      enter: %d, trig: %d, exit: %d' % (enter_instr, trig_instr, exit_instr)
                    continue
                    
            if enter_instr == 0:
                print '    ignore this run; enter_instr = 0 (?!)'
                continue
    
            outargs =  total, ARGS
            
            if retcode == 3 or retcode == 4: 
                anfailed = anfailed + 1
            else:
                 ansuccess = ansuccess + 1
    
            if retcode == 1:
                ancrash   += 1
                print 'ANCRASH', outargs
                print >>f, 'ANCRASH', outargs
            if retcode > 10:
                justcrash += 1
                print 'JUSTCRASH', outargs
                print >>f, 'JUSTCRASH', outargs
            if retcode == 2: 
                aninvalcert += 1
                print 'INVALCERT', outargs
                print >>f, 'INVALCERT', outargs
            if retcode == 3:
                anunexpcert += 1
                print 'UNEXPCERT', outargs
                print >>f, 'UNEXPCERT', outargs
            if retcode == 4:
                anunexpstate += 1
                print 'UNEXPSTATE', outargs
                print >>f, 'UNEXPSTATE', outargs
            if retcode == 5:
                aninvalres += 1
                print 'INVALRES', outargs
                print >>f, 'INVALRES', outargs
            if retcode == 6:
                aninvalstate += 1
                print 'INVALSTATE', outargs
                print >>f, 'INVALSTATE', outargs
        
            f.close()
            
        total = total + 1    
    
    print '--------------------'
    print strftime('finished: %H:%M:%S', gmtime())
    print 'Total runs = %d' % (total)
    print 'AN success = %d' % (ansuccess)
    print 'AN failed  = %d' % (anfailed)
    print '    just crashes     = %d' % (justcrash)
    print '    hang             = %d' % (hang)
    print '    AN crashes       = %d' % (ancrash)
    print '    invalid result   = %d' % (aninvalres)
    print '    invalid cert     = %d' % (aninvalcert)
    print '    unexpected cert  = %d' % (anunexpcert)
    print '    invalid state    = %d' % (aninvalstate)
    print '    unexpected state = %d' % (anunexpstate)


################### Main
if not os.path.exists('output_once'):
    os.makedirs('output_once')

random.seed()

print 'Changing ptrace_scope to 0...'
subprocess.call('echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope', shell=True)

print 'Changing randomize_va_space to 0...'
subprocess.call('echo 0 | sudo tee /proc/sys/kernel/randomize_va_space', shell=True)

print '\n\t\t Golden Run'
res = subprocess.call(PIN + EXE, shell=True)
assert res == 0, 'Oops! Golden run failed'

print '~~~~~~~~~~~ Tests start'
#test_func('trusted_init')          <-- glibc: memory corruption problem!
#test_func('trusted_prepare')
#test_func('trusted_promise')
test_func('trusted_propose')
#test_func('trusted_accept')
#test_func('trusted_commit')
#test_func('trusted_commited')
#test_func('trusted_catchup')
