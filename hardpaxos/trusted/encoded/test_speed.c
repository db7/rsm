/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include "../trusted.h"
#include "test.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <assert.h>
#include "rdtsc.h"

uint64_t
__ztas_now64()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((uint64_t) tv.tv_sec)*1000000000 + (uint64_t)(tv.tv_usec)*1000;
}

void SIListra_signal_error(const char* const errorMessage) {
    fprintf(stderr, "%s\n", errorMessage);
}

int main(int argc, char* argv[]) {
//    trusted_wrap_init();

    // ===== prepare ======
    cert_prepare_t* cprepare = malloc(sizeof(cert_prepare_t));
    memset(cprepare, 0, sizeof(cert_prepare_t));
    cert_promise_t* cpromise = malloc(sizeof(cert_promise_t));
    memset(cpromise, 0, sizeof(cert_promise_t));
    cert_propose_t* cpropose = malloc(sizeof(cert_propose_t));
    memset(cpropose, 0, sizeof(cert_propose_t));
    cert_accept_t* caccept = malloc(sizeof(cert_accept_t));
    memset(caccept, 0, sizeof(cert_accept_t));
    cert_commit_t* ccommit = malloc(sizeof(cert_commit_t));
    memset(ccommit, 0, sizeof(cert_commit_t));

    cert_promise_t* cpromises[2];
    cpromises[0] = malloc(sizeof(cert_promise_t));
    cpromises[1] = malloc(sizeof(cert_promise_t));
    memset(cpromises[0], 0, sizeof(cert_promise_t));
    memset(cpromises[1], 0, sizeof(cert_promise_t));

    cert_accept_t* caccepts[2];
    caccepts[0] = malloc(sizeof(cert_accept_t));
    caccepts[1] = malloc(sizeof(cert_accept_t));
    memset(caccepts[0], 0, sizeof(cert_accept_t));
    memset(caccepts[1], 0, sizeof(cert_accept_t));

    int r;

    /* ========== STARTING EPOCH ========= */
    trusted_t* trusted0 = trusted_init(3, 0, 0, NULL);
    r = trusted_prepare(trusted0, 10, cprepare, cpromise);

    cpromises[0]->type = cpromises[1]->type = HP_PROMISE;
    cpromises[0]->id = 0;  cpromises[1]->id = 1;
    cpromises[0]->e = cpromises[1]->e = 10;
    cpromises[0]->a = cpromises[1]->a = 0;
    cpromises[0]->cmd_mac = cpromises[1]->cmd_mac = 0;
    augment_cert(cpromises[0]);
    augment_cert(cpromises[1]);

    /* ========== LOOP ========= */
    uint64_t prev = __ztas_now64();
    uint64_t now;

    int limit  = 100000000;
    int period = 100000;
    int64_t cycles = 0;
    int loop_a;
    for (loop_a=1; loop_a <= limit; loop_a++) {
        if (loop_a % period == 0) {
            now = __ztas_now64();
            printf("%d\t%lu %ld\n", loop_a, now - prev, cycles);
            prev = now;
            cycles = 0;
        }

        cycles -= rdtsc();
        r = trusted_propose(trusted0, loop_a, 2, cpromises, DUMMY_CMD_MAC, cpropose, caccept);
        cycles += rdtsc();
        assert(r == 0 && "propose failed");

        caccepts[0]->type = caccepts[1]->type = HP_ACCEPT;
        caccepts[0]->id = 0;  caccepts[1]->id = 1;
        caccepts[0]->e = caccepts[1]->e = 10;
        caccepts[0]->a = caccepts[1]->a = loop_a;
        caccepts[0]->c = caccepts[1]->c = 10;
        caccepts[0]->cmd_mac = caccepts[1]->cmd_mac = DUMMY_CMD_MAC;
        augment_cert(caccepts[0]);
        augment_cert(caccepts[1]);

        cycles -= rdtsc();
        r = trusted_commit(trusted0, 2, caccepts, ccommit);
        cycles += rdtsc();
        assert(r == 0 && "commit failed");
    }

    // ===== finish =====
    free(caccepts[0]);
    free(caccepts[1]);
    free(cpromises[0]);
    free(cpromises[1]);
    free(cprepare);
    free(cpromise);
    free(cpropose);
    free(caccept);
    free(ccommit);

//    trusted_wrap_fini();
    return 0;
}
