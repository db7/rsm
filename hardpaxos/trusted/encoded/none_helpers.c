/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include "../trusted.h"
#include "test.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

uint32_t check_integrity(trusted_t* trusted, cert_t* cert){
    return 0;
}

void augment_cert(cert_t* cert) {
    // nothing to do
}

void compare_state(trusted_t* trusted, int32_t expN, int32_t expid,
        int32_t expe, int32_t expa, int32_t expc){
    if (trusted == NULL ||
            (trusted->key != 0) ||
            (trusted->key_size != 0) ||
            (trusted->highest_a != 0) ||
            /* now those fields that change */
            (trusted->N != expN) ||
            (trusted->id != expid) ||
            (trusted->e != expe) ||
            (trusted->a != expa) ||
            (trusted->c != expc)) {
        fprintf(stderr, "Unexpected state.\n");
        exit(UNEXPSTATE);
    }
}

void compare_cert(cert_t* cert, int32_t exptype, int32_t expid,
        int32_t expe, int32_t expa, int32_t expc, int32_t expcmd_mac){
    if ((cert->type != exptype) ||
            (cert->id != expid) ||
            (cert->e != expe) ||
            (cert->a != expa) ||
            (cert->c != expc) ||
            (cert->cmd_mac != expcmd_mac)){
        fprintf(stderr, "Unexpected certificate.\n");
        exit(UNEXPCERT);
    }
}
