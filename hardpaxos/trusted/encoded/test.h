/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef TEST_H_
#define TEST_H_

#include "../trusted.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Certificate is valid and expected, TM state is expected
#define NOFAULTS    0
// Fault Injection had effect, byzantine fault led to benign failure
//   or SIListra complains
#define CRASH       1
// Certificate is invalid, no crash
//   Fault Injection had effect, byzantine fault is detected. This certificate
//   will be ignored by other replicas.
#define INVALCERT   2
// Certificate is valid, but not expected
//   Fault Injection had effect, byzantine fault NOT detected.
//   AN-encoding didn't help.
#define UNEXPCERT   3
// Certificate is valid and expected, but TM state is not expected
//   Fault Injection had effect, byzantine fault NOT detected.
//   AN-encoding didn't help.
#define UNEXPSTATE  4
// TM returned error (result != 0)
//   Fault Injection had effect which led to early exit
//   AN-encoding helped
#define INVALRES    5
// TM state is invalid
//   Fault Injection had effect, byzantine fault is detected.
//   AN-encoding helped.
#define INVALSTATE  6


#define DUMMY_CMD_MAC 123

#define to64(high, low) ((uint64_t) high << 32 | low)
#define to32l(x) ((uint32_t) x)
#define to32h(x) ((uint32_t) (x >> 32))

#endif /* TEST_H_ */
