/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include "../trusted.h"
#include "test.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void compare_res(int res) {
    if (res) {
        fprintf(stderr, "Invalid result.\n");
        exit(INVALRES);
    }
}

void SIListra_signal_error(const char* const errorMessage) {
    fprintf(stderr, "%s\n", errorMessage);
    exit(CRASH);
}

extern void compare_state(trusted_t* trusted, int32_t expN, int32_t expid,
        int32_t expe, int32_t expa, int32_t expc);

void compare_cert(cert_t* cert, int32_t exptype, int32_t expid,
        int32_t expe, int32_t expa, int32_t expc, int32_t expcmd_mac);

int main(int argc, char* argv[]) {
//    trusted_wrap_init();

    // ===== prepare ======
    cert_prepare_t* cprepare = malloc(sizeof(cert_prepare_t));
    memset(cprepare, 0, sizeof(cert_prepare_t));
    cert_promise_t* cpromise = malloc(sizeof(cert_promise_t));
    memset(cpromise, 0, sizeof(cert_promise_t));
    cert_propose_t* cpropose = malloc(sizeof(cert_propose_t));
    memset(cpropose, 0, sizeof(cert_propose_t));
    cert_accept_t* caccept = malloc(sizeof(cert_accept_t));
    memset(caccept, 0, sizeof(cert_accept_t));
    cert_commit_t* ccommit = malloc(sizeof(cert_commit_t));
    memset(ccommit, 0, sizeof(cert_commit_t));
    cert_commit_t* snap = malloc(sizeof(cert_commit_t));
    memset(snap, 0, sizeof(cert_commit_t));

    cert_promise_t* cpromises[2];
    cpromises[0] = malloc(sizeof(cert_promise_t));
    cpromises[1] = malloc(sizeof(cert_promise_t));
    memset(cpromises[0], 0, sizeof(cert_promise_t));
    memset(cpromises[1], 0, sizeof(cert_promise_t));

    cert_accept_t* caccepts[2];
    caccepts[0] = malloc(sizeof(cert_accept_t));
    caccepts[1] = malloc(sizeof(cert_accept_t));
    memset(caccepts[0], 0, sizeof(cert_accept_t));
    memset(caccepts[1], 0, sizeof(cert_accept_t));

    int r;

    fprintf(stderr, "===== test trusted_init() =====\n");
    trusted_t* trusted0 = trusted_init(3, 0, 0, NULL);
    trusted_t* trusted1 = trusted_init(3, 1, 0, NULL);

    compare_state(trusted0, 3, 0, 0, 0, 0);
/*
    printf("trusted is %p \n", trusted0);
    printf("trusted.N is %d \n", SIListra_decode_an64(trusted0->N, 32715u));
*/

    fprintf(stderr, "===== test trusted_prepare() =====\n");
    r = trusted_prepare(trusted0, 10, cprepare, cpromise);
    compare_res(r);

    compare_state(trusted0, 3, 0, 10, 0, 0);
    compare_cert(cprepare, HP_PREPARE, 0, 10, 0, 0, 0);
    compare_cert(cpromise, HP_PROMISE, 0, 10, 0, 0, 0);
/*
    printf("   cprepare.id=%d, \t cprepare.e=%d, \t cprepare.cmd_mac=%d \n", cprepare->id, cprepare->e, cprepare->cmd_mac);
    printf("   cpromise.id=%d, \t cpromise.e=%d, \t cpromise.cmd_mac=%d \n", cpromise->id, cpromise->e, cpromise->cmd_mac);
*/

    fprintf(stderr, "===== test trusted_promise() =====\n");
    r = trusted_promise(trusted1, cprepare, cpromise);
    compare_res(r);

    compare_state(trusted1, 3, 1, 10, 0, 0);
    compare_cert(cpromise, HP_PROMISE, 1, 10, 0, 0, 0);
/*
    printf("   cpromise.id=%d, \t cpromise.e=%d, \t cpromise.cmd_mac=%d \n", cpromise->id, cpromise->e, cpromise->cmd_mac);
*/

    fprintf(stderr, "===== test trusted_propose() =====\n");
    cpromises[0]->type = cpromises[1]->type = HP_PROMISE;
    cpromises[0]->id = 0;  cpromises[1]->id = 1;
    cpromises[0]->e = cpromises[1]->e = 10;
    cpromises[0]->a = cpromises[1]->a = 0;
    cpromises[0]->cmd_mac = cpromises[1]->cmd_mac = 0;
    augment_cert(cpromises[0]);
    augment_cert(cpromises[1]);
//    create_mac(NULL, cpromises[0]);
//    create_mac(NULL, cpromises[1]);

    r = trusted_propose(trusted0, 1, 2, cpromises, DUMMY_CMD_MAC, cpropose, caccept);
    compare_res(r);

    compare_state(trusted0, 3, 0, 10, 1, 0);
    compare_cert(cpropose, HP_PROPOSE, 0, 10, 1, 0, DUMMY_CMD_MAC);
    compare_cert(caccept, HP_ACCEPT, 0, 10, 1, 10, DUMMY_CMD_MAC);

/*
    printf("   cpropose.id=%d, \t cpropose.a=%d, \t cpropose.cmd_mac=%d \n", cpropose->id, cpropose->a, cpropose->cmd_mac);
    printf("   caccept.id=%d, \t caccept.a=%d, \t caccept.cmd_mac=%d \n", caccept->id, caccept->a, caccept->cmd_mac);
    printf("   cpropose.mac=%d, \t compare=%d, \t caccept.mac=%d \t compare=%d\n",
            cpropose->mac, compare_mac(NULL, cpropose, __calc_mac(NULL, cpropose)),
            caccept->mac, compare_mac(NULL, caccept, __calc_mac(NULL, caccept)));
*/

    fprintf(stderr, "===== test trusted_accept() =====\n");
    r = trusted_accept(trusted1, cpropose, caccept);
    compare_res(r);

    compare_state(trusted1, 3, 1, 10, 1, 0);
    compare_cert(caccept, HP_ACCEPT, 1, 10, 1, 10, DUMMY_CMD_MAC);
/*
    printf("   caccept.id=%d, \t caccept.a=%d, \t caccept.cmd_mac=%d \n", caccept->id, caccept->a, caccept->cmd_mac);
*/

    fprintf(stderr, "===== test trusted_commit() =====\n");
    caccepts[0]->type = caccepts[1]->type = HP_ACCEPT;
    caccepts[0]->id = 0;  caccepts[1]->id = 1;
    caccepts[0]->e = caccepts[1]->e = 10;
    caccepts[0]->a = caccepts[1]->a = 1;
    caccepts[0]->c = caccepts[1]->c = 10;
    caccepts[0]->cmd_mac = caccepts[1]->cmd_mac = DUMMY_CMD_MAC;
    augment_cert(caccepts[0]);
    augment_cert(caccepts[1]);
//    create_mac(NULL, caccepts[0]);
//    create_mac(NULL, caccepts[1]);

    r = trusted_commit(trusted0, 2, caccepts, ccommit);
    compare_res(r);

    compare_state(trusted0, 3, 0, 10, 1, 1);
    compare_cert(ccommit, HP_COMMIT, 0, 10, 0, 1, DUMMY_CMD_MAC);
/*
    printf("   ccommit.id=%d, \t ccommit.a=%d, \t ccommit.cmd_mac=%d \n", ccommit->id, ccommit->a, ccommit->cmd_mac);
    printf("   ccommit.mac=%d, \t compare=%d\n",
            ccommit->mac, compare_mac(NULL, cpropose, __calc_mac(NULL, cpropose)));
*/

    fprintf(stderr, "===== test trusted_committed() =====\n");
    r = trusted_committed(trusted1, ccommit);
    compare_res(r);

    compare_state(trusted1, 3, 1, 10, 1, 1);

    fprintf(stderr, "===== test trusted_catchup() =====\n");
    snap->type = HP_COMMIT;
    snap->id = 2;
    snap->e = 30;
    snap->a = TRUSTED_K + TRUSTED_Z + 1;
    snap->c = TRUSTED_K + TRUSTED_Z + 1;
    snap->cmd_mac = DUMMY_CMD_MAC;
    augment_cert(snap);

    r = trusted_catchup(trusted0, snap);
    compare_res(r);

    compare_state(trusted0, 3, 0, 10, TRUSTED_Z, TRUSTED_Z);

    // ===== finish =====
    free(caccepts[0]);
    free(caccepts[1]);
    free(cpromises[0]);
    free(cpromises[1]);
    free(cprepare);
    free(cpromise);
    free(cpropose);
    free(caccept);
    free(ccommit);
    free(snap);

//    trusted_wrap_fini();
    return NOFAULTS;
}
