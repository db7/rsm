/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#ifndef CERT_MAC_H_
#define CERT_MAC_H_

#include "trusted.h"

// hack: used for tests only!
uint32_t __calc_mac(const trusted_t* trusted, const cert_t* cert);

void     create_mac(const trusted_t* trusted, cert_t* cert);
uint32_t mac_cmd(const trusted_t* trusted, const uint32_t size, const char* cmd);
uint32_t      compare_mac(const trusted_t* trusted, const cert_t* cert, uint32_t against_mac);
uint32_t      compare_mac_cmd(const trusted_t* trusted, const uint32_t size, const char *cmd, uint32_t against_mac);

#endif /* CERT_MAC_H_ */
