/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include <ztas/mem.h>
#include <ztas/string.h>
#include <ztas/shrp.h>
#include <ztas/utils/crc.h>
#include <assert.h>
#include "cert_mac.h"
#include "../certs.h"
#include "trusted.h"

uint32_t check_integrity(trusted_t* trusted, cert_t* cert){
    return 0;
    //return compare_mac(trusted, cert, cert->mac);
}


// ***************************************************************************
//                     MAC
// ***************************************************************************

uint32_t __calc_mac(const trusted_t* trusted, const cert_t* cert) {
    uint32_t size = sizeof(cert_t);
    uint32_t offset = ((unsigned long) &(((cert_t *) 0)->type));
    size -= offset;

    // to create mac we use only significant fields and omit the header
    // TODO change to crc32c one day
    uint32_t mac = crc((const unsigned char *)cert + offset, size);
    return mac;
}

void create_mac(const trusted_t* trusted, cert_t* cert) {
    uint32_t mac = __calc_mac(trusted, cert);
    cert->mac    = mac;
}

uint32_t mac_cmd(const trusted_t* trusted, const uint32_t size, const char *cmd) {
    // TODO change to crc32c one day
    uint32_t mac = crc((const unsigned char *)cmd, size);
    return mac;
}

uint32_t compare_mac(const trusted_t* trusted, const cert_t* cert, uint32_t against_mac) {
    uint32_t mac = __calc_mac(trusted, cert);
    return (mac == against_mac ? 0 : 1);
}

uint32_t compare_mac_cmd(const trusted_t* trusted, const uint32_t size, const char *cmd, uint32_t against_mac) {
    uint32_t mac = mac_cmd(trusted, size, cmd);
    return (mac == against_mac ? 0 : 1);
}
