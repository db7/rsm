/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _TRUSTED_H_
#define _TRUSTED_H_

#include <ztas/types.h>
#include "../certs.h"
#include "../constants.h"

#define REPROPOSE_ERROR         1
#define DUPLICATE_CERT_ERROR    2
#define BAD_CERT_ERROR          3
#define EPOCH_ERROR             4
#define LEADER_ERROR            5
#define SYNC_ERROR              6

// snapshotting period
#define TRUSTED_Z               SNAP_PERIOD
// maximum gap between A and C
#define TRUSTED_K               ((TRUSTED_Z / 5) * 2)

#define issnapmessage(s) (s > TRUSTED_Z && (s - TRUSTED_K) % TRUSTED_Z == 1)
#define getsnapid(s) (((s - TRUSTED_K - 1) / TRUSTED_Z) * TRUSTED_Z)

typedef struct {
    uint32_t N;      // amount of replicas
    uint32_t id;     // replica ID

    // NB: not needed for CRC
    char *key;          // symmetric crypto key
    uint32_t key_size;  // size of the crypto key

    uint32_t e;      // epoch counter
    uint32_t a;      // acceptance counter
    uint32_t c;      // commit counter

    // highest ever seen A value; used in GUARD certificates
    uint32_t highest_a;

    // A and E counters from previous epoch; used in Update()
    uint32_t prev_a;
    uint32_t prev_e;
} trusted_t;

trusted_t* trusted_init(const uint32_t N, const uint32_t id, const uint32_t key_size, const char *key);
void trusted_fini(trusted_t* trusted);

// Normal execution: Leader
uint32_t  trusted_propose(trusted_t* trusted, const uint32_t slot, const uint32_t amount,
		cert_t** certs, uint32_t cmd_mac, cert_propose_t *cpropose, cert_accept_t *caccept);
uint32_t  trusted_commit(trusted_t* trusted, const uint32_t amount, cert_accept_t** caccepts, cert_commit_t *ccommit);

// Normal execution: Follower
uint32_t  trusted_accept(trusted_t* trusted, const cert_propose_t *cpropose, cert_accept_t *caccept);
uint32_t  trusted_committed(trusted_t* trusted, const cert_commit_t *ccommit);

// Leader Election: Candidate
uint32_t  trusted_prepare(trusted_t* trusted, const uint32_t epoch, cert_prepare_t *cprepare, cert_promise_t *cpromise);

// Leader Election: Follower
uint32_t  trusted_promise(trusted_t* trusted, const cert_prepare_t *cprepare, cert_promise_t *cpromise);

// Leader Election: Candidate & Follower
uint32_t  trusted_update(trusted_t* trusted, cert_accept_t *caccept);

// Catchup
uint32_t trusted_catchup(trusted_t* trusted, const cert_commit_t *ccommit);

// helper functions
uint32_t  trusted_get_e(trusted_t* trusted);
uint32_t  trusted_get_a(trusted_t* trusted);
uint32_t  trusted_get_c(trusted_t* trusted);

#endif

