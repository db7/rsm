/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

// how many processes can be in system, needed for binding epochs to processes
// NOTE: if there are more than MAXPROCESSES nodes, then algo is not safe!
#define MAXPROCESSES 10

#define SNAP_PERIOD  4000

#define MAX_QUORUM 5

#define MAC_SIZE sizeof(uint32_t)

#endif /* _CONSTANTS_H_ */
