/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#ifndef _CERTS_H_
#define _CERTS_H_

#include <ztas/types.h>
#include <ztas/msg.h>
#include "constants.h"

#define BUF_SIZE (MAC_SIZE * 100)
#define NONE_ID    (MAX_QUORUM+1)
#define MAX_SLOTS  (2*SNAP_PERIOD)

typedef enum {
    HP_DUMMY   = -1,
    HP_PROPOSE=1,  HP_ACCEPT=2,  HP_COMMIT=3,  // normal runs
                   HP_ACCEPT_COMMITTED=1024+2, // accept which was committed
    HP_PREPARE=10, HP_PROMISE=11,              // leader election
    HP_REQUEST=20, HP_RESPONSE=21              // client communication
} cert_type;

// ***************************************************************************
//                      Certificates
// ***************************************************************************
//
//  NOTE: Certificates with e = 0 or pe = 0 are "empty" (NULL).

/*
 * use AN-encoded Trusted Module; integrity is preserved via AN-fields and not CRC.
 * DO NOT comment this macro out, check rsm/Sconscript file.
 */
//#define HARDPAXOS_ENCODED

// we use one common structure for all certificates
typedef struct {
#ifdef HARDPAXOS_ENCODED
    uint32_t   type_low;
    uint32_t   type_high;
    uint32_t   id_low;
    uint32_t   id_high;
    uint32_t   e_low;
    uint32_t   e_high;
    uint32_t   a_low;
    uint32_t   a_high;
    uint32_t   c_low;
    uint32_t   c_high;
    uint32_t  cmd_mac_low;
    uint32_t  cmd_mac_high;
#endif
    uint32_t  mac;
    uint32_t  type;
    uint32_t  id;
    uint32_t  e;
    uint32_t  a;
    uint32_t  c;
    uint32_t  cmd_mac;
} cert_t;

typedef cert_t cert_propose_t;
typedef cert_t cert_accept_t;
typedef cert_t cert_commit_t;
typedef cert_t cert_prepare_t;
typedef cert_t cert_promise_t;

// ***************************************************************************
//                      Messages for certificates
// ***************************************************************************

typedef struct {
	cert_propose_t cpropose;
    uint32_t       size;
    char           cmd[];
} msg_propose_t;

typedef struct {
    cert_promise_t cpromise;
    cert_commit_t  snap;
    uint32_t       size;
    char           slots[];
} msg_promise_t;

typedef struct {
#ifdef HARDPAXOS_ENCODED
    uint32_t   type_low;
    uint32_t   type_high;
    uint32_t   id_low;
    uint32_t   id_high;
    uint32_t   e_low;
    uint32_t   e_high;
    uint32_t   a_low;
    uint32_t   a_high;
    uint32_t   c_low;
    uint32_t   c_high;
    uint32_t  cmd_mac_low;
    uint32_t  cmd_mac_high;
#endif
    uint32_t mac;
    uint32_t type;
    uint32_t id;
    uint32_t e;
    uint32_t a;
    uint32_t c;
    uint32_t cmd_mac;
    uint32_t size;
    char cmd[];
} msg_request_t;

typedef struct {
#ifdef HARDPAXOS_ENCODED
    uint32_t   type_low;
    uint32_t   type_high;
    uint32_t   id_low;
    uint32_t   id_high;
    uint32_t   e_low;
    uint32_t   e_high;
    uint32_t   a_low;
    uint32_t   a_high;
    uint32_t   c_low;
    uint32_t   c_high;
    uint32_t  cmd_mac_low;
    uint32_t  cmd_mac_high;
#endif
    uint32_t mac;
    uint32_t type;
    uint32_t id;
    uint32_t e;
    uint32_t a;
    uint32_t c;              // Commit Counter to protect against replay attacks
    uint32_t cmd_mac;        // result on what command
    uint32_t size;
    char res[];
} msg_response_t;

#endif /* _CERTS_H_ */
