server_flags="--backup=2"

xterm -hold -geometry 65x25+0+0 -e "../../../../build/algorithms/rsm/sqlite/rsm_sqlite_perf_bench hard_rsm.ini 123" &
xterm -hold -geometry 65x35+0+400 -e "../../../../build/algorithms/rsm/sqlite/rsm_sqlite_server 0 hard_rsm.ini $server_flags > server0.txt" &
xterm -hold -geometry 65x35+400+400 -e "../../../../build/algorithms/rsm/sqlite/rsm_sqlite_server 1 hard_rsm.ini $server_flags > server1.txt" &
xterm -hold -geometry 65x35+800+400 -e "../../../../build/algorithms/rsm/sqlite/rsm_sqlite_server 2 hard_rsm.ini $server_flags > server2.txt" &
xterm -hold -geometry 80x35+1200+0 -e top

read

