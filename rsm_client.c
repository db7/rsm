/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * TODO: use free_reqs queue for freed request_t objects
 */

#include <ztas/ztime.h>
#include <ztas/ds/queue.h>
#include <ztas/net/connection.h>
#include <ztas/proc/cfg.h>
#include <ztas/ds/tree.h>
#include <ztas/log.h>

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "rsm_client.h"
#include <debug.h>

//#define CLIENTBATCHING

static rsm_client_t* rsm = NULL;

static int __rsm_client_request(rsm_client_t* rsm, rsmmsg_request_t* req,
        rsm_client_response_cb* cb, void* args);

/* -----------------------------------------------------------------------------
 *   Handle sync requests
 * -------------------------------------------------------------------------- */
static pthread_mutex_t    res_mutex;
static pthread_cond_t     res_cv;
static rsmmsg_response_t* last_res;

/*
 * Client calls rsm_client_invoke() to send req to Server and wait for res
 * Client has to copy res if he needs it later (after another invoke)
 */
int
rsm_client_invoke(rsm_client_t* rsm, rsmmsg_request_t* req,
        rsmmsg_response_t** res, void* args) {
    if (!rsm->client_sync) {
        fprintf(stderr, "rsm_client_invoke: cannot invoke, client in async mode\n");
        return 0;
    }

    last_res->cid = -1;
    __rsm_client_request(rsm, req, NULL, args);

    // wait till rsm_client_response() gets a response and writes it to last_res
    pthread_mutex_lock(&res_mutex);
    if (last_res->cid == -1) {
        // could be that the response already came,
        // but if not, wait on res_cv
        pthread_cond_wait(&res_cv, &res_mutex);
    }
    *res = last_res;
    pthread_mutex_unlock(&res_mutex);
    return 1;
}

/* -----------------------------------------------------------------------------
 *   Handle async requests
 * -------------------------------------------------------------------------- */

/*
 * Client calls rsm_client_request() to send req to Server
 */
int
rsm_client_request(rsm_client_t* rsm, rsmmsg_request_t* req,
        rsm_client_response_cb* cb, void* args)
{
    if (rsm->client_sync) {
        fprintf(stderr, "rsm_client_request: cannot invoke, client in sync mode\n");
        return 0;
    }
    return __rsm_client_request(rsm, req, cb, args);
}

static int
__rsm_client_request(rsm_client_t* rsm, rsmmsg_request_t* req,
        rsm_client_response_cb* cb, void* args)
{
#if CLIENTBATCHING
    int batch_flush = 0;
    if (req && rsm->inflight > MAX_CLIENT_INFLIGHT) {
        if (rsm->batch->size + req->size + sizeof(rsmmsg_item_t) < RSMMSG_MAX) {
            if (rsm->batch->bcount == 0)
                rsm->batch->sent = __ztas_now64();
            // add to batch
            rsmmsg_item_t* item = (rsmmsg_item_t*) (rsm->batch->data + rsm->batch->size);
            item->size = req->size;
            memcpy(item->data, req->data, req->size);
            rsm->batch->size += item->size + sizeof(rsmmsg_item_t);
            ++rsm->batch->bcount;

            return 1;
        } else {
            // batch is full
            rsmmsg_request_t* tmp = req;
            req = rsm->batch;

            // create new batch
            rsm->batch = rsmmsg_request_init(RSMMSG_MAX);
            rsm->batch->size = 0;
            rsm->batch->k    = rsm->k;
            rsm->batch->cid  = rsm->cid + 1;

            rsmmsg_item_t* item = (rsmmsg_item_t*) (rsm->batch->data + rsm->batch->size);
            item->size = tmp->size;
            memcpy(item->data, tmp->data, tmp->size);
            rsm->batch->size += item->size + sizeof(rsmmsg_item_t);
            ++rsm->batch->bcount;
            rsm->batch->sent = __ztas_now64();

            // there's batch to flush
            batch_flush = 1;
        }
    }

    if (!req) {
        req = rsm->batch;

        rsm->batch = rsmmsg_request_init(RSMMSG_MAX);
        rsm->batch->size = 0;
        rsm->batch->k    = rsm->k;
        rsm->batch->cid  = rsm->cid + 1;

        batch_flush = 1;
    }

    // more inflight messages
    ++rsm->inflight;

    if (req->bcount > 1) {
        uint64_t now = __ztas_now64();
        // take average time on the batch
        req->sent = (now - req->sent)/2 + now;
    }
#endif
    // prepare request: seal if needed
    if (rsm->seal_requests) {
        rsm_msg_seal(req, rsm->k, rsm->cid);
        DLOG("Sealing message (k = %d, cid = %d)\n", rsm->k, rsm->cid);
    }
    ++rsm->cid;

    if (cb)   rsm->response_cb = cb;
    if (args) rsm->args = args;

    int32_t tsize = sizeof(rsmmsg_request_t) + req->size;
    int r = 0;

    // the request is sent only now
//    req->sent = __ztas_now64();

    static int connection_established = 0;
    if (rsm->send_to_all || connection_established == 0) {
        idset_it_t* replicas_iterator = malloc(sizeof(idset_it_t));
        idset_it_init(rsm->replicas_idset, replicas_iterator);
        int32_t rid = idset_it_next(replicas_iterator);

        while(rid != -1) {
            shrp_hold(req);
//            printf("Sending to %d\n", rid);
            r += connection_send(tree_get(rsm->replicas, rid), (void*) req, tsize);

            rid = idset_it_next(replicas_iterator);
        }

        free(replicas_iterator);
        connection_established = 1;
    } else {
        // send only to Replica 0
        shrp_hold(req);
        r += connection_send(tree_get(rsm->replicas, 0), (void*) req, tsize);
    }
#if CLIENTBATCHING
    // release batch pointer
    if (batch_flush) shrp_free(req);
#endif
    if (r != CONNECTION_SEND_OK) {
        --rsm->cid;
        return 0;
    }

    DLOG("Sent message (k = %d, cid = %d)\n", rsm->k, rsm->cid);
    return 1;
}

/*
 * Connection calls connection_send_done() callback when request is sent to network.
 * We should free data we allocated (by schrp_hold) in rsm_client_request().
 */
static void
connection_send_done(connection_t* connection, void* data, void* args)
{
    shrp_free(data);
}

/* -----------------------------------------------------------------------------
 * Handle responses
 * -------------------------------------------------------------------------- */

/*
 * connection_recv() calls rsm_client_response() as a bridge between
 * RSM client and real Client.
 */
static void rsm_client_response(rsm_client_t* rsm, rsmmsg_response_t* res) {
	if (res->cid < rsm->min_arr) {
		// response is from ancient past; ignore it
		LOG2("Ignore too old response %d (min slot = %d)\n", res->cid, rsm->min_arr);
		return;
	}

    int slot = res->cid % MAXRESARR;

    if (rsm->res_arr[slot] >= rsm->num_responses) {
    	// already had enough responses
    	return;
    }

    if (rsm->num_responses > 1) {
		uint32_t rmac = res->mac;
		if (res->size) {
			rsm_msg_seal((rsmmsg_request_t*) res, res->k, res->cid);
			rmac = res->mac;
		}

		if (rsm->res_arr[slot] == 0) {
			// remember the first mac that is received
			rsm->mac_arr[slot] = rmac;
		} else {
			// there is already a mac received, compare with it
			if (rsm->mac_arr[slot] != rmac) {
				LOG1("[rsm_client] Ignore response cid = %d\n", res->cid);
				return;
			}
		}
    }

    rsm->res_arr[slot]++;
    if (rsm->res_arr[slot] == rsm->num_responses) {
        // enough responses
        DLOG("Response k = %d cid = %d\n", res->k, res->cid);

        if (rsm->client_sync) {
            // in sync mode, write to last_res and signal
            pthread_mutex_lock(&res_mutex);
            assert(res->size + sizeof(rsmmsg_response_t) <= MAXPAYLOAD);
            memcpy(last_res, res, res->size + sizeof(rsmmsg_response_t));
            pthread_cond_signal(&res_cv);
            pthread_mutex_unlock(&res_mutex);
        } else {
            // in async mode, make an upcall
            rsm->response_cb(rsm, res, rsm->args);
        }

        // clean one of old slots, in the hope that this old slot won't be used
        if (res->cid >= MAXRESARR/2) {
            int purge_slot = (res->cid - MAXRESARR/2);
            rsm->res_arr[purge_slot % MAXRESARR] = 0;
            rsm->min_arr = purge_slot + 1;
        }

#if CLIENTBATCHING
        --rsm->inflight;
        if (rsm->inflight < 0) {
            rsm->inflight = 0;
        }
        if (rsm->inflight < MAX_CLIENT_INFLIGHT && rsm->batch->size > 0) {
            // flush batch
            __rsm_client_request(rsm, NULL, NULL, NULL);
        }
#endif
    }
}

/*
 * Connection calls connection_recv() callback when response comes from network.
 * We should retrieve the whole rsmmsg_response message and send it up
 * to Client via rsm->response_cb().
 */
static uint64_t
connection_recv(connection_t* replica, void** data, ssize_t* read, void* args)
{
    assert (data && read && args);
    int32_t tsize = sizeof(rsmmsg_response_t);
    rsm_client_t* rsm = (rsm_client_t*) args;
    if (*data == NULL) {
        // no buffer yet, provide it
        *read = 0;
        *data = (void*) shrp_malloc(rsm->buffer_size);
        return tsize;
    }

    if (*data && *read == 0) {
        // client disconnected
        shrp_free(*data);
        return 0;
    }

    assert (*read > 0);
    if (*read < tsize) {
        return tsize - *read;
    }

    rsmmsg_response_t* res = (rsmmsg_response_t*) *data;
    size_t total_size = sizeof(rsmmsg_response_t) + res->size;

    if (total_size > rsm->buffer_size) {
        rsm->buffer_size = total_size;
        void* buf = shrp_realloc(*data, rsm->buffer_size);
        assert(buf && "Allocation failed");
        *data = buf;
        return rsm->buffer_size - *read;
    }

    if (*read < total_size) {
        return total_size - *read;
    }
    assert (*read == total_size);

    // retrieve request
#if 0
    // TODO if it will be used some day, change to rsm_client_response()
    assert (!queue_empty(rsm->requests));
    // depends on FIFO property of TCP (?)
    request_t* request = queue_deq(rsm->requests);

    // free request and message
    shrp_free(request->req);
    request->req = NULL;

    // response callback
    request->cb(rsm, res, request->args);

    // free request data structure
    free(request);
#else
    rsm_client_response(rsm, res);
#endif

    // next request
    shrp_free(res);
    *data = NULL;
    *read = 0;
    return 0;
}

/* -----------------------------------------------------------------------------
 * init/fini
 * -------------------------------------------------------------------------- */
static void
stop_cb(struct ev_loop* loop, struct ev_async* w, int revents)
{
    ev_unloop (EV_A_ EVUNLOOP_ALL);

    // if not in main thread, then run_loop() shall continue execution
}

static void*
run_loop(void* args)
{
    struct ev_loop* loop = (struct ev_loop*) args;
    ev_loop(loop, 0);

    // loop was unlooped by stop_cb(), this thread is done
    return NULL;
}

rsm_client_t*
rsm_client_initev(struct ev_loop* loop, const char* fname, int32_t client_id, void* args)
{
    rsm = (rsm_client_t*) malloc(sizeof(rsm_client_t));
    assert (rsm);
    rsm->cid = 0;
    if (client_id == -1)
        rsm->k = ((int32_t) time(0)) << 8 | ((int32_t) getpid());
    else
        rsm->k = client_id;

    printf("starting client k = %d\n", rsm->k);
    rsm->requests = queue_create(100);
    rsm->local_loop = 0;

    rsm->seal_requests = 0;
    rsm->client_sync = 0;
    rsm->send_to_all = 0;
    rsm->buffer_size = BUFFER_SIZE;
    rsm->replicas = tree_init();

    int i;
    for (i = 0; i < MAXRESARR; i++)
    	rsm->res_arr[i] = 0;
    rsm->min_arr = 0;

    ev_async_init(&rsm->stop_w, stop_cb);

    if (loop == NULL) {
        rsm->loop = ev_loop_new(0);
        assert (rsm->loop);
        rsm->local_loop = 1;
        ev_async_start(rsm->loop, &rsm->stop_w);
    } else rsm->loop = loop;


    // look for all servers
    assert (fname);
    cfg_t* cfg = cfg_parse(fname);

    // batching
    rsm->inflight = 0;
    rsm->batch       = rsmmsg_request_init(RSMMSG_MAX);
    rsm->batch->size = 0;
    rsm->batch->k    = rsm->k;
    rsm->batch->cid  = rsm->cid;

    rsm->replicas_idset = idset_init_str(cfg_get(cfg,"paxos:replicas"));
    idset_it_t* replicas_iterator = malloc(sizeof(idset_it_t));
    idset_it_init(rsm->replicas_idset, replicas_iterator);

    // returns host id or -1 if end is reached
    int32_t host_id = idset_it_next(replicas_iterator);
    char buf[20];

    // do all server Replicas answer to client?
    rsm->num_responses = 1;
    const char* num_responses_str = cfg_get(cfg, "rsm:num_responses");
    if (num_responses_str)
        rsm->num_responses = atoi(num_responses_str);
    printf("[starting] number of responses: %d\n", rsm->num_responses);

    last_res = NULL;
    const char* client_sync_str = cfg_get(cfg, "rsm:client_sync");
    if (client_sync_str && strcmp(client_sync_str, "true") == 0) {
        rsm->client_sync = 1;
        last_res = rsmmsg_response_init(MAXPAYLOAD);
    }

    const char* seal_requests = cfg_get(cfg, "rsm:seal_requests");
    if (seal_requests && strcmp(seal_requests, "true") == 0)
        rsm->seal_requests = 1;
    printf("[starting] sealing requests: %d\n", rsm->seal_requests);

    const char* connect_to_all = cfg_get(cfg, "rsm:connect_to_all");
    if (connect_to_all && strcmp(connect_to_all, "true") == 0)
        rsm->send_to_all = 1;

    while(host_id != -1) {
        sprintf(buf, "p%d:host", host_id);
        const char* host = cfg_get(cfg, buf);
        host = host ? host : "localhost";
        int32_t port = SERVER_PORT + host_id;

        connection_cb_t client;
        client.recv  = connection_recv;
        client.close = NULL;
        client.done  = connection_send_done;

        printf("connecting to server %s:%d\n", host, port);
        // instead of hash i use host_id here
        tree_put(rsm->replicas, host_id,
                connection_init(rsm->loop, host, port,
                        &client, (void*) rsm));

        host_id = idset_it_next(replicas_iterator);
    }
    free(replicas_iterator);

    // create thread if necessary
    if (rsm->local_loop)
        pthread_create(&rsm->thread, NULL, run_loop, (void*) rsm->loop);

    // close cfg object
    cfg_close(cfg);

    return rsm;
}

rsm_client_t*
rsm_client_init(const char* fname, int32_t client_id, void* args)
{
    return rsm_client_initev(NULL, fname, client_id, args);
}

void
rsm_client_stop(rsm_client_t* rsm)
{
    tree_it_t* replicas_it = malloc(sizeof(tree_it_t));
    tree_it_init(rsm->replicas, replicas_it);
    void* replica = tree_it_next(replicas_it);
    while (replica != NULL) {
        connection_stop((connection_t*) replica);
        replica = tree_it_next(replicas_it);
    }

    if (rsm->local_loop) {
        // call async to wakeup thread if there is a thread
        ev_async_send(rsm->loop, &rsm->stop_w);
        // let thread stop async watcher when it leaves run_loop
        printf("stopping rsm loop... ");
        pthread_join(rsm->thread, 0);
        printf("done\n");
    } else; // async watcher shouldn't have been started (so nothing to do)
}

void
rsm_client_fini(rsm_client_t* rsm)
{
    assert(rsm);
    if (last_res)
        shrp_free(last_res);
#if 0
    idset_fini(rsm->replicas_idset);
    queue_free(rsm->requests);
    free(rsm);
    rsm = NULL;
#endif
}
