/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/shrp.h>
#include <ztas/log.h>
//#include <ztas/utils/crc.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "rsm_msg.h"
#include "utils/crc32c/crc32c.h"

static crc32c_f* crc_f = NULL;

rsmmsg_request_t*
rsmmsg_request_init(int32_t size)
{
    rsmmsg_request_t* msg = (rsmmsg_request_t*) shrp_malloc(size + sizeof(rsmmsg_request_t));
    assert (msg);
    bzero(msg, size + sizeof(rsmmsg_request_t));
    msg->header.type = RSM_REQUEST;
    msg->size = size;
    return msg;
}

rsmmsg_response_t*
rsmmsg_response_init(int32_t size)
{
    rsmmsg_response_t* msg = (rsmmsg_response_t*) shrp_malloc(size + sizeof(rsmmsg_response_t));
    assert (msg);
    bzero(msg, size + sizeof(rsmmsg_response_t));
    msg->header.type = RSM_RESPONSE;
    msg->size = size;
    return msg;
}

void
rsm_msg_seal(rsmmsg_request_t* msg, int32_t k, int32_t cid)
{
    assert (msg);
    msg->k   = k;
    msg->cid = cid;

    if (!crc_f) {
        // find the best implementation of CRC
        crc_f = crc32c_impl();
    }

    msg->mac = crc32c_finish(crc_f(crc32c_init(), (const void*)msg->data, msg->size));
}

int rsm_msg_verify(rsmmsg_request_t* msg) {
    assert (msg);

    if (!crc_f) {
        // find the best implementation of CRC
        crc_f = crc32c_impl();
    }

    uint32_t rmac = crc32c_finish(crc_f(crc32c_init(), (const void*)msg->data, msg->size));
    if (rmac != msg->mac) {
        LOG2("Corrupted request, ignore message (k = %d, cid = %d)\n", msg->k, msg->cid);
        return 0;
    }
    return 1;
}
