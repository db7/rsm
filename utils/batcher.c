/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <assert.h>
#include <ztas/shrp.h>
#include <ztas/mem.h>
#include <ztas/log.h>
#include <debug.h>

#include "batcher.h"
#include "../rsm_msg.h"

batcher_t*
batcher_init(int max_bytes, int max_messages)
{
    assert (max_messages <= BATCH_MAX_AMOUNT);
    batcher_t* batcher = (batcher_t*) malloc(sizeof(batcher_t));
    assert (batcher);
    batcher->max_bytes = max_bytes;
    batcher->max_messages = max_messages;
    batcher->bytes = 0;
    int i;
    for (i = 0; i < max_messages; ++i) {
        batcher->batch.messages[i] = NULL;
        batcher->batch.sizes[i] = 0;
    }
    batcher->batch.header.type = RSM_BATCH;
    batcher->batch.len = 0;
    batcher->header = NULL;
    return batcher;
}

void
batcher_fini(batcher_t* batcher)
{
    int i;
    for (i = 0; i < batcher->batch.len; ++i)
        if (batcher->batch.messages[i])
            shrp_free(batcher->batch.messages[i]);

    free(batcher);
}

void
batcher_new(batcher_t* batcher)
{
    batcher->header = rsmmsg_request_init(0);
    assert (batcher->header);
    int i;
    for (i = 0; i < batcher->batch.len; ++i) {
        batcher->batch.messages[i] = NULL;
        batcher->batch.sizes[i] = 0;
    }
    batcher->batch.messages[0] = batcher->header;
    batcher->batch.sizes[0]    = 0;
    batcher->batch.len         = 1;
    batcher->bytes = sizeof(*batcher->header);
    batcher->header->header.type = RSM_REQUEST;
    batcher->header->has_next = HASNEXT_HEAD;
}

int
batcher_add(batcher_t* batcher, rsmmsg_request_t* req)
{
    assert (batcher->header && "call new before calling add");
    uint64_t mbytes = sizeof(rsmmsg_request_t) + req->size;
    uint64_t bytes = batcher->bytes + mbytes;
    if (bytes > batcher->max_bytes) return BATCHER_FULL;

    int len = batcher->batch.len + 1;
    if (len > batcher->max_messages) return BATCHER_FULL;

    assert (batcher->batch.messages[batcher->batch.len] == NULL);
    assert (batcher->batch.sizes[batcher->batch.len] == 0);

    shrp_hold(req);
    batcher->batch.messages[batcher->batch.len] = req;
    batcher->batch.sizes[batcher->batch.len] = mbytes;
    batcher->batch.len = len;
    batcher->bytes = bytes;
    req->has_next = HASNEXT_YES;

    batcher->header->size += mbytes;

    if (len == batcher->max_messages) return BATCHER_READY;

    return BATCHER_OK;
}

const rsm_batch_t*
batcher_close(batcher_t* batcher)
{
    DLOG("BATCH SIZE %d \n", batcher->bytes);

    // last in batch
    assert (batcher->batch.len > 1);
    ((rsmmsg_request_t*) batcher->batch.messages[batcher->batch.len-1])->has_next = HASNEXT_NO;

    return &batcher->batch;
}

void
batcher_reopen(batcher_t* batcher)
{
    assert (batcher->batch.len > 1);
    ((rsmmsg_request_t*) batcher->batch.messages[batcher->batch.len-1])->has_next = HASNEXT_YES;
}

int
batcher_empty(batcher_t* batcher)
{
    return batcher->batch.len == 1;
}
