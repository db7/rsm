/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _BATCHER_H_
#define _BATCHER_H_

#include "../rsm_msg.h"

#define BATCH_MAX_AMOUNT 100
#define BATCHER_OK 0
#define BATCHER_FULL 1
#define BATCHER_READY 2

/*
 * rsm_batch has header type RSM_BATCH
 * It is internal RSM-to-Consensus message
 */
typedef struct {
	rsmmsg_t header;
    void* messages[BATCH_MAX_AMOUNT];
    uint64_t sizes[BATCH_MAX_AMOUNT];
    int len;
} rsm_batch_t;

/*
 * header is a shortcut to batch->message[0], this message does not have
 * a payload, but header->size indicates total size of messages from 1 to
 * batch->len, and header->has_next = HASNEXT_HEADER
 *
 * Batcher stores rsmmsg_requests in his `batch` field
 *
 * e.g., if we are to store 2 rsmmsg_requests, we have 3 messages in batch:
 *     message[0]->size = totalsize(request1) + totalsize(request2)
 *     message[0]->data = null
 *     message[1] = request1
 *     message[2] = request2
 */
typedef struct {
    int max_bytes;
    int max_messages;
    rsmmsg_request_t* header;
    rsm_batch_t batch;
    int bytes;                  // total size including header
} batcher_t;

batcher_t* batcher_init(int max_size, int max_messages);
void batcher_fini(batcher_t* batcher);

void batcher_new(batcher_t* batcher);
int  batcher_add(batcher_t* batcher, rsmmsg_request_t* req);

const rsm_batch_t* batcher_close(batcher_t* batcher);
void batcher_reopen(batcher_t* batcher);
int batcher_empty(batcher_t* batcher);

#endif
