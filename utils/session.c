/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <assert.h>
#include <ztas/mem.h>

#include "session.h"

session_t*
session_init(connection_t* connection)
{
    session_t* session = (session_t*) malloc(sizeof(session_t));
    assert (session);
    session->cid = 0;
    session->connection = connection;
    return session;
}

void
session_fini(session_t* session)
{
    free(session);
}
