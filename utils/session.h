/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _SESSION_H_
#define _SESSION_H_

#include <ztas/net/connection.h>
#include <stdint.h>

/*
 * session binds Client ID with actual connection to this client
 */
typedef struct {
    int32_t cid;
    connection_t* connection;
} session_t;

session_t* session_init(connection_t* connection);
void session_fini(session_t* session);

#endif
