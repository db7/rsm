/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#ifndef _RSM_SERVER_H_
#define _RSM_SERVER_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include <ev.h>
#include <ztas/shrp.h>
#include <ztas/ds/tree.h>
#include <ztas/ds/queue.h>
#include <ztas/net/connection.h>
#include <ztas/net/listener.h>
#include "rsm.h"
#include "rsm_msg.h"

typedef struct rsm_server rsm_server_t;

typedef int  (rsm_server_deliver_cb) (rsm_server_t* server,
        rsmmsg_request_t* req, void* args);
typedef void (rsm_server_response_cb)(rsm_server_t* server,
        rsmmsg_response_t* response, void* args);
typedef int  (rsm_server_snapshot_cb)(rsm_server_t* server,
        int32_t snap_id, void* args);

typedef struct {
    rsm_server_deliver_cb*  deliver;
    rsm_server_response_cb* response;
    rsm_server_snapshot_cb* snapshot;
} rsm_server_cbs_t;

/*
 * UNUSED
 */
 typedef struct {
     void*    data;
     uint64_t size;
     connection_t* conn;
 } message_t;

 struct rsm_server {
     rsm_t* rsm;
     listener_t* listener;
     void* args;
     queue_t* requests;                  // UNUSED
     queue_t* responses;                 // UNUSED
     tree_t* sessions;

     int seal_requests;  // clients seal requests, server has to unseal & check

     int buffer_size;
     unsigned cid;

     rsm_server_deliver_cb*   deliver;
     rsm_server_snapshot_cb* snapshot;

     struct ev_loop* loop;
     pthread_t thread;
     int local_loop;
     struct ev_timer halt_timer;
     struct ev_timer die_timer;
 };

 rsm_server_t* rsm_server_init(struct ev_loop* loop,
         int replica,
         const char* fname,
         rsm_server_cbs_t* server_cbs,
         void* args);
 void          rsm_server_stop(rsm_server_t* rsm);
 void          rsm_server_fini(rsm_server_t* rsm);
 void          rsm_server_response(rsm_server_t* rsm, rsmmsg_response_t* res);
 void          rsm_server_snapdone(rsm_server_t* server, int32_t snap_id);

#endif /* _RSM_SERVER_H_ */
