/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/shrp.h>
#include <ztas/log.h>
#include <ztas/proc/proc.h>
#include <ztas/proc/cfg.h>
#include <ztas/ds/tree.h>
#include <ztas/ds/queue.h>
#include <ztas/msg.h>
#include <ztas/flags.h> // ZTAS_UCAST_OK
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include <string.h>

#include "rsm.h"
#include "utils/batcher.h"
#include "rsm_msg.h"

//#define DEBUG
#include <debug.h>

#define MAX_RETRY_DELAY2 1
#define MAX_RETRY_DELAY  1
#define MIN_RETRY_DELAY  1 // 1 s

static void batch_flush(struct ev_loop* loop, struct ev_timer* w, int revents);
static int rsm_cansend(rsm_t* rsm, int yes);


/* -----------------------------------------------------------------------------
 *   Handle requests
 * -------------------------------------------------------------------------- */

/*
 * Called by RSM Server when request comes from RSM Client.
 * Add request to batcher; if batcher is full then send the previous batch
 * to Replica. Also reset the timer for Batcher.
 */
int32_t
rsm_request(rsm_t* rsm, rsmmsg_request_t* req)
{
    if (!rsm->can_send) {
        DLOG("BACK PRESSURE\n");
        return ZTAS_UCAST_FULL;
    }

#if 0
    shrp_hold(req);
    void* rv = tree_put(rsm->requests, req->header.hash, req);
    if (rv) {
        ELOG("Request hash %u was already sent!\n", req->header.hash);
        shrp_free(req);
    }
#endif

    // complete headers
    req->header.type = RSM_REQUEST;
#if 0
    ztas_msg_init(req, rsm->pid, 0);
#endif

    // reset timer
//    ev_timer_again(rsm->loop, &rsm->flush_timer);

    // add message to batch
    DLOG("request into batch cid = %d, k = %d\n", req->cid, req->k);
    int32_t r = batcher_add(rsm->batcher, req);

    int cs = 1; // by default assume that system is overloaded always
    if (rsm->rcbs.issaturated)
        cs = rsm->rcbs.issaturated(rsm->binded_replica);

    if (r == BATCHER_OK && cs) {
        // there is still room for batching AND system is overloaded
        // need to batch and go out
        return ZTAS_UCAST_OK;
    }

    // batcher is full or system is unloaded, send this batch
    const rsm_batch_t* batch = batcher_close(rsm->batcher);

    int rc = proc_lsend(rsm->replica, (void*) batch, sizeof(rsm_batch_t));

    // reset timer for batch
    rsm->retry_delay += MIN_RETRY_DELAY;
    ev_timer_again(rsm->loop, &rsm->flush_timer);

    batcher_new(rsm->batcher);

    if (r == BATCHER_FULL) {
        // req wasn't added previously
        // add it now to the new batch
        r = batcher_add(rsm->batcher, req);
        assert (r != BATCHER_FULL);
    }

    return rc;
}

/* -----------------------------------------------------------------------------
 *   Handle responses
 * -------------------------------------------------------------------------- */

/*
 * Called by RSM Server when actual Server generates a response.
 * We let RSM Server send the response by calling cbs.response()
 * and also send response to Replica so it can start pruning.
 */
int
rsm_response(rsm_t* rsm, rsmmsg_response_t* res)
{
    if (!rsm->can_send) return ZTAS_UCAST_FULL;

#if 0
    // retrieve from tree the request for this response
    rsm_msg_t* r = tree_pop(rsm->requests, res->header.hash);
    if (r) {
        // free request
        shrp_free(r);
        rsm->cbs.response(rsm, res, rsm->args);
        // store reponse in tree with hash of request (move to another tree)
        // tree_put(rsm->responses, res->hash, res);
    } else {
        DLOG("Could not find request with hash %d\n", res->header.hash);
    }
#else
    rsm->cbs.response(rsm, res, rsm->args);
#endif
    if (res->has_next == HASNEXT_NO) {
        // consensus message committed, try to flush current batch
        int cs = 1; // by default assume that system is overloaded always
        if (rsm->rcbs.issaturated)
            cs = rsm->rcbs.issaturated(rsm->binded_replica);

        if (cs == 0 && rsm->can_send && !batcher_empty(rsm->batcher)) {
            // consensus protocol is not overloaded, flush batch
            const rsm_batch_t* batch = batcher_close(rsm->batcher);
            int rc = proc_lsend(rsm->replica, (void*) batch, sizeof(rsm_batch_t));

            if (rc == ZTAS_UCAST_OK) {
//                LOG("I FLUSHED\n");
                batcher_new(rsm->batcher);
            }
            else
                batcher_reopen(rsm->batcher);
        }

        // create response to replica (so that replica can generate
        // prune messages and do other cleanup)
        rsmmsg_response_t* pres = rsmmsg_response_init(0);
#if 0
        pres->replica   = rsm->pid;
        pres->slot      = res->slot;
        ztas_msg_init(pres, rsm->pid, 0);
#endif

        DLOG("---> Sending response slot %d to leader (cid = %d, k = %d)\n",
                pres->slot, res->cid, res->k);

        // put in ztas loop the request
        int r = proc_lsend(rsm->replica, pres, sizeof(rsmmsg_response_t));

        shrp_free(pres);
        return r;
    } else {
#ifdef DEBUG
        DLOG("---> not sending response slot %d to leader (cid = %d, k = %d)\n",
                res->slot, res->cid, res->k);
#endif
        return ZTAS_UCAST_OK;
    }
}

/* -----------------------------------------------------------------------------
 *   Handle snapshots
 * -------------------------------------------------------------------------- */

/*
 * RSM Server calls rsm_snapdone when actual Server is done snapshoting.
 * NB: function is usually called from another thread, must be thread-safe!
 */
int32_t
rsm_snapdone(rsm_t* rsm, int32_t snap_id)
{
    if (rsm->rcbs.setsnapdone) {
        // call "snapshot done" routine of consensus directly
        // NB: thread safe
        return rsm->rcbs.setsnapdone(rsm->binded_replica, snap_id);
    } else {
        // no direct access to consensus -- then send local message
        // NB: not thread safe
        rsmmsg_snapdone_t snap;
        snap.header.type = RSM_SNAPDONE;
        snap.replica   = rsm->pid;
        snap.slot      = snap_id;
#if 0
        ztas_msg_init(&snap, rsm->pid, 0);
#endif

        // put in ztas loop the request
        return proc_lsend(rsm->replica, (void*)&snap, sizeof(rsmmsg_snapdone_t));
    }
}

/* -----------------------------------------------------------------------------
 * helper functions
 * -------------------------------------------------------------------------- */
void
rsm_bind_replica(rsm_t* rsm, replica_cbs_t* rcbs, void* binded_replica)
{
    memcpy(&rsm->rcbs, rcbs, sizeof(replica_cbs_t));
    rsm->binded_replica = binded_replica;
}

int
rsm_cansend(rsm_t* rsm, int yes)
{
    rsm->can_send = yes;
    return 0;
}

void
batch_flush(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    rsm_t* rsm = (rsm_t*) w->data;
    assert (rsm);

    if (!rsm->can_send) {
        rsm->retry_delay += MIN_RETRY_DELAY*10;
        if (rsm->retry_delay > MAX_RETRY_DELAY2) rsm->retry_delay = MAX_RETRY_DELAY2;
        ev_timer_set(w, 0., rsm->retry_delay);
        ev_timer_again(loop, w);
        return;
    }

    if (!batcher_empty(rsm->batcher)) {
        const rsm_batch_t* batch = batcher_close(rsm->batcher);

        DLOG("flush cid = %d, k = %d, next = %d\n",
                batch->messages[0]->cid,
                batch->messages[0]->k,
                batch->messages[0]->has_next);

        int rc = proc_lsend(rsm->replica, (void*) batch, sizeof(rsm_batch_t));

        if (rc == ZTAS_UCAST_OK) {
            DLOG("Batch timeout len = %d\n", batch->len);
            batcher_new(rsm->batcher);
            rsm->retry_delay /= 2;
            if (rsm->retry_delay < MIN_RETRY_DELAY) rsm->retry_delay = MIN_RETRY_DELAY;
            ev_timer_set(w, 0., rsm->retry_delay);
        } else {
            batcher_reopen(rsm->batcher);
        }
        ev_timer_again(loop, w);
    }
}

/* -----------------------------------------------------------------------------
 * init/fini
 * -------------------------------------------------------------------------- */
void*
run_server(void* args)
{
    ev_loop((struct ev_loop*) args, 0);
    pthread_exit(0); // clear pthread
    return NULL;
}

rsm_t*
rsm_initev(struct ev_loop* loop,
        int32_t pid, const char* fname,
        const rsm_cbs_t* cbs,
        void* args)
{
    rsm_t* rsm = (rsm_t*) malloc(sizeof(rsm_t));
    assert (rsm);
    rsm->loop = loop;
    rsm->pid  = pid;
    rsm->args = args;
    //rsm->responses = tree_init();
    rsm->requests = tree_init();
    rsm->can_send = 1;
    //rsm->free_req  = queue_create(1000);
    rsm->threaded  = 0;
    memcpy(&rsm->cbs, cbs, sizeof(rsm_cbs_t));
    rsm->cbs.bindreplica = rsm_bind_replica;
    rsm->binded_replica = NULL;

    // parse configuration file
    cfg_t* cfg = cfg_parse(fname);
    rsm->cfg = cfg;

    // create batcher (default batch size BATCH_MAX_AMOUNT)
    const char* bs = cfg_get(cfg, "rsm:batch_size");
    int batch_size = bs ? atoi(bs): BATCH_MAX_AMOUNT;

    printf("[RSM] batch size = %d\n", batch_size);
    rsm->batcher   = batcher_init(1024*1024*10, batch_size);
    batcher_new(rsm->batcher);

    /* we use the ev_timer_again trick here  */
    rsm->retry_delay = MIN_RETRY_DELAY;
    ev_timer_init(&rsm->flush_timer, batch_flush, 0., rsm->retry_delay);
    ev_set_priority(&rsm->flush_timer, -2);
    rsm->flush_timer.data = (void*) rsm;

#if 0
    // place free requests in free req
    int i = 0;
    for (i = 0; i < 100; ++i) {
        request_t* req = (request_t*) shrp_malloc(sizeof(request_t));
        int rc = queue_enq(rsm->free_req, req);
        assert (rc == QUEUE_OK);
    }
#endif

    const char* mod = cfg_get(cfg, "rsm:mod_replica");
    if (mod) {
        char key[128];
        sprintf(key, "p%d:mod", pid);
        cfg_set(cfg, key, mod);
        rsm->replica = proc_init_cfg(loop, pid, cfg);
    } else rsm->replica = proc_init_cfg(loop, pid, cfg);

    // create local leader
    mod = cfg_get(cfg, "rsm:mod_leader");
    if (mod) {
        char key[128];
        sprintf(key, "p%d:mod", pid+10);
        cfg_set(cfg, key, mod);
        rsm->leader = proc_init_cfg(NULL, pid+10, cfg);
    } else rsm->leader = NULL;

    // create local acceptor
    mod = cfg_get(cfg, "rsm:mod_acceptor");
    if (mod) {
        char key[128];
        sprintf(key, "p%d:mod", pid+20);
        cfg_set(cfg, key, mod);
        rsm->acceptor = proc_init_cfg(NULL, pid + 20, cfg);
    } else rsm->acceptor = NULL;

    // messages are sealed on client-side; unseal & check when received
    const char* seal_requests_str = cfg_get(cfg, "rsm:seal_requests");
    int seal_requests = 0;
    if (seal_requests_str && strcmp(seal_requests_str, "true") == 0)
        seal_requests = 1;

    rsmmsg_config_t conf_msg;
    conf_msg.header.type   = RSM_CONFIG;
    conf_msg.args          = rsm->args;
    conf_msg.rsm           = rsm;
    conf_msg.seal_requests = seal_requests;
    memcpy(&conf_msg.cbs, &rsm->cbs, sizeof(rsm_cbs_t));
    conf_msg.cbs.cansend = rsm_cansend;
    int32_t r = proc_lsend(rsm->replica, (void*)&conf_msg, sizeof(rsmmsg_config_t));
    assert (r == 0);

    return rsm;
}

rsm_t*
rsm_init(int32_t pid, const char* fname,
        const rsm_cbs_t* cbs,
        void* args)
{
    struct ev_loop* loop = ev_loop_new(0);
    rsm_t* rsm = rsm_initev(loop, pid, fname, cbs, args);
    assert (rsm);

    // create thread for ztas
    pthread_create(&rsm->thread, NULL, run_server, loop);
    rsm->threaded = 1;

    return rsm;
}

void
rsm_stop(rsm_t* rsm)
{
    // stop server thread and join
    proc_stop(rsm->replica);

    if (rsm->leader)
        proc_stop(rsm->leader);

    if (rsm->acceptor)
        proc_stop(rsm->acceptor);

    ev_timer_stop(rsm->loop, &rsm->flush_timer);
}

void
rsm_fini(rsm_t* rsm)
{

    if (rsm->threaded)
        pthread_join(rsm->thread,NULL);

    // fini processes
    proc_fini(rsm->replica);
    if (rsm->leader)
        proc_fini(rsm->leader);

    if (rsm->acceptor)
        proc_fini(rsm->acceptor);

    // free requests
#if 0
    while (!queue_empty(rsm->free_req)) {
        shrp_free(queue_deq(rsm->free_req));
    }
    queue_free(rsm->free_req);
#endif

    // free requests
    rsmmsg_request_t* r = NULL;
    while ((r = tree_popdf(rsm->requests)) != NULL) {
        //shrp_free(r->req);
        shrp_free(r);
    }
    tree_fini(rsm->requests);

    // free responses
#if 0
    while ((r = tree_popdf(rsm->responses)) != NULL) {
        shrp_free(r->req);
        shrp_free(r);
    }
    tree_fini(rsm->responses);
#endif

    // free batcher
    batcher_fini(rsm->batcher);

    // free cfg if any
    if (rsm->cfg) cfg_close(rsm->cfg);

    free(rsm);
}
