/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
/**
 * \file  rsm.h
 * \brief A replicated state machine core
 */
#ifndef _RSM_H_
#define _RSM_H_

#include <ev.h>
#include <ztas/proc/proc.h>
#include <ztas/ds/tree.h>
#include "rsm_msg.h"
#include "utils/batcher.h"

#define BUFFER_SIZE 8192*1024
#define SERVER_PORT 5000

typedef struct rsm rsm_t;
typedef struct rsm_cbs rsm_cbs_t;
typedef struct replica_cbs replica_cbs_t;

/* -- callbacks ------------------------------------------------------------ */

typedef int  (rsm_deliver_cb) (rsm_t* rsm, rsmmsg_request_t* req, void* args);
typedef void (rsm_response_cb)(rsm_t* rsm, rsmmsg_response_t* response, void* args);
typedef int  (rsm_snapshot_cb)(rsm_t* rsm, int32_t snap_id, void* args);
typedef int  (rsm_snapload_cb)(rsm_t* rsm, int32_t snap_id, void* data, size_t size, void* args);
typedef int  (rsm_snapread_cb)(rsm_t* rsm, int32_t snap_id, void** data, size_t* size, void* args);
typedef int  (rsm_cansend_cb)(rsm_t* rsm, int yes);
typedef void (rsm_bindreplica_cb)(rsm_t* rsm, replica_cbs_t* rcbs, void* binded_replica);

struct rsm_cbs {
    rsm_deliver_cb*  deliver;
    rsm_response_cb* response;
    rsm_snapshot_cb* snapshot;
    rsm_snapload_cb* snapload;
    rsm_snapread_cb* snapread;
    rsm_cansend_cb* cansend;
    rsm_bindreplica_cb* bindreplica;
};

typedef int (replica_issaturated_cb) (void* binded_replica);
typedef int (replica_isleader_cb)    (void* binded_replica);
typedef int (replica_setsnapdone_cb) (void* binded_replica, int32_t snap_id);
typedef int (replica_setdying_cb)    (void* binded_replica);

struct replica_cbs {
    replica_issaturated_cb*  issaturated;
    replica_isleader_cb*     isleader;
    replica_setsnapdone_cb*  setsnapdone;
    replica_setdying_cb*     setdying;
};

/* -- rsm object ------------------------------------------------------------ */

/** Represents the rsm in the client
 */
struct rsm {
    int32_t pid;

    void* args;

    tree_t* requests;
    int can_send;

    int threaded;
    pthread_t thread;

    proc_t* replica;
    proc_t* leader;
    proc_t* acceptor;

    rsm_cbs_t cbs;
    cfg_t* cfg;

    replica_cbs_t rcbs;
    void* binded_replica;

    struct ev_loop* loop;
    struct ev_timer flush_timer;
    batcher_t* batcher;
    float retry_delay;  // timeout to flush the batch
};

/* -- config message -------------------------------------------------------- */

typedef struct {
    rsmmsg_t header;
    rsm_cbs_t cbs;      // callbacks
    void* args;         // args for deliver cb
    rsm_t* rsm;
    int seal_requests;  // seal requests and batches
} rsmmsg_config_t;

/* -- management ------------------------------------------------------------ */

rsm_t*  rsm_init(int32_t pid, const char* fname,
        const rsm_cbs_t* cb, void* args);

rsm_t*  rsm_initev(struct ev_loop* loop, int32_t pid, const char* fname,
        const rsm_cbs_t* cb, void* args);

void rsm_stop(rsm_t* rsm);
void rsm_fini(rsm_t* rsm);


/* -- client side ----------------------------------------------------------- */

// returns 0 if everything fine otherwise <0
int rsm_request(rsm_t* rsm, rsmmsg_request_t* req);

/* -- server side ----------------------------------------------------------- */
int rsm_response(rsm_t* rsm, rsmmsg_response_t* res);
int rsm_snapdone(rsm_t* rsm, int32_t snap_id);

#endif /* _RSM_H_ */
