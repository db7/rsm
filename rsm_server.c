/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/shrp.h>
#include <ztas/log.h>
#include <ztas/ds/tree.h>
#include <ztas/ds/queue.h>
#include <ztas/net/listener.h>
#include <ztas/proc/cfg.h>
#include <ztas/flags.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>

#include "rsm.h"
#include "rsm_server.h"
#include "utils/session.h"
#include "rsm_msg.h"

#include <debug.h>

static int rsm_server_deliver(rsm_t* rsm, rsmmsg_request_t* req, void* args);


/* -----------------------------------------------------------------------------
 *   Handle requests
 * -------------------------------------------------------------------------- */

/*
 * Connection calls connection_recv() callback (through connection_recv_cb)
 * when Client request comes from network.
 * We should retrieve the whole rsmmsg_request message and send it up
 * to RSM engine. In case we don't use RSM, simply deliver this request.
 */
static uint64_t
connection_recv(connection_t* connection, void** data, ssize_t* read, void* args)
{
    DLOG("receiving data connection %p size %ld \n", connection, *read);

    assert (data && read && args);
    rsm_server_t* server = (rsm_server_t*) args;
    int32_t tsize = sizeof(rsmmsg_request_t);

    if (*data == NULL) {
        // no buffer yet, provide it
        *read = 0;

        assert(server->buffer_size > 0);
        *data = (void*) shrp_malloc(server->buffer_size);
        return tsize;
    }

    if (*data && *read == 0) {
        // client disconnected
        shrp_free(*data);
        return 0;
    }

    assert (*read > 0);
    if (*read < tsize) {
        return tsize - *read;
    }

    rsmmsg_request_t* req = (rsmmsg_request_t*) *data;
    size_t total_size = sizeof(rsmmsg_request_t) + req->size;

    if (total_size > server->buffer_size) {
        server->buffer_size = total_size;
        void* buf = shrp_realloc(*data, server->buffer_size);
        assert(buf && "Allocation failed");
        *data = buf;
        return server->buffer_size - *read;
    }

    if (*read < total_size) {
        return total_size - *read;
    }
    assert (*read == total_size);

    DLOG("Request from client k = %d\n", req->k);

    // do we have a session with this client right now?
    session_t* session = tree_get(server->sessions, req->k);
    if (session) {
        // if yes, the connection should be the same!
        if (session->connection != connection) {
            DLOG("connection in session differs from request connection\n");
        }
    } else {
        // we don't have session with the client. let's create one.
        session = session_init(connection);
        void* p = tree_put(server->sessions, req->k, (void*) session);
        assert (!p);
    }

    if (server->seal_requests && !rsm_msg_verify(req))
        goto finalize;

    if (server->rsm) {
        // send request to RSM engine
        int r = rsm_request(server->rsm, req);
        if (r != ZTAS_UCAST_OK) {
            DLOG("Dropping client request (k = %d cid = %d)\n", req->k, req->cid);
        } else {
            DLOG("Sending client request (k = %d cid = %d)\n", req->k, req->cid);
        }
    } else {
        // don't use RSM engine, simply deliver the request
        rsm_server_deliver(NULL, req, server);
    }

finalize:
    // next request
    shrp_free(req);
    *data = NULL;
    *read = 0;
    return 0;
}

/*
 * Called by RSM Engine or by RSM Server (in case no RSM Engine used) when
 * request is to be delivered.
 * We make an upcall to Server.
 */
static int
rsm_server_deliver(rsm_t* rsm, rsmmsg_request_t* req, void* args)
{
    assert (args);
    rsm_server_t* server = (rsm_server_t*) args;

	req->flags &= ~0x01;
	if (rsm->rcbs.isleader)
		if (rsm->rcbs.isleader(rsm->binded_replica))
			req->flags |= 0x01;

    server->deliver(server, req, server->args);
    return 0;
}

/* -----------------------------------------------------------------------------
 * Handle responses
 * -------------------------------------------------------------------------- */

/*
 * Called by RSM Engine or by Server (in case no RSM Engine used) when
 * response is generated.
 * Simply sends rsmmsg_response to RSM Client.
 */
static void
send_response(rsm_t* rsm, rsmmsg_response_t* res, void* arg)
{
    rsm_server_t* server = (rsm_server_t*) arg;

    DLOG("received response for request %d (k = %d, slot = %d) \n",
            res->cid, res->k, res->slot);

    session_t* session = tree_get(server->sessions, res->k);
    if (!session || !session->connection) return;

#if defined(DEBUGX)
    char buffer[4096];
    memcpy(buffer, m->data, m->size);
    buffer[m->size] = '\0';
    DLOG("REQUEST WAS: %s \n", buffer);
#endif

    // reply request to client
    DLOG("replying client request %d (k = %d, slot = %d) \n",
            res->cid, res->k, res->slot);

    void* data = (void*) res;
    int32_t tsize = res->size + sizeof(rsmmsg_response_t);

    // will be free in send_response_done
    shrp_holdcpy(&data, tsize);

    int r = connection_send(session->connection, data, tsize);
    assert (r == CONNECTION_SEND_OK);
}

/*
 * Connection calls send_response_done() callback (through connection_done_cb)
 * when response is sent to network.
 * We should free data we allocated (by schrp_hold) in send_response().
 */
static void
send_response_done(connection_t* connection, void* data, void* args)
{
    shrp_free(data);
}


/*
 * Server calls rsm_server_response() when he delivered request
 * and made a response res.
 * We either notify RSM Engine about this, or send response directly
 * to Client if there is no RSM Engine.
 */
void
rsm_server_response(rsm_server_t* server, rsmmsg_response_t* res)
{
    if (server->rsm)
         rsm_response(server->rsm, res);
    else {
        // no RSM Engine, simply send response to RSM Client
        send_response(NULL, res, server);
    }
}

/* -----------------------------------------------------------------------------
 * Handle snapshots
 * -------------------------------------------------------------------------- */

/*
 * RSM Engine calls rsm_server_snapshot callback (through rsm_snapshot_cb).
 * We simply make upcall to Server, snapshoting is his responsibility.
 */
static int
rsm_server_snapshot(rsm_t* rsm, int32_t snap_id, void* args)
{
    assert (args);
    rsm_server_t* server = (rsm_server_t*) args;
    server->snapshot(server, snap_id, args);
    return 1;
}

/*
 * Server calls rsm_server_snapdone() when he made a snapshot.
 * We forward this news to RSM Engine.
 */
void
rsm_server_snapdone(rsm_server_t* server, int32_t snap_id)
{
    rsm_snapdone(server->rsm, snap_id);
}

/* -----------------------------------------------------------------------------
 * init/fini
 * -------------------------------------------------------------------------- */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <strings.h>
#include <string.h>

static void
halt_timer_cb(struct ev_loop* loop, struct ev_timer* watcher, int revents)
{
    rsm_server_t* server = (rsm_server_t*) watcher->data;
    assert (server);
    rsm_server_stop(server);
}

static void
die_timer_cb(struct ev_loop* loop, struct ev_timer* watcher, int revents)
{
    rsm_server_t* server = (rsm_server_t*) watcher->data;
    assert (server);

    if (server->rsm->rcbs.isleader)
        if (server->rsm->rcbs.isleader(server->rsm->binded_replica)) {
            // this replica is really the leader, kill myself
            // NB: only for hardpaxos
            server->rsm->rcbs.setdying(server->rsm->binded_replica);
            LOG("[rsm_server] start killing Leader (will die right before snapshot)\n");
//            rsm_server_stop(server);
        }
}

/* prototypes */

static void*
run_loop(void* args)
{
    struct ev_loop* loop = (struct ev_loop*) args;
    ev_loop(loop, 0);
    return NULL;
}

rsm_server_t*
rsm_server_init(struct ev_loop* loop, int replica, const char* fname,
        rsm_server_cbs_t* server_cbs, void* args)
{
    assert (server_cbs);

    rsm_server_t* server = malloc(sizeof(rsm_server_t));
    assert (server);

    server->requests      = queue_create(1000);
    server->responses     = queue_create(1000);
    server->buffer_size   = BUFFER_SIZE;
    server->cid           = 0;
    server->args          = args;
    server->local_loop    = 0;
    server->deliver       = server_cbs->deliver;
    server->snapshot      = server_cbs->snapshot;
    server->sessions      = tree_init();

    server->seal_requests  = 0;

    // create event loop
    if (loop == NULL) {
        server->loop = ev_loop_new(0);
        assert (server->loop);
        server->local_loop = 1;
    } else server->loop = loop;

    connection_cb_t cbs;
    cbs.recv  = connection_recv;
    cbs.done  = send_response_done;
    cbs.close = NULL;

    server->listener = listener_init(server->loop, SERVER_PORT + replica, &cbs,
            (void*) server);

    // initilize rsm
    int32_t pid = replica;
    cfg_t* cfg = cfg_parse(fname);

    const char* enabled = cfg_get(cfg, "rsm:enabled");
    if (!enabled || strcmp(enabled, "true") == 0) {
        // rsm is enabled by default
        rsm_cbs_t cbs2;
        cbs2.response = send_response;
        cbs2.deliver  = rsm_server_deliver;
        cbs2.snapshot = rsm_server_snapshot;
        server->rsm = rsm_initev(server->loop, pid, fname, &cbs2,
                (void*) server);
    } else {
        // to disable rsm, set in rsm.ini "[rsm]\nenabled=false"
        server->rsm = NULL;
    }

    // create thread if necessary
    if (server->local_loop)
        pthread_create(&server->thread, NULL, run_loop, (void*) server->loop);

    // should we halt?
    const char* halt_time = cfg_get(cfg, "main:halt_after");
    if (halt_time) {
        double halt_time_s = atof(halt_time); // in seconds
        ev_timer_init(&server->halt_timer, halt_timer_cb, halt_time_s, 0.);
        server->halt_timer.data = (void*) server;
        if ((int) halt_time_s != 0)
            ev_timer_start(loop, &server->halt_timer);
    } else {
        ev_timer_init(&server->halt_timer, halt_timer_cb, 0., 0.);
        // will never be called since we do not call start
    }

    // messages are sealed on client-side; unseal & check when received
    const char* seal_requests_str = cfg_get(cfg, "rsm:seal_requests");
    if (seal_requests_str && strcmp(seal_requests_str, "true") == 0)
        server->seal_requests = 1;
    LOG1("[rsm_server] sealing requests: %d\n", server->seal_requests);

    // kill leader after die_after seconds
    const char* die_time = cfg_get(cfg, "main:die_after");
    if (die_time) {
        double die_time_s = atof(die_time); // in seconds
        ev_timer_init(&server->die_timer, die_timer_cb, die_time_s, 0.);
        server->die_timer.data = (void*) server;
        if ((int) die_time_s != 0)
        	ev_timer_start(loop, &server->die_timer);
    } else {
        ev_timer_init(&server->die_timer, die_timer_cb, 0., 0.);
        // will never be called since we do not call start
    }

    cfg_close(cfg);

    return server;
}

void
rsm_server_stop(rsm_server_t* server)
{
    // acceptor_stop
    listener_stop(server->listener);

    // watchers
    ev_timer_stop(server->loop, &server->halt_timer);

    // rsm stop
    if (server->rsm)
        rsm_stop(server->rsm);

    if (server->local_loop) {
        // join with server->thread
    }

    ev_unloop (server->loop, EVUNLOOP_ALL);

//    exit(0);
}

void
rsm_server_fini(rsm_server_t* server)
{
    // finish rsm library
    if (server->rsm)
        rsm_fini(server->rsm);

    // free data structures
    while(!queue_empty(server->responses)) {
        shrp_free(queue_deq(server->responses));
    }
    queue_free(server->responses);

    while(!queue_empty(server->requests)) {
        shrp_free(queue_deq(server->requests));
    }
    queue_free(server->requests);

    // TODO:
    // session_t* session;
    // tree_popdf for sessions
    tree_fini(server->sessions);

    listener_fini(server->listener);
    free(server);
}
