/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include "sqlitefmt.h"

/*
 * convert SELECT result table into JSON
 * original code taken from
 *   http://stackoverflow.com/questions/3414729/export-sqlite3-table-to-a-text-file-from-c-program?rq=1
 */
int
sqlitedb_json(sqlite3_stmt *stmt, char *outbuf, size_t buflen, size_t *used) {
   int nuse = 0;
   int ncolumn = sqlite3_column_count(stmt);

   int i=0;
   int res;

   nuse += snprintf(outbuf, buflen, "[\n");

   while ( (res = sqlite3_step(stmt)) == SQLITE_ROW ) {
       nuse += snprintf(outbuf + nuse, buflen - nuse, "\t{\n");
       for (i = 0; i < ncolumn; ++i) {
           nuse += snprintf(outbuf + nuse, buflen - nuse,
                           "\t\t\"%s\" : ", sqlite3_column_name(stmt, i));
           switch (sqlite3_column_type(stmt, i)) {
               case SQLITE_INTEGER:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                                   "%d,\n", sqlite3_column_int(stmt, i));
                   break;
               case SQLITE_FLOAT:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                                   "%e,\n", sqlite3_column_double(stmt, i));
                   break;
               case SQLITE_TEXT:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                           "\"%s\",\n", sqlite3_column_text(stmt, i));
                   break;
               case SQLITE_BLOB:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                           "\"%s\",\n", (char*) sqlite3_column_blob(stmt, i));
           }
       }

       nuse -= 2; // trailing ',\n'
       nuse+=snprintf(outbuf+nuse,buflen-nuse, "\n\t},\n");
       if( buflen < nuse ) {
           printf("sqlite3_json: buffer overflow\n");
           break;
       }
   }
   if (i && res == SQLITE_DONE) nuse-=2; //trailing ',\n'
   nuse += snprintf(outbuf + nuse, buflen - nuse, "\n]\n");
   if (used) *used = nuse;
   return res;
}

/*
 * convert SELECT result table into CVS (very simple version)
 *   delimiter: "," (comma, no spaces)
 *   all text and blob values are quoted
 */
int
sqlitedb_csv(sqlite3_stmt *stmt, char *outbuf, size_t buflen, size_t *used) {
   int nuse = 0;
   int i=0;
   int ncolumn;
   int res;

   if ( (ncolumn = sqlite3_column_count(stmt)) > 0 ) {
       // --- write column header
       for (i = 0; i < ncolumn; ++i)
           nuse += snprintf(outbuf + nuse, buflen - nuse,
                           "\"%s\",", sqlite3_column_name(stmt, i));
       nuse--;  // ignore trailing ","
       nuse += snprintf(outbuf + nuse, buflen - nuse, "\n");
   }

   // --- write data
   while ( (res = sqlite3_step(stmt)) == SQLITE_ROW ) {
       for (i = 0; i < ncolumn; ++i) {
           switch (sqlite3_column_type(stmt, i)) {
               case SQLITE_INTEGER:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                                   "%d,", sqlite3_column_int(stmt, i));
                   break;
               case SQLITE_FLOAT:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                                   "%e,", sqlite3_column_double(stmt, i));
                   break;
               case SQLITE_TEXT:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                           "\"%s\",", sqlite3_column_text(stmt, i));
                   break;
               case SQLITE_BLOB:
                   nuse += snprintf(outbuf + nuse, buflen - nuse,
                           "\"%s\",", (char*) sqlite3_column_blob(stmt, i));
           }
       }

       nuse--;  // ignore trailing ","
       nuse += snprintf(outbuf + nuse, buflen - nuse, "\n");

       if( buflen < nuse ) {
           printf("sqlite3_json: buffer overflow\n");
           break;
       }
   }

   if (used) *used = nuse;
   return res;
}
