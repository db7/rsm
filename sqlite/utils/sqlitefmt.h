/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#ifndef SQLITEFMT_H_
#define SQLITEFMT_H_

int sqlitedb_json(sqlite3_stmt *stmt, char *outbuf, size_t buflen, size_t *used);
int sqlitedb_csv(sqlite3_stmt *stmt, char *outbuf, size_t buflen, size_t *used);

#endif /* SQLITEFMT_H_ */
