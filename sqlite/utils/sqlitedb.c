/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include <ztas/ztime.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <sqlite3.h>
#include "sqlitedb.h"

// fastest performance by default
int SQLITE3DB_NUM_PAGES   = 2000;
int SQLITE3DB_PAGE_SIZE   = 1024;
int SQLITE3DB_SYNCHRONOUS = 0;
int SQLITE3DB_WAL_ENABLED = 0;
int SQLITE3DB_INMEMORY    = 1;
int SQLITE3DB_BACKUP      = 0; // 0 -- no backup; 1 -- sync; 2 -- async

static char* SQLITE_DIR  = "/tmp/sqlite-rsm";
static char* SQLITE_SNAP_DIR  = "/tmp/sqlite-rsm";
static char* SQLITE_NAME = "%s/rsm_sqlite3-%02d.db";
static char* SQLITE_SNAP = "%s/rsm_sqlite3-%02d-snap%010d.db";

#define DEBUG_SNAPSHOT

/* -----------------------------------------------------------------------------
 *  filesystem helpers
 * -------------------------------------------------------------------------- */
static int get_dir(char* result) {
    char buf[100];
    snprintf(buf, sizeof(buf), "%s", SQLITE_DIR);
    if (mkdir(buf, 0755) != 0)
        fprintf(stdout, "Cannot create directory %s (probably already exists?)\n", buf);

    if (result)
        strncpy(result, buf, sizeof(buf));
    return 0;
}

/* -----------------------------------------------------------------------------
 *  SQL helpers
 * -------------------------------------------------------------------------- */

void sqlitedb_exec_check(int status, char *err_msg) {
  if (status != SQLITE_OK) {
    fprintf(stderr, "SQL error: %s\n", err_msg);
    sqlite3_free(err_msg);
  }
}

void sqlitedb_step_check(int status) {
  if (status != SQLITE_DONE) {
    fprintf(stderr, "SQL step error: status = %d\n", status);
  }
}

void sqlitedb_error_check(int status) {
  if (status != SQLITE_OK) {
    fprintf(stderr, "sqlite3 error: status = %d\n", status);
  }
}

sqlite3* sqlitedb_open(int replica) {
    sqlite3* db = NULL;

    fprintf(stderr, "SQLite:     version %s\n", SQLITE_VERSION);

    int status;
    char buf[100];
    char* err_msg = NULL;

    // Open database
    char tmp_dir[100];
    get_dir(tmp_dir);
    snprintf(buf, sizeof(buf), SQLITE_NAME, tmp_dir, replica);
    unlink(buf);

    if (SQLITE3DB_INMEMORY) {
        status = sqlite3_open(":memory:", &db);
    } else {
        // open file-based database
        status = sqlite3_open(buf, &db);
    }
    if (status) {
        fprintf(stderr, "sqlite3 open error: %s\n", sqlite3_errmsg(db));
    }

    // Change SQLite cache size
    snprintf(buf, sizeof(buf),
                 "PRAGMA cache_size = %d",
                 SQLITE3DB_NUM_PAGES);
    status = sqlite3_exec(db, buf, NULL, NULL, &err_msg);
    sqlitedb_exec_check(status, err_msg);

    // FLAGS_page_size is defaulted to 1024
    snprintf(buf, sizeof(buf),
                "PRAGMA page_size = %d",
                SQLITE3DB_PAGE_SIZE);
    status = sqlite3_exec(db, buf, NULL, NULL, &err_msg);
    sqlitedb_exec_check(status, err_msg);

    // use synchronous / asynchronous writes
    if (SQLITE3DB_SYNCHRONOUS)
        snprintf(buf, sizeof(buf), "PRAGMA synchronous = FULL");
    else
        snprintf(buf, sizeof(buf), "PRAGMA synchronous = OFF");
    status = sqlite3_exec(db, buf, NULL, NULL, &err_msg);
    sqlitedb_exec_check(status, err_msg);

    // Change journal mode to WAL if WAL enabled flag is on
    // otherwise set journal mode to OFF -- no transactions!
    if (SQLITE3DB_WAL_ENABLED)
        snprintf(buf, sizeof(buf), "PRAGMA journal_mode = WAL");
    else
        snprintf(buf, sizeof(buf), "PRAGMA journal_mode = OFF");

    status = sqlite3_exec(db, buf, NULL, NULL, &err_msg);
    sqlitedb_exec_check(status, err_msg);

    // Change locking mode to exclusive and create tables/index for database
    snprintf(buf, sizeof(buf), "PRAGMA locking_mode = EXCLUSIVE");
    status = sqlite3_exec(db, buf, NULL, NULL, &err_msg);
    sqlitedb_exec_check(status, err_msg);

    fprintf(stdout, "------------------------------------------SQLite\n");
    fprintf(stdout, "num_pages:      %d\n", SQLITE3DB_NUM_PAGES);
    fprintf(stdout, "page_size:      %d\n", SQLITE3DB_PAGE_SIZE);
    fprintf(stdout, "synchronous:    %d\n", SQLITE3DB_SYNCHRONOUS);
    fprintf(stdout, "wal_enabled:    %d\n", SQLITE3DB_WAL_ENABLED);
    fprintf(stdout, "in_memory:      %d\n", SQLITE3DB_INMEMORY);
    fprintf(stdout, "locking_mode:   EXCLUSIVE\n");
    fprintf(stdout, "backup mode:    %d\n", SQLITE3DB_BACKUP);
    fprintf(stdout, "------------------------------------------------\n");

    return db;
}

int sqlitedb_close(sqlite3* db) {
    printf("[sqlite_server] closing the database\n");
    return sqlite3_close(db);
}

int sqlitedb_backup(sqlite3* db, int replica, int32_t snap_id) {
    char      snap_filename[100];
    sqlite3*        snap_db;
    sqlite3_backup* snap_backup;
    int r;

#ifdef DEBUG_SNAPSHOT
    uint64_t now = __ztas_now64();
#endif
    snprintf(snap_filename, sizeof(snap_filename),
            SQLITE_SNAP, SQLITE_SNAP_DIR, replica, snap_id);

    r = sqlite3_open(snap_filename, &snap_db);
    if (r == SQLITE_OK) {
        snap_backup = sqlite3_backup_init(snap_db, "main", db, "main");
        if (snap_backup) {
          sqlite3_backup_step(snap_backup, -1);
          sqlite3_backup_finish(snap_backup);
        }
        r = sqlite3_errcode(snap_db);
    }

    if (r != SQLITE_OK)
        printf("[sqlite_server] SNAPSHOT FAILED: %s\n", sqlite3_errmsg(snap_db));
    sqlite3_close(snap_db);
#ifdef DEBUG_SNAPSHOT
    now = __ztas_now64() - now;
    fprintf(stderr, "[sqlite_server] backup to file: %ld us\n", now/US);
#endif
    return r;
}

int sqlitedb_copy(sqlite3* to, sqlite3* from) {
#ifdef DEBUG_SNAPSHOT
    uint64_t now = __ztas_now64();
#endif
    sqlite3_backup* backup = sqlite3_backup_init(to, "main", from, "main");
    if (backup) {
      sqlite3_backup_step(backup, -1);
      sqlite3_backup_finish(backup);
    }
    int r = sqlite3_errcode(to);

    if (r != SQLITE_OK)
        printf("[sqlite_server] INMEMORY COPYING FAILED: %s\n", sqlite3_errmsg(to));

#ifdef DEBUG_SNAPSHOT
    now = __ztas_now64() - now;
    fprintf(stderr, "[sqlite_server] database copy-in-memory: %ld us\n", now/US);
#endif
    return r;
}

void sqlitedb_test(sqlite3* db) {
    printf("~~~~~ SQLite3 local test begin ~~~~~\n");
    int status = 0;
    sqlite3_stmt *stmt;
    char* test = "CREATE TABLE \"test_-1\" (key text, value text, PRIMARY KEY(key))";
    status = sqlite3_prepare_v2(db, test, strlen(test), &stmt, NULL);
    sqlitedb_error_check(status);
    status = sqlite3_step(stmt);
    sqlitedb_step_check(status);
    printf("~~~~~ SQLite3 local test end ~~~~~\n");
}
