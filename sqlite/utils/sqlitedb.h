/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#ifndef SQLITEDB_H_
#define SQLITEDB_H_

void sqlitedb_exec_check(int status, char *err_msg);
void sqlitedb_step_check(int status);
void sqlitedb_error_check(int status);

sqlite3* sqlitedb_open(int replica);
int sqlitedb_close(sqlite3* db);
int sqlitedb_backup(sqlite3* db, int replica, int32_t snap_id);
int sqlitedb_copy(sqlite3* to, sqlite3* from);
void sqlitedb_test(sqlite3* db);

#endif /* SQLITEDB_H_ */
