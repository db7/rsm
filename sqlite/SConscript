# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------
Import('env')
deps = env['LIBS']

utils = ['utils/sqlitedb.c', 'utils/sqlitefmt.c']

# try to find the static library libsqlite3.a in usual directories
sqlite3_name = ''
sqlite3_paths = ['', '/usr/lib/', '/usr/lib/x86_64-linux-gnu/', '/usr/local/lib/', '/usr/lib/i386-linux-gnu/']

import os.path
for sqlite3_path in sqlite3_paths:
    tmp = sqlite3_path + 'libsqlite3.a'
    if os.path.isfile(tmp) == True:
        sqlite3_name = tmp
        break
    
if len(sqlite3_name) == 0:
    print 'ERROR: sqlite3 static library (libsqlite3.a) not found,'
    print '    install libsqlite3-dev package'
    Exit(1)

##############################################
# found sqlite3 static library, continue build
##############################################

### server (one for all clients)
sqlite3_lib = File(sqlite3_name)
server = env.Program('rsm_sqlite_server', ['sqlite_server.c'] + utils,
                     LIBS = ['rsm_server', sqlite3_lib] + deps)

### simple client
client = env.Program('rsm_sqlite_client', ['sqlite_client.c'],
                     LIBS = ['rsm_client', 'ztime', 'utils'] + deps)

reg = env.Registry('rsm_sqlite_server')
reg.addFiles([server, client])

### simple performance benchmark (similar to echo_client)
perf_bench = env.Program('rsm_sqlite_perf_bench', ['sqlite_perf_bench.c'],
                     LIBS = ['rsm_client', 'ztime', 'utils'] + deps)

reg = env.Registry('rsm_sqlite_perf_bench')
reg.addFiles([server, perf_bench])


### leveldb performance benchmark
bench_src = ['bench/histogram.cc', 'bench/sqlite_bench.cc', 'bench/status.cc', 'bench/testutil.cc']

bench = env.Program('rsm_sqlite_bench', bench_src,
                     LIBS = ['rsm_client', 'ztime', 'utils', 'shrp'] + deps)

reg = env.Registry('rsm_sqlite_google_bench')
reg.addFiles([server, bench])
