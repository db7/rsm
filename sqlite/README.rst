.. -*- rst -*-

SQLite service
==============

- No support for transactions

- SQLite server opens one database for all users

- SQLite simple client (sqlite_client) is for test purposes

- SQLite simple benchmark (sqlite_perf_bench) is similar to echo_service

- SQLite complex benchmark (in bench/) is based on a microbenchmark from Google LevelDB
  original code is: https://code.google.com/p/leveldb/source/browse/doc/bench/db_bench_sqlite3.cc
  
  - batching (via transactions) and sync/async modes are not supported,
     they should be tuned on server-side (as well as many other parameters)
     
  - only one benchmark at a time
  
  - to test reads, need to populate the database first (e.g., with write benchmarks)
  
  
The exp/ directory contains 3 types of experiments on cluster:

- paxos     -- hardpaxos run as normal unprotected paxos

- encoded   -- hardpaxos, hardened with one AN-encoded trusted module

- twice     -- hardpaxos, hardened with duplicated AN-encoded trusted module  