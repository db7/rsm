# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

dude_version = 3
from dude.defaults import *
import dude.summaries
import muddi
import os
import glob
import sys
import struct
from ConfigParser import ConfigParser
sys.path.append("..")
import cluster 

############################
##  Configuration          #
############################

# ~~~~~~~~~~~ experiment ~~~~~~~~~~~ 
optspace = {
    'clients'    : [1, 2, 4, 8],
    'times'      : [1,2,3],
    'type'       : ['replace'],
}

TYPES = {
         'replace': {'prep': "CREATE TABLE 'test%d' (key text, value text, PRIMARY KEY(key))",
                     'exec': "REPLACE INTO 'test%d'(key, value) VALUES('key%d', 'Hello!')"}, 
        }

REQ_PERIOD   = 0.01
BATCH_SIZE   = 30

HALT_AFTER   = 30
MAX_INFLIGHT = 0        # doesn't matter
CONNECTALL   = 'false'
RSMENGINE    = 'true'

NUMRESPONSES  = 1
SEALREQUESTS  = 'false'
REPLICALIB    = '/librsm_replica_hard.so'

SERVER_FLAGS = '--synchronous=0 --WAL_enabled=1 --in_memory=1 --backup=0'

# ~~~~~~~~~~~ cluster ~~~~~~~~~~~ 
reserved_nodes = range(4, 50)
broken_nodes   = [7, 8, 9, 13, 17, 18, 28, 29]

first_server = reserved_nodes[0]
first_client = first_server + 3

# servers
servers = range(first_server, first_client)
for i in broken_nodes:
    if i in servers: servers.remove(i)

#clients
clients = range(first_client, max(reserved_nodes)+1)
for i in broken_nodes:
    if i in clients: clients.remove(i)

#hosts  = chosts = shosts = ['localhost']

hosts  = ['stream%d' % i for i in servers] + ['stream%d' % i for i in clients]
shosts = hosts[:len(servers)]
chosts = hosts[len(servers):]

assert len(servers) == 3, 'amount of server nodes is not 3'

replicas = [0,1,2]
deploy_dir = '/tmp/rsm'

############################
##  Dude run               #
############################

def prepare_global():
    if hosts != ['localhost']:
        files = glob.glob('../../*')
        files = [f for f in files if os.path.isfile(f)]
        h = [(host, files, deploy_dir) for host in set(hosts)]
        c = [(host, 'rm -rf %s/*' % deploy_dir, None, None) for host in set(hosts)]
        cfg = muddi.conf(c, h, [], logdir='prepare_global')
        muddi.check(cfg, stdout = sys.stdout, stderr = sys.stderr)
        muddi.execute(cfg, stdout = sys.stdout, stderr = sys.stderr)
        assert muddi.upload(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0


def prepare_exp(optpt):
    TYPE         = optpt['type']
    
    processes = [str(p) for p in replicas]
    config = ConfigParser()
    config.add_section('main')
    config.set('main', 'processes', ','.join(processes))
    config.set('main', 'cpu_stats', deploy_dir)
    config.set('main', 'stats', deploy_dir)
    config.set('main', 'halt_after', str(HALT_AFTER))

    config.add_section('paxos')
    config.set('paxos', 'replicas', ','.join(processes))
    config.set('paxos', 'max_inflight', str(MAX_INFLIGHT))

    config.add_section('rsm')
    config.set('rsm', 'enabled', RSMENGINE)
    config.set('rsm', 'connect_to_all', CONNECTALL)
    config.set('rsm', 'batch_size', str(BATCH_SIZE))
    
    config.set('rsm', 'seal_requests', SEALREQUESTS)
    config.set('rsm', 'num_responses', str(NUMRESPONSES))
    
    config.set('rsm', 'mod_replica', deploy_dir + REPLICALIB)    

    config.add_section('sqlite_client')
    config.set('sqlite_client', 'req_period', str(REQ_PERIOD))
    config.set('sqlite_client', 'prep_query', TYPES[TYPE]['prep'])
    config.set('sqlite_client', 'exec_query', TYPES[TYPE]['exec'])

    for p in replicas:
        host = shosts[p % len(shosts)]
        sec = 'p%d' % p
        config.add_section(sec)
        config.set(sec, 'host', host)

    f = open('rsm.ini', 'w')
    config.write(f)
    f.close()    
    

    upload  = [(host, 'rsm.ini', deploy_dir) for host in set(hosts)]
    cleanup = [(host, 'rm -f %s/*.dat' % deploy_dir, None, None)
               for host in set(hosts)]
    cfg = muddi.conf(cleanup, upload, [], logdir='prepare_exp')
    assert muddi.execute(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0
    assert muddi.upload(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0


def fork_exp(optpt):
    assert len(shosts) >= len(replicas), 'too few servers to deploy replicas on'

    procs = [(shosts[i % len(shosts)],
              'cd %s && (./rsm_sqlite_server %d rsm.ini %s < /dev/null)' \
                  % (deploy_dir, i, SERVER_FLAGS),
              None, 'killall rsm_sqlite_server') for i in replicas]

    for i in range(0, optpt['clients']):
        procs += [(chosts[i % len(chosts)],
                   'cd %s && (./rsm_sqlite_perf_bench rsm.ini %d < /dev/null)' \
                       % (deploy_dir, i),
                   None, 'killall rsm_sqlite_perf_bench')]

    down = [(host, '%s/*.dat' % deploy_dir, '.') for host in set(hosts)]
    cfg = muddi.conf(procs, [], down, logdir = 'logs')
    assert muddi.execute(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0
    muddi.download(cfg, stdout = sys.stdout, stderr = sys.stderr)


############################
##  Dude sum               #
############################

s = dude.summaries.FilesMultiLineSelect (
    name   = 'tput',
    files  = 'logs/execution-*',
    filters = [
        ('lat', '.*avglatency.*', lambda l: l.split(': ')[1].split(' ')[0]),
        ('tput', '.*throughput.*', lambda l: l.split(': ')[1]),
        ('sec', '.*seconds.*', lambda l: str(int(l.split(':')[1]))),
        ],
    fname_header = 'client',
    fname_split = lambda fname: fname.split('.')[0].split('execution-')[1]
    )

cpu = dude.summaries.FilesLineSelect (
    name   = 'cpu',
    files  = 'cpu-*',
    header = 'time elapsed thread process',
    split  = lambda l: l,
    fname_header = 'pid',
    fname_split = lambda fname: fname.split('.')[0].split('cpu-')[1]
    )

def netf(l):
    field = l.split(':')
    # time src dst
    field[2] = field[2].split('.')[1]
    return ' '.join(field)

net = dude.summaries.FilesLineSelect (
    name   = 'net',
    files  = 'stats-*',
    #header = 'time elapsed thread process',
    header = 'time src dst send scount recv rcount',
    regex  = ',*',
    split  = netf,
    fname_header = 'pid',
    fname_split = lambda fname: fname.split('.')[0].split('stats-')[1]
    )

## list of summaries
summaries = [s, cpu, net]
