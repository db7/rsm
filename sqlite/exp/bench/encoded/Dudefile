# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

dude_version = 3
from dude.defaults import *
import dude.summaries
import muddi
import os
import glob
import sys
import struct
sys.path.append("../..")
from ConfigParser import ConfigParser

############################
##  Configuration          #
############################

## experiment variables

optspace = {
    'clients'    : [1, 2, 4, 8, 16, 32, 40, 48],
    'batch'      : [30],
}

# ========= user params ==============
first_server = 20
reserved_servers = range(20, 50)
broken_servers   = [8, 9, 13, 17, 18, 28, 29]

first_client = 23

# servers
ids = range(first_server, first_client)
for i in broken_servers:
    if i in ids: ids.remove(i)
    
assert len(ids) == 3, 'amount of server nodes is not 3'

#clients
ids2 = range(first_client, max(reserved_servers)+1)
for i in broken_servers:
    if i in ids2: ids2.remove(i)

#hosts  = ['localhost']
#chosts = ['localhost']
#shosts = ['localhost']

hosts  = ['stream%d' % i for i in ids] + ['stream%d' % i for i in ids2]
shosts = hosts[:len(ids)]
chosts = hosts[len(ids):]

replicas = [0,1,2]

deploy_dir = '/tmp/rsm'

REQ_NUM      = 500
REQ_SIZE     = 1024       # key size is 16, in total it's ~100 bytes
BENCHMARK    = 'fillseq'

SERVER_FLAGS = '--page_size=1024 --num_pages=2000 --synchronous=0 --WAL_enabled=1 --in_memory=1'
BENCH_FLAGS  = '--benchmarks=%s --histogram=1 --compression_ratio=0.5 --num=%s --value_size=%s --reads=-1' \
                    % (BENCHMARK, REQ_NUM, REQ_SIZE)

def prepare_global():
    if hosts != ['localhost']:
        files = glob.glob('../../*')
        files = [f for f in files if os.path.isfile(f)]
        h = [(host, files, deploy_dir) for host in set(hosts)]
        c = [(host, 'rm -rf %s/*' % deploy_dir, None, None) for host in set(hosts)]
        cfg = muddi.conf(c, h, [], logdir='prepare_global')
        muddi.check(cfg, stdout = sys.stdout, stderr = sys.stderr)
        muddi.execute(cfg, stdout = sys.stdout, stderr = sys.stderr)
        assert muddi.upload(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0

def prepare_exp(optpt):
    # config file for 3 replicas

    HALT_AFTER   = 120
    MAX_INFLIGHT = 0        # doesn't matter
    BATCH_SIZE   = optpt['batch']
    
    CONNECTALL   = 'false'
    RSMENGINE     = 'true'
    
    NUM_RESPONSES = 2
    SEALREQUESTS  = 'true'
    
    processes = [str(p) for p in replicas]
    config = ConfigParser()
    config.add_section('main')
    config.set('main', 'processes', ','.join(processes))
    config.set('main', 'cpu_stats', deploy_dir)
    config.set('main', 'stats', deploy_dir)
    config.set('main', 'halt_after', str(HALT_AFTER))

    config.add_section('paxos')
    config.set('paxos', 'replicas', ','.join(processes))
    config.set('paxos', 'max_inflight', str(MAX_INFLIGHT))

    config.add_section('rsm')
    config.set('rsm', 'mod_replica', deploy_dir + '/librsm_replica_tc_enc.so')
    
    config.set('rsm', 'enabled', RSMENGINE)
    config.set('rsm', 'connect_to_all', CONNECTALL)
    config.set('rsm', 'batch_size', str(BATCH_SIZE))
    
    config.set('rsm', 'seal_requests', SEALREQUESTS)
    config.set('rsm', 'num_responses', str(NUM_RESPONSES))
    config.set('rsm', 'client_sync', 'true')        # sqlite benchmark needs sync mode

    for p in replicas:
        host = shosts[p % len(shosts)]
        sec = 'p%d' % p
        config.add_section(sec)
        config.set(sec, 'host', host)

    f = open('rsm.ini', 'w')
    config.write(f)
    f.close()

    upload  = [(host, 'rsm.ini', deploy_dir) for host in set(hosts)]
    cleanup = [(host, 'rm -f %s/*.dat' % deploy_dir, None, None)
               for host in set(hosts)]
    cfg = muddi.conf(cleanup, upload, [], logdir='prepare_exp')
    assert muddi.execute(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0
    assert muddi.upload(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0

def fork_exp(optpt):
    assert len(shosts) >= len(replicas), 'too few servers to deploy replicas on'

    procs = [(shosts[i % len(shosts)],
              'cd %s && (./rsm_sqlite_server %d rsm.ini %s < /dev/null)' \
                  % (deploy_dir, i, SERVER_FLAGS),
              None, 'killall rsm_sqlite_server') for i in replicas]

    for i in range(0, optpt['clients']):
        procs += [(chosts[i % len(chosts)],
                   'cd %s && (./rsm_sqlite_bench rsm.ini %s < /dev/null)' \
                       % (deploy_dir, BENCH_FLAGS),
                   None, 'killall rsm_sqlite_bench')]

    down = [(host, '%s/*.dat' % deploy_dir, '.') for host in set(hosts)]
    cfg = muddi.conf(procs, [], down, logdir = 'logs')
    assert muddi.execute(cfg, stdout = sys.stdout, stderr = sys.stderr) == 0
    muddi.download(cfg, stdout = sys.stdout, stderr = sys.stderr)


# =========== plot preparations ===========

s = dude.summaries.FilesMultiLineSelect (
    name   = 'tput',
    files  = 'logs/execution-*',
    filters = [
        ('lat', '.*latency.*', lambda l: l.split(': ')[1].split(' ')[0]),
        ('tput', '.*throughput.*', lambda l: l.split(': ')[1].split(' ')[0]),
        ],
    fname_header = 'client',
    fname_split = lambda fname: fname.split('.')[0].split('execution-')[1]
    )

cpu = dude.summaries.FilesLineSelect (
    name   = 'cpu',
    files  = 'cpu-*',
    header = 'time elapsed thread process',
    split  = lambda l: l,
    fname_header = 'pid',
    fname_split = lambda fname: fname.split('.')[0].split('cpu-')[1]
    )

def netf(l):
    field = l.split(':')
    # time src dst
    field[2] = field[2].split('.')[1]
    return ' '.join(field)

net = dude.summaries.FilesLineSelect (
    name   = 'net',
    files  = 'stats-*',
    #header = 'time elapsed thread process',
    header = 'time src dst send scount recv rcount',
    regex  = ',*',
    split  = netf,
    fname_header = 'pid',
    fname_split = lambda fname: fname.split('.')[0].split('stats-')[1]
    )

## list of summaries
summaries = [s, cpu, net]
