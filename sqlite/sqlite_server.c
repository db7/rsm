/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <ztas/ztime.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include <sqlite3.h>
#include "utils/sqlitedb.h"
#include "utils/sqlitefmt.h"
#include "../rsm_server.h"

#define MAXBUF 1*1024*1024 // 1 MB
//#define SQLTOJSON // comment out to use CVS format

static sqlite3* db_;
static char buf_[MAXBUF];

extern int SQLITE3DB_NUM_PAGES;
extern int SQLITE3DB_PAGE_SIZE;
extern int SQLITE3DB_SYNCHRONOUS;
extern int SQLITE3DB_WAL_ENABLED;
extern int SQLITE3DB_INMEMORY;
extern int SQLITE3DB_BACKUP;

static int
__deliver(rsm_server_t* server, rsmmsg_request_t* req, void* args)
{
    // req->data contains SQL command
//    printf("[sqlite_server] deliver: %s\n", req->data);

    int status = 0;
    sqlite3_stmt *stmt;
    status = sqlite3_prepare_v2(db_, req->data, req->size, &stmt, NULL);
    sqlitedb_error_check(status);

    if (status != SQLITE_OK) {
        fprintf(stderr, "[sqlite_server] during exec: %s", req->data);
    }

    static size_t res_size;
#ifdef SQLTOJSON
    status = sqlitedb_json(stmt, buf_, MAXBUF, &res_size);
#else
    status = sqlitedb_csv(stmt, buf_, MAXBUF, &res_size);
#endif
//    res_size++; // account for trailing zero
    sqlitedb_step_check(status);

    int isleader = ((req->flags & 0x01) == 1);
    if (isleader) {
        //TODO for now leader does not send any reply
        return 1;
    }

    rsmmsg_response_t* res = rsmmsg_response_init(res_size);
    res->k        = req->k;
    res->cid      = req->cid;
    res->sent     = req->sent;
    res->has_next = req->has_next;
    res->bcount   = req->bcount;
    memcpy(res->data, buf_, res->size);

    rsm_server_response(server, res);
    shrp_free(res);

    return 1;
}


typedef struct {
    rsm_server_t* server;
    int32_t       snap_id;
    sqlite3*      new_db;
} snap_async_t;

static snap_async_t snap_async;

// run in helper thread
static void*
__snapshot_async(void* snap_async) {
    snap_async_t* s = (snap_async_t*) snap_async;
    sqlitedb_backup(s->new_db, s->server->rsm->pid, s->snap_id);
    sqlite3_close(s->new_db);

    rsm_server_snapdone(s->server, s->snap_id);
    return NULL;
}

static int
__snapshot(rsm_server_t* server, int32_t snap_id, void* args)
{
    printf("[sqlite_server] snapshot: %d\n", snap_id);

    switch (SQLITE3DB_BACKUP) {
    case 0:
        // no backups
        rsm_server_snapdone(server, snap_id);
        break;
    case 1:
        // sync backup
        sqlitedb_backup(db_, server->rsm->pid, snap_id);
        rsm_server_snapdone(server, snap_id);
        break;
    case 2:
        // async backup: first copy to another in-memory db,
        //               then perform sync backup in helper thread
        snap_async.server  = server;
        snap_async.snap_id = snap_id;
        snap_async.new_db = NULL;
        int r = sqlite3_open(":memory:", &snap_async.new_db);
        if (r == SQLITE_OK) {
            // in-memory copy is hopefully fast and could be done synchronously
            sqlitedb_copy(snap_async.new_db, db_);
            // copy-to-storage is supposedly slow, so delegate to helper thread
            pthread_t snap_thread;
            pthread_create(&snap_thread, NULL, __snapshot_async, (void*) &snap_async);
        }
    }

    return 1;
}

int
main(int argc, char* argv[])
{
    assert (argc >= 3 && "sqlite_server `replica_id` `rsm.ini` `sqlite3_flags`");

    int i;
    for (i = 3; i < argc; i++) {
        int n;
        char junk;
        if (sscanf(argv[i], "--page_size=%d%c", &n, &junk) == 1) {
            SQLITE3DB_PAGE_SIZE = n;
        } else if (sscanf(argv[i], "--num_pages=%d%c", &n, &junk) == 1) {
            SQLITE3DB_NUM_PAGES = n;
        } else if (sscanf(argv[i], "--synchronous=%d%c", &n, &junk) == 1 &&
                (n == 0 || n == 1)) {
            SQLITE3DB_SYNCHRONOUS = n;
        } else if (sscanf(argv[i], "--WAL_enabled=%d%c", &n, &junk) == 1 &&
                   (n == 0 || n == 1)) {
            SQLITE3DB_WAL_ENABLED = n;
        } else if (sscanf(argv[i], "--in_memory=%d%c", &n, &junk) == 1 &&
                (n == 0 || n == 1)) {
            SQLITE3DB_INMEMORY = n;
        } else if (sscanf(argv[i], "--backup=%d%c", &n, &junk) == 1) {
            SQLITE3DB_BACKUP = n;
        } else {
          fprintf(stderr, "[sqlite_server] invalid flag '%s'\n", argv[i]);
          exit(1);
        }
      }

    db_ = sqlitedb_open(atoi(argv[1]));
    sqlitedb_test(db_);

    rsm_server_cbs_t cbs;
    cbs.deliver = __deliver;
    cbs.snapshot = __snapshot;
    cbs.response = NULL;

    struct ev_loop* loop = ev_loop_new(0);
    rsm_server_t* server = rsm_server_init(loop, atoi(argv[1]),
            argv[2], &cbs, NULL);

    ev_loop(loop, 0);

    rsm_server_fini(server);
    sqlitedb_close(db_);

    ev_loop_destroy(loop);
    printf("[sqlite_server] server stopped\n");

    return 0;
}
