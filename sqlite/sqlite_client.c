/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ztas/shrp.h>
#include <ztas/proc/cfg.h>
#include "../rsm_client.h"

typedef struct {
    int32_t k;
    int32_t cid;

    struct ev_loop* loop;
    struct ev_timer finish_w;
    struct ev_timer create_w;

    rsm_client_t* client;
} state_t;

void __error_check(int status) {
  if (status != 1) {
    fprintf(stderr, "[SQLite client] error: %d\n", status);
  }
}

void
__response(rsm_client_t* rsm, rsmmsg_response_t* res, void* arg)
{
//    state_t* state = (state_t*) arg;
    printf("[SQLite client] response %d:\n%s\n", res->cid, res->data);
}

void
__request(state_t* state, char* sqlcmd, int size) {
    size++; // account for trailing zero

    rsmmsg_request_t* req = rsmmsg_request_init(size);
    req->k    = state->k;
    req->cid  = state->cid++;
    req->size = size;
    memcpy(req->data, sqlcmd, req->size);

    int r = 0;
    r = rsm_client_request(state->client, req, __response, (void*) state);
    __error_check(r);

    shrp_free(req);
}

void
test_start(struct ev_loop* loop, struct ev_timer* w, int revents) {
    if(EV_ERROR & revents) exit(EXIT_FAILURE);
    state_t* state = (state_t*) w->data;

    char buf[1000];
    int size;

    size = snprintf(buf, sizeof(buf),
            "CREATE TABLE test (key text, value text, PRIMARY KEY(key))");
    __request(state, buf, size);

    size = snprintf(buf, sizeof(buf),
            "REPLACE INTO test(key, value) VALUES('foo', 'Hello!')");
    __request(state, buf, size);

    size = snprintf(buf, sizeof(buf),
            "REPLACE INTO test(key, value) VALUES('bar', 'Bye!')");
    __request(state, buf, size);

    size = snprintf(buf, sizeof(buf),
            "SELECT key, value FROM test ORDER BY key");
    __request(state, buf, size);

    printf("[SQLite client] sent requests, waiting for answers...\n");
}

void
test_finish(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if(EV_ERROR & revents) exit(EXIT_FAILURE);

    state_t* state = (state_t*) w->data;

    ev_timer_stop(loop, &state->finish_w);
    rsm_client_stop(state->client);

    rsm_client_fini(state->client);
    printf("[SQLite client] Test finished\n");
}

int
main(const int argc, const char* argv[])
{
    assert (argc == 2 && "sqlite_client rsm.ini");

    state_t state;
    state.loop = ev_loop_new(0);
    state.cid = 0;
    state.client = rsm_client_initev(NULL, argv[1], -1, (void*) &state);
    state.k   = state.client->k;

    cfg_t* cfg = cfg_parse(argv[1]);

    const char* str = cfg_get(cfg, "main:halt_after");
    double halt_time_s = str ? atof(str): 0; // in seconds

    // requests are created once starting in 3 seconds
    ev_timer_init(&state.create_w, test_start, 3., 0);

    // terminate everything after x seconds
    ev_timer_init(&state.finish_w, test_finish, halt_time_s, 0.);

    state.create_w.data   = (void*) &state;
    state.finish_w.data   = (void*) &state;

    ev_timer_start(state.loop, &state.create_w);
    if ((int)halt_time_s != 0)
        ev_timer_start(state.loop, &state.finish_w);

    ev_loop(state.loop, 0);

    ev_loop_destroy(state.loop);

    printf("[SQLite client] client stopped\n");
    return 0;
}
