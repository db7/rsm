/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */

// Copyright (c) 2011 The LevelDB Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

// adoption to ZTAS done by Dmitry Kuvayskiy

// TODO change implementation to pure C some day

#include <assert.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <pthread.h>
#include <unistd.h>

#include "status.h"
#include "histogram.h"
#include "random.h"
#include "testutil.h"

extern "C" {
    #include <ztas/shrp.h>
    #include <ztas/proc/cfg.h>
    #include "../../rsm_client.h"
    #include "../../rsm_msg.h"
}

typedef struct {
    int32_t k;
    int32_t cid;

    rsm_client_t* client;
} state_t;

static state_t* state_ = NULL;

/* -----------------------------------------------------------------------------
 *   RSM functions/callbacks
 * -------------------------------------------------------------------------- */

void
__request(state_t* state, char* sqlcmd, int size, char** out_res, int* out_size) {
    size++; // account for trailing zero

    rsmmsg_request_t* req = rsmmsg_request_init(size);
    req->k    = state->k;
    req->cid  = state->cid++;
    req->size = size;
    memcpy(req->data, sqlcmd, req->size);

    int r = 0;
    rsmmsg_response_t* res; // no need to free here

    // execute request and wait for response
//    printf("SQLiteBench: invoking request %s\n", req->data);
    r = rsm_client_invoke(state->client, req, &res, (void*) state);
    if (res->cid != req->cid) {
        fprintf(stderr, "SQLiteBench: response %d for request %d is incorrect\n", res->cid, req->cid);
        shrp_free(req);
        return;
    }

//    printf("~~~ Response %d:\n'%s'\n", res->cid, res->data);
    if (out_res) {
        *out_size = res->size;
        *out_res  = res->data;
    }

    shrp_free(req);
}

/* -----------------------------------------------------------------------------
 *   Benchmark
 * -------------------------------------------------------------------------- */

// Comma-separated list of operations to run in the specified order
//   Actual benchmarks:
//
//   fillseq       -- write N values in sequential key order in async mode
//   fillseqsync   -- write N/100 values in sequential key order in sync mode
//   fillseqbatch  -- batch write N values in sequential key order in async mode
//   fillrandom    -- write N values in random key order in async mode
//   fillrandsync  -- write N/100 values in random key order in sync mode
//   fillrandbatch -- batch write N values in sequential key order in async mode
//   overwrite     -- overwrite N values in random key order in async mode
//   fillrand100K  -- write N/1000 100K values in random order in async mode
//   fillseq100K   -- write N/1000 100K values in sequential order in async mode
//   readseq       -- read N times sequentially
//   readrandom    -- read N times in random order
//   readrand100K  -- read N/1000 100K values in sequential order in async mode
static const char* FLAGS_benchmarks =
    "fillseq,"
    "fillseqsync,"
    "fillseqbatch,"
    "fillrandom,"
    "fillrandsync,"
    "fillrandbatch,"
    "overwrite,"
    "overwritebatch,"
    "readrandom,"
    "readseq,"
    "fillrand100K,"
    "fillseq100K,"
    "readseq,"
    "readrand100K,"
    ;

// Number of key/values to place in database
static int FLAGS_num = 1000; //1000000;

// Number of read operations to do.  If negative, do FLAGS_num reads.
static int FLAGS_reads = -1;

// Size of each value
static int FLAGS_value_size = 100;

// Print histogram of operation timings
static int FLAGS_histogram = 0;

// Arrange to generate values that shrink to this fraction of
// their original size after compression
static double FLAGS_compression_ratio = 0.5;

// If true, do not destroy the existing database.  If you set this
// flag and also specify a benchmark that wants a fresh database, that
// benchmark will fail.
static int FLAGS_use_existing_db = 0;

namespace sqlite_bench {

// Helper for quickly generating random data.
namespace {
class RandomGenerator {
 private:
  std::string data_;
  int pos_;

 public:
  RandomGenerator() {
    // We use a limited amount of data over and over again and ensure
    // that it is larger than the compression window (32KB), and also
    // large enough to serve all typical value sizes we want to write.
    Random rnd(301);
    std::string piece;
    while (data_.size() < 1048576) {
      // Add a short fragment that is as compressible as specified
      // by FLAGS_compression_ratio.
      test::CompressibleString(&rnd, FLAGS_compression_ratio, 100, &piece);
      data_.append(piece);
    }
    pos_ = 0;
  }

  Slice Generate(int len) {
    if (pos_ + len > data_.size()) {
      pos_ = 0;
      assert(len < data_.size());
    }
    pos_ += len;
    return Slice(data_.data() + pos_ - len, len);
  }
};

}  // namespace

class Benchmark {
 private:
  sqlite3* db_;
  int db_num_;
  int num_;
  int reads_;
  double start_;
  double last_op_finish_;
  int64_t bytes_;
  std::string message_;
  Histogram hist_;
  RandomGenerator gen_;
  Random rand_;

  // State kept for progress messages
  int done_;
  int next_report_;     // When to report next

  uint64_t NowMicros() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return static_cast<uint64_t>(tv.tv_sec) * 1000000 + tv.tv_usec;
  }

  void PrintHeader() {
    const int kKeySize = 16;
    fprintf(stdout, "------------------------------------------SQLite\n");
    fprintf(stdout, "Keys:       %d bytes each\n", kKeySize);
    fprintf(stdout, "Values:     %d bytes each\n", FLAGS_value_size);
    fprintf(stdout, "Entries:    %d\n", num_);
    fprintf(stdout, "RawSize:    %.1f MB (estimated)\n",
            ((static_cast<int64_t>(kKeySize + FLAGS_value_size) * num_)
             / 1048576.0));
    fprintf(stdout, "------------------------------------------------\n");
    fflush(stdout);
  }

  void Start() {
    start_ = NowMicros() * 1e-6;
    bytes_ = 0;
    message_.clear();
    last_op_finish_ = start_;
    hist_.Clear();
    done_ = 0;
    next_report_ = 100;
  }

  void FinishedSingleOp() {
    if (FLAGS_histogram) {
      double now = NowMicros() * 1e-6;
      double micros = (now - last_op_finish_) * 1e6;
      hist_.Add(micros);
      if (micros > 20000) {
        fprintf(stderr, "long op: %.1f micros%30s\r", micros, "");
        fflush(stderr);
      }
      last_op_finish_ = now;
    }

    done_++;
    if (done_ >= next_report_) {
      if      (next_report_ < 1000)   next_report_ += 100;
      else if (next_report_ < 5000)   next_report_ += 500;
      else if (next_report_ < 10000)  next_report_ += 1000;
      else if (next_report_ < 50000)  next_report_ += 5000;
      else if (next_report_ < 100000) next_report_ += 10000;
      else if (next_report_ < 500000) next_report_ += 50000;
      else                            next_report_ += 100000;
      fprintf(stderr, "... finished %d ops%30s\r", done_, "");
      fflush(stderr);
    }
  }

  void Stop(const Slice& name) {
    double finish = NowMicros() * 1e-6;

    // Pretend at least one op was done in case we are running a benchmark
    // that does not call FinishedSingleOp().
    if (done_ < 1) done_ = 1;

    if (bytes_ > 0) {
      char rate[100];
      snprintf(rate, sizeof(rate), "%06.1f KB/s",
               (bytes_ / 1024.0) / (finish - start_));
      if (!message_.empty()) {
        message_  = std::string(rate) + " " + message_;
      } else {
        message_ = rate;
      }
    }

    fprintf(stdout, "%-12s :\n\tlatency: %011.3f micros/op\n\tthroughput:%s%s\n",
            name.ToString().c_str(),
            (finish - start_) * 1e6 / done_,
            (message_.empty() ? "" : " "),
            message_.c_str());
    if (FLAGS_histogram) {
      fprintf(stdout, "Microseconds per op:\n%s\n", hist_.ToString().c_str());
    }
    fflush(stdout);
  }

 public:
  enum Order {
    SEQUENTIAL,
    RANDOM
  };
  enum DBState {
    FRESH,
    EXISTING
  };

  Benchmark()
  : db_(NULL),
    db_num_(0),
    num_(FLAGS_num),
    reads_(FLAGS_reads < 0 ? FLAGS_num : FLAGS_reads),
    bytes_(0),
    rand_(301),
    start_(0),
    done_(0),
    next_report_(0),
    last_op_finish_(0.0) {
  }

  ~Benchmark() {
  }

  void Run() {
    PrintHeader();

    char buf[1000];
    int size;
    size = snprintf(buf, sizeof(buf),
            "CREATE TABLE \"test_%d\" (key text, value text, PRIMARY KEY(key))", state_->k);
    __request(state_, buf, size, NULL, NULL);

    const char* benchmarks = FLAGS_benchmarks;
    while (benchmarks != NULL) {
      const char* sep = strchr(benchmarks, ',');
      Slice name;
      if (sep == NULL) {
        name = benchmarks;
        benchmarks = NULL;
      } else {
        name = Slice(benchmarks, sep - benchmarks);
        benchmarks = sep + 1;
      }

      printf("=== Benchmark %s ===\n", name.ToString().c_str());
      bytes_ = 0;
      Start();

      bool known = true;
      bool write_sync = false;
      if (name == Slice("fillseq")) {
        Write(write_sync, SEQUENTIAL, FRESH, num_, FLAGS_value_size, 1);
      } else if (name == Slice("fillseqbatch")) {
        printf("  abort: batching (transactions) not supported\n");
      } else if (name == Slice("fillrandom")) {
        Write(write_sync, RANDOM, FRESH, num_, FLAGS_value_size, 1);
      } else if (name == Slice("fillrandbatch")) {
        printf("  abort: batching (transactions) not supported\n");
      } else if (name == Slice("overwrite")) {
        printf("  note: database must be filled by 'fillseq' beforehand\n");
        Write(write_sync, RANDOM, EXISTING, num_, FLAGS_value_size, 1);
      } else if (name == Slice("overwritebatch")) {
        printf("  abort: batching (transactions) not supported\n");
      } else if (name == Slice("fillrandsync")) {
        printf("  abort: synchronous database mode not supported\n");
      } else if (name == Slice("fillseqsync")) {
        printf("  abort: synchronous database mode not supported\n");
      } else if (name == Slice("fillrand100K")) {
        Write(write_sync, RANDOM, FRESH, num_ / 1000, 100 * 1000, 1);
      } else if (name == Slice("fillseq100K")) {
        Write(write_sync, SEQUENTIAL, FRESH, num_ / 1000, 100 * 1000, 1);
      } else if (name == Slice("readseq")) {
        printf("  note: database must be filled by 'fillseq' beforehand\n");
        ReadSequential();
      } else if (name == Slice("readrandom")) {
        printf("  note: database must be filled by 'fillseq' beforehand\n");
        Read(RANDOM, 1);
      } else if (name == Slice("readrand100K")) {
        printf("  note: database must be filled by 'fillseq' beforehand\n");
        int n = reads_;
        reads_ /= 1000;
        Read(RANDOM, 1);
        reads_ = n;
      } else {
        known = false;
        if (name != Slice()) {  // No error message for empty name
          fprintf(stderr, "unknown benchmark '%s'\n", name.ToString().c_str());
        }
      }
      if (known) {
        Stop(name);
      }

      printf("Sleeping for 2 seconds...\n");
      sleep(2);
    }
  }

  void Write(bool write_sync, Order order, DBState state,
             int num_entries, int value_size, int entries_per_batch) {
    // TODO function does not reset a database
    //      should be called only once per benchmark run

    // NB: - write_sync is useless, synchronous/asynchronous is set on server side
    //     - entries_per_batch is useless, we do not support transactions

    if (num_entries != num_) {
      char msg[100];
      snprintf(msg, sizeof(msg), "(%d ops)", num_entries);
      message_ = msg;
    }

    char* rvalue = (char*) malloc(value_size + 1);
    for (int i = 0; i < num_entries; i++) {
        // Create and execute SQL statements
        const char* value = gen_.Generate(value_size).data();

        // TODO copying is bad, but we need null-terminator for the string
        //      think of anything smarter
        memcpy(rvalue, value, value_size);
        rvalue[value_size] = '\0';

        // Create values for key-value pair
        const int k = (order == SEQUENTIAL) ? i :
                      (rand_.Next() % num_entries);
        char key[100];
        snprintf(key, sizeof(key), "%016d", k);

        char replace_str[1000*1000];
        int size = snprintf(replace_str, 1000*1000,
                "REPLACE INTO \"test_%d\" (key, value) VALUES ('%s', '%s')",
                state_->k, key, rvalue);

//        printf("SENDING key %s\n", key);

        bytes_ += value_size + strlen(key);

        __request(state_, replace_str, size, NULL, NULL);

        FinishedSingleOp();
    }
    free(rvalue);
  }

  void Read(Order order, int entries_per_batch) {
      // NB: - entries_per_batch is useless, we do not support transactions

      char* res_data;
      int   res_size;

      for (int i = 0; i < reads_; i ++) {
        // Create and execute SQL statements

        // Create key value
        char key[100];
        int k = (order == SEQUENTIAL) ? i : (rand_.Next() % reads_);
        snprintf(key, sizeof(key), "%016d", k);

        char read_str[10000];
        int size = snprintf(read_str, 10000,
                "SELECT key, value FROM \"test_%d\" WHERE key = '%s'",
                state_->k, key);

        __request(state_, read_str, size, &res_data, &res_size);
        // real data is of line_size minus newline minus comma minus 4 quotes
        int payload_size = res_size - 1 - 1 - 4 - strlen("\"key\",\"value\"") - 1;
        bytes_ += payload_size;
//        printf("Response (%d, %d): %s\n", res_size, payload_size, res_data);

        FinishedSingleOp();
    }
  }

  void ReadSequential() {
      char read_str[10000];
      int size = snprintf(read_str, 10000,
              "SELECT key, value FROM \"test_%d\" ORDER BY key",
              state_->k);

      char* res_data;
      int   res_size;
      __request(state_, read_str, size, &res_data, &res_size);

      FILE *stream = fmemopen(res_data, res_size, "r");
      uint64_t buf_size = 1000;
      ssize_t line_size;
      char* line = (char*) malloc(buf_size);

      // reading row by row
      while ((line_size = getline(&line, &buf_size, stream)) > 0) {
          // real data is of line_size minus newline minus comma minus 4 quotes
          bytes_ += line_size - 1 - 1 - 4;
          printf("SQLiteBench DEBUG: line %s\n", line);
          printf("SQLiteBench DEBUG: bytes %d\n", (int)(line_size - 1 - 1 - 4));

          FinishedSingleOp();
      }

      free(line);
      fclose(stream);
  }

};

}  // namespace sqlite_bench

int main(int argc, char** argv) {
  std::string default_db_path;
  for (int i = 3; i < argc; i++) {
    double d;
    int n;
    char junk;
    if (sqlite_bench::Slice(argv[i]).starts_with("--benchmarks=")) {
      FLAGS_benchmarks = argv[i] + strlen("--benchmarks=");
    } else if (sscanf(argv[i], "--histogram=%d%c", &n, &junk) == 1 &&
               (n == 0 || n == 1)) {
      FLAGS_histogram = n;
    } else if (sscanf(argv[i], "--compression_ratio=%lf%c", &d, &junk) == 1) {
      FLAGS_compression_ratio = d;
    } else if (sscanf(argv[i], "--use_existing_db=%d%c", &n, &junk) == 1 &&
               (n == 0 || n == 1)) {
      FLAGS_use_existing_db = n;
    } else if (sscanf(argv[i], "--num=%d%c", &n, &junk) == 1) {
      FLAGS_num = n;
    } else if (sscanf(argv[i], "--reads=%d%c", &n, &junk) == 1) {
      FLAGS_reads = n;
    } else if (sscanf(argv[i], "--value_size=%d%c", &n, &junk) == 1) {
      FLAGS_value_size = n;
    } else {
      fprintf(stderr, "SQLiteBench: Invalid flag '%s'\n", argv[i]);
      exit(1);
    }
  }

  state_t state;
  state_ = &state;

  state.client = rsm_client_initev(NULL, argv[1], atoi(argv[2]), (void*) &state);
  state.k   = state.client->k;
  state.cid = 0;

  sleep(3);

  sqlite_bench::Benchmark benchmark;
  benchmark.Run();

  printf("SQLiteBench: [%d] Benchmark terminates\n", state.k);
  return 0;
}
