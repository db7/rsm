/* ----------------------------------------------------------------------
 * Copyright (c) 2013 Technische Universitaet Dresden
 * Distributed under the MIT license. See accompanying file LICENSE.
 * ------------------------------------------------------------------- */
#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#include <ztas/shrp.h>
#include <ztas/ds/tree.h>
#include <ztas/ds/queue.h>
#include <ztas/utils/hash.h>
#include <debug.h>
#include <ztas/shrp.h>
#include "../rsm_client.h"
#include <ztas/ztime.h>
#include <ztas/proc/cfg.h>

#define TINTERVAL 1.0
#define MAXBUFFER 10000

/* -----------------------------------------------------------------------------
 * data structures
 * -------------------------------------------------------------------------- */

typedef struct {
    int32_t k;
    int32_t cid;

    int sent_count;
    int prepared;

    double req_period;
    char prep_query[MAXBUFFER];
    char exec_query[MAXBUFFER];

    struct ev_loop* loop;

    struct ev_timer finish_w;
    struct ev_timer create_w;
    struct ev_timer stat_w;

    uint64_t count;
    uint64_t tput_count;
    uint64_t recv_count;
    uint64_t ts;

    uint64_t avglatency;
    int      seconds;

    rsm_client_t* client;
} state_t;

/* -----------------------------------------------------------------------------
 * client
 * -------------------------------------------------------------------------- */
void
__response(rsm_client_t* rsm, rsmmsg_response_t* res, void* arg)
{
    state_t* state = (state_t*) arg;
    uint64_t now = __ztas_now64();
    uint64_t send_time = res->sent; // contains the sent time of the request

    state->count      += res->bcount ? res->bcount : 1;
    state->tput_count += res->bcount ? res->bcount : 1;
    state->recv_count++;
    state->avglatency += now - send_time;
}

int
__request(state_t* state, char* sqlcmd) {
    int32_t size = strnlen(sqlcmd, MAXBUFFER) + 1;
    if (size <= 1)
        return -1;

//    if (state->cid >= 10)        return;

    rsmmsg_request_t* req = rsmmsg_request_init(size);
    req->k    = state->k;
    req->cid  = state->cid++;
    req->size = size;
    req->sent = __ztas_now64();
    memcpy(req->data, sqlcmd, req->size);

//    printf("[sqlite_perf_bench] Sending:\n\t%s\n", sqlcmd);
    int r = rsm_client_request(state->client, req, __response, (void*) state);

    shrp_free(req);
    return r;
}

void
create_request(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if(EV_ERROR & revents) exit(EXIT_FAILURE);

    state_t* state = (state_t*) w->data;

    char str[MAXBUFFER];
    if (!state->prepared) {
        snprintf(str, MAXBUFFER, state->prep_query, state->k);
        state->prepared = 1;
    } else {
        snprintf(str, MAXBUFFER, state->exec_query, state->k, state->cid);
    }

    int r = __request(state, str);
    if (r == 0)
        printf("ERROR WHILE SENDING REQUEST %d\n", state->cid);
    else
        state->sent_count++;
}

void
print_stat(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    state_t* state = (state_t*) w->data;
    uint64_t now = __ztas_now64();
    uint64_t elapsed = now - state->ts;

    printf("CLIENT\n");
    printf("\tthroughput: %f\n", state->tput_count*TINTERVAL*S/elapsed);
    if (state->tput_count != 0)
        printf("\tavglatency: %f us\n", state->avglatency/1.0/US/state->recv_count);
    else
        printf("\tavglatency: 0.0 us\n");

    printf("\tmissing responses: %ld\n", state->sent_count - state->count);
    printf("\tseconds: %d\n", state->seconds);
    state->tput_count = 0;
    state->recv_count = 0;
    state->ts = __ztas_now64();
    state->avglatency = 0;
    state->seconds++;
}

/* -----------------------------------------------------------------------------
 * main()
 * -------------------------------------------------------------------------- */

void
finish_test(struct ev_loop* loop, struct ev_timer* w, int revents)
{
    if(EV_ERROR & revents) exit(EXIT_FAILURE);

    state_t* state = (state_t*) w->data;

    ev_timer_stop(loop, &state->finish_w);
    ev_timer_stop(loop, &state->create_w);
    ev_timer_stop(loop, &state->stat_w);

    rsm_client_stop(state->client);
    rsm_client_fini(state->client);
}

int
main(const int argc, const char* argv[])
{
    // create proxy state
    state_t state;
    state.loop = ev_loop_new(0);
    state.cid = 0;

    // measurements
    state.sent_count = 0;
    state.prepared   = 0;

    state.ts = __ztas_now64();
    state.count = 0;
    state.tput_count = 0;
    state.recv_count = 0;
    state.avglatency = 0;
    state.seconds = 1;

    // initilize rsm
    assert (argc >= 2 && "sqlite_client rsm.ini [client_id]");

    int32_t client_id = -1;
    if (argc > 2)
        client_id = atoi(argv[2]);

//    state.client = rsm_client_initev(NULL, argv[1], client_id, (void*) &state);
    state.client = rsm_client_initev(state.loop, argv[1], client_id, (void*) &state);
    state.k   = state.client->k;

    cfg_t* cfg = cfg_parse(argv[1]);
    const char* str = cfg_get(cfg, "main:halt_after");
    double halt_time_s = str ? atof(str): 0; // in seconds

    // period before creating new request (in seconds)
    str = cfg_get(cfg, "sqlite_client:req_period");
    state.req_period = str ? atof(str) : 0.1;

    // prepare query
    // NOTE: must have one parameter "%d", bound to state.k
    str = cfg_get(cfg, "sqlite_client:prep_query");
    memset(state.prep_query, 0, MAXBUFFER);
    if (str)
        strncpy(state.prep_query, str, MAXBUFFER);

    // executable query
    // NOTE: must have two parameters "%d", bound to state.k & state.cid
    str = cfg_get(cfg, "sqlite_client:exec_query");
    memset(state.exec_query, 0, MAXBUFFER);
    if (str)
        strncpy(state.exec_query, str, MAXBUFFER);

    // requests are created starting from 3rd second
    ev_timer_init(&state.create_w, create_request, 3., state.req_period);
    // terminate everything after halt_time_s seconds
    ev_timer_init(&state.finish_w, finish_test, halt_time_s, 0.);
    // get statistics every TINTERVAL seconds
    ev_timer_init(&state.stat_w, print_stat, 3.03, TINTERVAL);

    // save state as watchers data
    state.create_w.data   = (void*) &state;
    state.finish_w.data   = (void*) &state;
    state.stat_w.data   = (void*) &state;

    ev_timer_start(state.loop, &state.create_w);
    ev_timer_start(state.loop, &state.stat_w);
    if ((int)halt_time_s != 0)
        ev_timer_start(state.loop, &state.finish_w);

    ev_loop(state.loop, 0);

    ev_loop_destroy(state.loop);
    printf("[sqlite_client] Client is stopped\n");

    return 0;
}
