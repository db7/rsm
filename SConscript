# -*- python -*-

# ----------------------------------------------------------------------
# Copyright (c) 2013 Technische Universitaet Dresden
# Distributed under the MIT license. See accompanying file LICENSE.
# ----------------------------------------------------------------------

import os.path

Import('env')
deps = env['LIBS']

############################
# create utils
############################
SConscript('utils/SConscript', duplicate = False, exports='env')

############################
# create consensus libraries
############################
SConscript('hardpaxos/SConscript', duplicate = False, exports='env')

################################################################################
## Programs and static libraries
################################################################################

ztas        = env.Registry.collectObjects(['ev', 'net', 'ds', 'ztime', 'msg',
                                    'support', 'proc', 'shrp', 'stats',
                                    'utils'])
#iface      = env.Registry.collectObjects(['logger02'])
iface       = env.Registry.collectObjects(['ztasif'])
net         = env.Registry.collectObjects(['ev', 'net', 'ds', 'shrp'])

core        = env.Registry.collectObjects(['ev', 'net', 'ds', 'ztime', 'proc-mod',
                                    'shrp', 'stats', 'utils', 'support'])
rsm_utils   = env.Registry.collectObjects(['rsm_utils'])

########################
## rsm_replica library
########################
replica_hard  = env.Object(['hardpaxos_rsm/rsm_replica.c']) +\
    env.Registry.collectObjects(['hardpaxos-replica', 'hardpaxos-ds', 'hardpaxos-trusted', 'rsm_utils'])
env.Library('rsm_replica_hard_static', replica_hard + ztas + iface)
    
replica_hard_enc  = env.Object(['hardpaxos_rsm/rsm_replica.c']) +\
    env.Registry.collectObjects(['hardpaxos-replica', 'hardpaxos-ds', 'hardpaxos-trusted-enc', 'rsm_utils'])
env.Library('rsm_replica_hard_enc_static', replica_hard_enc + ztas + iface)

replica_hard_enc2  = env.Object(['hardpaxos_rsm/rsm_replica.c']) +\
    env.Registry.collectObjects(['hardpaxos-replica', 'hardpaxos-ds', 'hardpaxos-trusted-enc2', 'rsm_utils'])
env.Library('rsm_replica_hard_enc2_static', replica_hard_enc2 + ztas + iface)

########################
## rsm_engine
########################
rsm_engine = env.Object(['rsm_engine.c', 'rsm_msg.c']) + rsm_utils

########################
## rsm_server library
########################
server = env.Object(['rsm_server.c']) + rsm_utils
rsmserver = env.Library('rsm_server_replica_hard', server + replica_hard + rsm_engine + ztas + iface)

########################
## rsm_client library
########################
client = env.Object(['rsm_client.c', 'rsm_msg.c']) + net + rsm_utils +\
    env.Registry.collectObjects(['cfg'])
rsmclient = env.Library('rsm_client', client)

################################################################################
## Shared libraries
################################################################################

env_mod = env.Clone()
env_mod.Append(CFLAGS = "-DIS_MODULE ")

sztas = env.Registry.collectSharedObjects(['ztasif', 'ds', 'msg', 'support',
                                           'shrp', 'utils'])
smsg = env.SharedObject('rsm_msg.c')

########################
## sharedlib replica_hard
########################
sreplica_hard  = env_mod.SharedObject(['hardpaxos_rsm/rsm_replica.c']) + sztas + smsg +\
    env.Registry.collectSharedObjects(['hardpaxos-replica', 'hardpaxos-ds', 'hardpaxos-trusted', 'rsm_utils'])
sr_hard = env_mod.SharedLibrary('rsm_replica_hard', sreplica_hard)

############################
## sharedlib replica_hard_enc
############################
'''
To build the encoded version of hardpaxos (which has bigger cert_t header),
  we need to compile all hardpaxos sources with HARDPAXOS_ENCODED macro;
Since we compile the same source for unencoded AND encoded versions,
  need to put the "hardpaxos-enc" object files in some other directory 
'''
env_enc = env_mod.Clone()
env_enc.Append(CPPDEFINES = ['HARDPAXOS_ENCODED'])

# encoded with one TM
enc_obj_files = env_enc.SharedObject(target = 'hardpaxos_all_enc/rsm_replica.c.os', source = 'hardpaxos_rsm/rsm_replica.c')
for src_file in env_mod.Registry.collectSources(['hardpaxos-replica', 'hardpaxos-trusted-enc']):
    str = src_file.rstr()
    enc_obj_files.append( env_enc.SharedObject(target = 'hardpaxos_all_enc/%s.os' % os.path.basename(str), source = str) )

sreplica_hard_enc  = enc_obj_files + sztas + smsg +\
        env.Registry.collectSharedObjects(['hardpaxos-ds', 'rsm_utils'])
sr_hard_enc = env_enc.SharedLibrary('rsm_replica_hard_enc', sreplica_hard_enc)

# encoded with two TMs
enc_obj_files = env_enc.SharedObject(target = 'hardpaxos_all_enc2/rsm_replica.c.os', source = 'hardpaxos_rsm/rsm_replica.c')
for src_file in env_mod.Registry.collectSources(['hardpaxos-replica', 'hardpaxos-trusted-enc2']):
    str = src_file.rstr()
    enc_obj_files.append( env_enc.SharedObject(target = 'hardpaxos_all_enc2/%s.os' % os.path.basename(str), source = str) )

sreplica_hard_enc2  = enc_obj_files + sztas + smsg +\
        env.Registry.collectSharedObjects(['hardpaxos-ds', 'rsm_utils'])
sr_hard_enc2 = env_enc.SharedLibrary('rsm_replica_hard_enc2', sreplica_hard_enc2)

########################
## rsm_hard
########################
reg = env.Registry('rsm_hard')
reg.addFiles([sr_hard])

########################
## rsm_hard_enc
########################
reg = env.Registry('rsm_hard_enc')
reg.addFiles([sr_hard_enc])

reg = env.Registry('rsm_hard_enc2')
reg.addFiles([sr_hard_enc2])

########################
## rsm_server library
########################
env.Library('rsm_server', server + rsm_engine + core + iface)

reg = env.Registry('rsm_all')
reg.addFiles([sr_hard])
reg.addFiles([sr_hard_enc])
reg.addFiles([sr_hard_enc2])
reg.addFiles([rsmserver])
reg.addFiles([rsmclient])


########################
# create services
########################
SConscript('echo_server/SConscript', duplicate = False, exports='env')
SConscript('sqlite/SConscript', duplicate = False, exports='env')

